import calendar
import datetime
import re
import sys

import pyautogui
from selenium.webdriver.common.keys import Keys

sys.path.append('../')

import pytest
import random

import time
import unittest
import allure
from selenium import webdriver
from selenium.webdriver import ActionChains

from Helper.Helper_common import *

import glob
import os
import unittest
import configparser
import HTMLTestRunner
import openpyxl
from read_configs import *
from winreg import *
import Helper.Helper_common
from datetime import datetime


# **********************************************************************************************************************


class RPanel_Body(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.driver = webdriver.Chrome('chromedriver.exe')
        driver = self.driver
        driver.maximize_window()

        # Log in
        login(driver)
        time.sleep(5)

        # Moving to test page
        driver.get(url_root + '/project-visibility/work-report')
        time.sleep(10)

    @classmethod
    def tearDownClass(self):
        self.driver.quit()

    @allure.story('Check Next day')
    def test_WR_Next_day(self):
        driver = self.driver
        list_fail = []

        driver.refresh()
        time.sleep(5)

        try:
            with allure.step('Click on Next day button'):
                current_date = driver.find_element_by_css_selector(
                    'table.scheduler-bg-table> * th.is-today>div>span.day-num').text

                btn_right = driver.find_element_by_css_selector('div.header-mid>div>span:last-child')
                ActionChains(driver).move_to_element(btn_right).click().perform()
                time.sleep(.05)
        except:
            list_fail.append('Unable to click on Next button')

        try:
            with allure.step('Check the date jumps by 1'):
                next_date = driver.find_element_by_css_selector(
                    'table.scheduler-bg-table> * th.active>div>span.day-num').text

                # compare date with range 1
                self.assertTrue(
                    (datetime.strptime(next_date, '%d/%m') - datetime.strptime(current_date, '%d/%m')).days > 0)
        except:
            list_fail.append('After clicking on Next day, the active date jumps by [' +
                             (datetime.strptime(next_date, '%d/%m') - datetime.strptime(current_date, '%d/%m')).days
                             + ']')

        try:
            with allure.step('Check today highlight color'):
                # check highlight colour
                self.assertEqual(rbga_to_hex(driver.find_element_by_css_selector('th.active').value_of_css_property(
                    'border-top-color')).upper(), '#7A8EBB')
        except:
            list_fail.append('The Today cell is highlighted with an incorrect color')

        try:
            with allure.step('Check format of title '):
                # format of title
                a = driver.find_element_by_css_selector('span.header2-text-label').text
                validate_date(a)
        except:
            list_fail.append('Incorrect data format, should be DD-MM-YYYY')

        try:
            with allure.step('Check Filter Options label'):
                # Filter Options label
                self.assertEqual(driver.find_element_by_css_selector('div.filter-div').text, 'Filter Options')
        except:
            list_fail.append('No Filter Options label')

        try:
            with allure.step('Check date name header'):
                # check table header
                for i in driver.find_elements_by_css_selector('table.scheduler-bg-table> * th'):
                    self.assertTrue(i.find_element_by_css_selector('* span.day-name').text in
                                    ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN'])
        except:
            list_fail.append("The header is not displayed as ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN']")

        try:
            with allure.step('Check date text header'):
                # check table header
                for i in driver.find_elements_by_css_selector('table.scheduler-bg-table> * th'):
                    self.assertTrue(datetime.strptime(i.find_element_by_css_selector('* span.day-num').text, '%d/%m'))
        except:
            list_fail.append('The date format is not DD/MM')

        try:
            with allure.step('Check Sat/Sun background colour'):
                # check Sat bg colour
                sat_color = driver.find_element_by_css_selector('table.scheduler-bg-table> * th:nth-last-child(2)'). \
                    value_of_css_property('background-color')
                sun_color = driver.find_element_by_css_selector('table.scheduler-bg-table> * th:last-child'). \
                    value_of_css_property('background-color')

                self.assertEquals(sat_color, sun_color, 'rgba(225, 236, 255, 0.5)')
        except:
            list_fail.append('The background colour of Sat/Sun is not [rgba(225,236,255,0.5)]')

        try:
            with allure.step('Check user Alexiel'):
                # check current user name
                scroll_to(driver, driver.find_element_by_xpath("//div[contains(text(), 'Alexiel')]"))
                time.sleep(0.2)
        except:
            list_fail.append('Username Alexiel is not available')

        try:
            with allure.step('Check department'):
                # check user department
                self.assertTrue(
                    driver.find_element_by_xpath("//div[contains(text(), 'Alexiel')]/following-sibling::div"))
        except:
            list_fail.append("User's department is not displayed")

        try:
            with allure.step('Check avatar'):
                # check user avatar
                self.assertTrue(
                    driver.find_element_by_xpath("//div[contains(text(), 'Alexiel')]/../preceding-sibling::span"))
        except:
            list_fail.append("User's avatar is not displayed")

        try:
            with allure.step('Check week icons'):
                # check week icon
                week_icon = []
                for i in driver.find_elements_by_xpath(
                        "//div[contains(text(), 'Alexiel')]/../../following::div[1]/div/div[@class='day']"):
                    week_icon.append(i.text)
                self.assertEqual(week_icon, ['M', 'T', 'W', 'T', 'F', 'S', 'S'])
        except:
            list_fail.append("The week order is not ['M', 'T', 'W', 'T', 'F', 'S', 'S']")

        try:
            with allure.step('Check text Total'):
                # check text [Total: x/40]
                total_text = ''
                for i in driver.find_elements_by_xpath(
                        "//div[contains(text(), 'Alexiel')]/../../following-sibling::div[2]/span"):
                    total_text = total_text + i.text
                self.assertTrue('Total:' in total_text and '/40' in total_text)
        except:
            list_fail.append('The Total text is displayed in wrong format')

        try:
            with allure.step('Verify Vertical scrollbar'):
                offsetHeight = int(
                    driver.find_element_by_css_selector('div.resource-view').get_property('offsetHeight'))
                scrollHeight = int(
                    driver.find_element_by_css_selector('table.resource-table>tbody').get_property('scrollHeight'))

                self.assertTrue(scrollHeight > offsetHeight)
                time.sleep(0.01)
        except:
            list_fail.append('No Vertical scrollbar')
            pass

        self.assertEqual(len(list_fail), 0, list_fail)

    @allure.story('Check Previous day')
    def test_WR_Previous_day(self):
        driver = self.driver
        list_fail = []

        driver.refresh()
        time.sleep(5)

        try:
            with allure.step('Click on Previous day button'):
                current_date = driver.find_element_by_css_selector(
                    'table.scheduler-bg-table> * th.is-today>div>span.day-num').text

                btn_left = driver.find_element_by_css_selector('div.header-mid>div>span:first-child')
                ActionChains(driver).move_to_element(btn_left).click().perform()
                time.sleep(.05)
        except:
            list_fail.append('Unable to click on Previous button')

        try:
            with allure.step('Check the date jumps by 1'):
                previous_date = driver.find_element_by_css_selector(
                    'table.scheduler-bg-table> * th.active>div>span.day-num').text

                # compare date with range 1
                self.assertTrue(
                    (datetime.strptime(previous_date, '%d/%m') - datetime.strptime(current_date, '%d/%m')).days < 0)
        except:
            list_fail.append('After clicking on Previous day, the active date jumps by [' +
                             (datetime.strptime(previous_date, '%d/%m') - datetime.strptime(current_date, '%d/%m')).days
                             + ']')

        try:
            with allure.step('Check today highlight color'):
                # check highlight colour
                self.assertEqual(rbga_to_hex(driver.find_element_by_css_selector('th.active').value_of_css_property(
                    'border-top-color')).upper(), '#7A8EBB')
        except:
            list_fail.append('The Today cell is highlighted with an incorrect color')

        try:
            with allure.step('Check format of title '):
                # format of title
                a = driver.find_element_by_css_selector('span.header2-text-label').text
                validate_date(a)
        except:
            list_fail.append('Incorrect data format, should be DD-MM-YYYY')

        try:
            with allure.step('Check Filter Options label'):
                # Filter Options label
                self.assertEqual(driver.find_element_by_css_selector('div.filter-div').text, 'Filter Options')
        except:
            list_fail.append('No Filter Options label')

        try:
            with allure.step('Check date name header'):
                # check table header
                for i in driver.find_elements_by_css_selector('table.scheduler-bg-table> * th'):
                    self.assertTrue(i.find_element_by_css_selector('* span.day-name').text in
                                    ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN'])
        except:
            list_fail.append("The header is not displayed as ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN']")

        try:
            with allure.step('Check date text header'):
                # check table header
                for i in driver.find_elements_by_css_selector('table.scheduler-bg-table> * th'):
                    self.assertTrue(datetime.strptime(i.find_element_by_css_selector('* span.day-num').text, '%d/%m'))
        except:
            list_fail.append('The date format is not DD/MM')

        try:
            with allure.step('Check Sat/Sun background colour'):
                # check Sat bg colour
                sat_color = driver.find_element_by_css_selector('table.scheduler-bg-table> * th:nth-last-child(2)'). \
                    value_of_css_property('background-color')
                sun_color = driver.find_element_by_css_selector('table.scheduler-bg-table> * th:last-child'). \
                    value_of_css_property('background-color')

                self.assertEquals(sat_color, sun_color, 'rgba(225, 236, 255, 0.5)')
        except:
            list_fail.append('The background colour of Sat/Sun is not [rgba(225,236,255,0.5)]')

        try:
            with allure.step('Check user Alexiel'):
                # check current user name
                scroll_to(driver, driver.find_element_by_xpath("//div[contains(text(), 'Alexiel')]"))
                time.sleep(0.2)
        except:
            list_fail.append('Username Alexiel is not available')

        try:
            with allure.step('Check department'):
                # check user department
                self.assertTrue(
                    driver.find_element_by_xpath("//div[contains(text(), 'Alexiel')]/following-sibling::div"))
        except:
            list_fail.append("User's department is not displayed")

        try:
            with allure.step('Check avatar'):
                # check user avatar
                self.assertTrue(
                    driver.find_element_by_xpath("//div[contains(text(), 'Alexiel')]/../preceding-sibling::span"))
        except:
            list_fail.append("User's avatar is not displayed")

        try:
            with allure.step('Check week icons'):
                # check week icon
                week_icon = []
                for i in driver.find_elements_by_xpath(
                        "//div[contains(text(), 'Alexiel')]/../../following::div[1]/div/div[@class='day']"):
                    week_icon.append(i.text)
                self.assertEqual(week_icon, ['M', 'T', 'W', 'T', 'F', 'S', 'S'])
        except:
            list_fail.append("The week order is not ['M', 'T', 'W', 'T', 'F', 'S', 'S']")

        try:
            with allure.step('Check text Total'):
                # check text [Total: x/40]
                total_text = ''
                for i in driver.find_elements_by_xpath(
                        "//div[contains(text(), 'Alexiel')]/../../following-sibling::div[2]/span"):
                    total_text = total_text + i.text
                self.assertTrue('Total:' in total_text and '/40' in total_text)
        except:
            list_fail.append('The Total text is displayed in wrong format')

        try:
            with allure.step('Verify Vertical scrollbar'):
                offsetHeight = int(
                    driver.find_element_by_css_selector('div.resource-view').get_property('offsetHeight'))
                scrollHeight = int(
                    driver.find_element_by_css_selector('table.resource-table>tbody').get_property('scrollHeight'))

                self.assertTrue(scrollHeight > offsetHeight)
                time.sleep(0.01)
        except:
            list_fail.append('No Vertical scrollbar')
            pass

        self.assertEqual(len(list_fail), 0, list_fail)

    @allure.story('Check Day tab')
    def test_WR_Check_Day_tab(self):
        driver = self.driver
        list_fail = []

        # driver.refresh()
        # time.sleep(5)

        try:
            with allure.step('Verify the Day button'):
                # verify Day button
                self.assertEqual(len(driver.find_elements_by_css_selector('div.header-right>div>label:first-child')), 1)
        except:
            list_fail.append('The Day button is not displayed')

        try:
            with allure.step('Click on Day button'):
                # click on Day button
                ActionChains(driver).move_to_element(
                    driver.find_element_by_css_selector('div.header-right>div>label:first-child')).click().perform()
                time.sleep(3)
        except:
            list_fail.append('Unable to click on Day button')

        try:
            with allure.step('Check Filter Options label'):
                # Filter Options label
                self.assertEqual(driver.find_element_by_css_selector('div.filter-div').text, 'Filter Options')
        except:
            list_fail.append('No Filter Options label')

        try:
            with allure.step('Check date name header'):
                # check table header
                for i in driver.find_elements_by_css_selector('div.week-days'):
                    self.assertTrue(i.find_element_by_css_selector('* span.day-name').text in
                                    ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN'])
        except:
            list_fail.append("The header is not displayed as ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN']")

        try:
            with allure.step('Check date text header'):
                # check table header
                for i in driver.find_elements_by_css_selector('div.week-days'):
                    self.assertTrue(datetime.strptime(i.find_element_by_css_selector('* span.day-num').text, '%d/%m'))
        except:
            list_fail.append('The date format is not DD/MM')

        try:
            with allure.step('Check Sat/Sun background colour'):
                # check Sat bg colour
                sat_color = driver.find_element_by_css_selector('table.scheduler-bg-table> * th:nth-last-child(2)'). \
                    value_of_css_property('background-color')
                sun_color = driver.find_element_by_css_selector('table.scheduler-bg-table> * th:last-child'). \
                    value_of_css_property('background-color')

                self.assertEquals(sat_color, sun_color, 'rgba(225, 236, 255, 0.5)')
        except:
            list_fail.append('The background colour of Sat/Sun is not [rgba(225,236,255,0.5)]')

        try:
            with allure.step('Check user Alexiel'):
                # check current user name
                scroll_to(driver, driver.find_element_by_xpath("//div[contains(text(), 'Alexiel')]"))
                time.sleep(0.2)
        except:
            list_fail.append('Username Alexiel is not available')

        try:
            with allure.step('Check department'):
                # check user department
                self.assertTrue(
                    driver.find_element_by_xpath("//div[contains(text(), 'Alexiel')]/following-sibling::div"))
        except:
            list_fail.append("User's department is not displayed")

        try:
            with allure.step('Check avatar'):
                # check user avatar
                self.assertTrue(
                    driver.find_element_by_xpath("//div[contains(text(), 'Alexiel')]/../preceding-sibling::span"))
        except:
            list_fail.append("User's avatar is not displayed")

        try:
            with allure.step('Check week icons'):
                # check week icon
                week_icon = []
                for i in driver.find_elements_by_xpath(
                        "//div[contains(text(), 'Alexiel')]/../../following::div[1]/div/div[@class='day']"):
                    week_icon.append(i.text)
                self.assertEqual(week_icon, ['M', 'T', 'W', 'T', 'F', 'S', 'S'])
        except:
            list_fail.append("The week order is not ['M', 'T', 'W', 'T', 'F', 'S', 'S']")

        try:
            with allure.step('Check text Total'):
                # check text [Total: x/40]
                total_text = ''
                for i in driver.find_elements_by_xpath(
                        "//div[contains(text(), 'Alexiel')]/../../following-sibling::div[2]/span"):
                    total_text = total_text + i.text
                self.assertTrue('Total:' in total_text and '/40' in total_text)
        except:
            list_fail.append('The Total text is displayed in wrong format')

        try:
            with allure.step('Check time stamp'):
                # check timestamp
                timestamp = []
                for i in driver.find_elements_by_css_selector('table.scheduler-bg-table> * th>div>span'):
                    scroll_to(driver, i)
                    timestamp.append(i.text)

                expected = ['00h30', '01h', '01h30', '02h', '02h30', '03h', '03h30', '04h', '04h30', '05h',
                            '05h30', '06h', '06h30', '07h', '07h30', '08h', '08h30', '09h', '09h30', '10h',
                            '10h30', '11h', '11h30', '12h', '12h30', '13h', '13h30', '14h', '14h30', '15h',
                            '15h30', '16h', '16h30', '17h', '17h30', '18h', '18h30', '19h', '19h30', '20h',
                            '20h30', '21h', '21h30', '22h', '22h30', '23h', '23h30', '00h']

                self.assertEqual(timestamp, expected)
        except:
            list_fail.append('The different value is ' + str(list(set(expected) - set(timestamp))))

        try:
            with allure.step('Check color blocks'):
                # check colour of non-working hours
                color_blocks = driver.find_elements_by_css_selector(
                    'div.scheduler-bg>table> * tr:first-child>td[style*="background-color"]')

                for i in color_blocks:
                    self.assertEqual(i.value_of_css_property('background-color'), 'rgba(225, 236, 255, 0.5)')
        except:
            list_fail.append('A non-working hour block is not in correct color rgba(225, 236, 255, 0.5)')

        try:
            with allure.step('Check real time line'):
                # check current time tail
                self.assertEqual(len(driver.find_elements_by_css_selector('div.current-time-tail')), 1)
        except:
            list_fail.append('The real time line is not displayed')

        try:
            with allure.step('Verify Vertical scrollbar'):
                offsetHeight = int(
                    driver.find_element_by_css_selector('div.resource-view').get_property('offsetHeight'))
                scrollHeight = int(
                    driver.find_element_by_css_selector('table.resource-table>tbody').get_property('scrollHeight'))

                self.assertTrue(scrollHeight > offsetHeight)
                time.sleep(0.01)
        except:
            list_fail.append('No Vertical scrollbar')
            pass

        try:
            with allure.step('Verify work block'):
                ActionChains(driver).move_to_element(
                    driver.find_element_by_css_selector('div.header-left> * div.react-select__control')) \
                    .click().perform()
                time.sleep(0.2)

                scroll_to(driver, driver.find_elements_by_css_selector('div.react-select__menu-list>div')[24])
                ActionChains(driver).move_to_element(
                    driver.find_elements_by_css_selector('div.react-select__menu-list>div')[24]).click().perform()
                time.sleep(0.2)

                scroll_to(driver, driver.find_element_by_xpath("//div[contains(text(), 'Aaron')]"))
                time.sleep(0.2)

                self.assertTrue(len(driver.find_elements_by_css_selector('a.timeline-event')) > 0)
        except:
            list_fail.append('There is no workblock for user Aaron in week #25')

        self.assertEqual(len(list_fail), 0, list_fail)

    @allure.story('Check Month tab')
    def test_WR_Check_Month_tab(self):
        driver = self.driver
        list_fail = []

        driver.refresh()
        time.sleep(5)

        try:
            with allure.step('Verify the Month button'):
                # verify Month button
                self.assertEqual(len(driver.find_elements_by_css_selector('div.header-right>div>label:last-child')), 1)
        except:
            list_fail.append('The Month button is not displayed')

        try:
            with allure.step('Click on Month button'):
                # click on Month button
                ActionChains(driver).move_to_element(
                    driver.find_element_by_css_selector('div.header-right>div>label:last-child')).click().perform()
                time.sleep(3)
        except:
            list_fail.append('Unable to click on Month button')

        try:
            with allure.step('Check Filter Options label'):
                # Filter Options label
                self.assertEqual(driver.find_element_by_css_selector('div.filter-div').text, 'Filter Options')
        except:
            list_fail.append('No Filter Options label')

        try:
            with allure.step('Check number of days on current month'):
                # check number of cell in month
                expected = calendar.monthrange(datetime.now().year, datetime.now().month)
                self.assertEqual(
                    len(driver.find_elements_by_css_selector(
                        'table.scheduler-bg-table> * th.header3-text')), expected[1])
        except:
            list_fail.append('The header only contains [' + str(len(driver.find_elements_by_css_selector(
                'table.scheduler-bg-table> * th.header3-text'))) + '] days instead of ['
                             + str(expected[1]) + ']')

        try:
            with allure.step('Check date name header'):
                # check table header
                for i in driver.find_elements_by_css_selector('table.scheduler-bg-table> * th'):
                    self.assertTrue(i.find_element_by_css_selector('* span.day-name').text in
                                    ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN'])
        except:
            list_fail.append("The header is not displayed as ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN']")

        try:
            with allure.step('Check date text header'):
                # check table header
                for i in driver.find_elements_by_css_selector('table.scheduler-bg-table> * th'):
                    self.assertTrue(datetime.strptime(i.find_element_by_css_selector('* span.day-num').text, '%d/%m'))
        except:
            list_fail.append('The date format is not DD/MM')

        try:
            with allure.step('Check past days highlight'):
                # check past days highlight
                for i in driver.find_elements_by_css_selector(
                        'table.scheduler-bg-table> * th.header3-text:not(.is-future):not(.is-today)> * span.day-num'):
                    self.assertEqual(rbga_to_hex(i.value_of_css_property('color')).upper(), '#427FEC')
        except:
            list_fail.append('A past day is not highlighted')

        try:
            with allure.step('Check all Sat background colour'):
                # check Sat bg colour
                for i in driver.find_elements_by_xpath('//span[contains(text(), "Sat")]/../..'):
                    scroll_to(driver, i)
                    time.sleep(0.1)
                    self.assertEqual(i.value_of_css_property('background-color'), 'rgba(225, 236, 255, 0.5)')
        except:
            list_fail.append('The background colour of Sat is not [rgba(225,236,255,0.5)]')

        try:
            with allure.step('Check all Sun background colour'):
                # check Sun bg colour
                for i in driver.find_elements_by_xpath('//span[contains(text(), "Sun")]/../..'):
                    scroll_to(driver, i)
                    time.sleep(0.1)
                    self.assertEqual(i.value_of_css_property('background-color'), 'rgba(225, 236, 255, 0.5)')
        except:
            list_fail.append('The background colour of Sun is not [rgba(225,236,255,0.5)]')

        try:
            with allure.step('Check user Alexiel'):
                # check current user name
                scroll_to(driver, driver.find_element_by_xpath("//div[contains(text(), 'Alexiel')]"))
                time.sleep(0.2)
        except:
            list_fail.append('Username Alexiel is not available')

        try:
            with allure.step('Check department'):
                # check user department
                self.assertTrue(
                    driver.find_element_by_xpath("//div[contains(text(), 'Alexiel')]/following-sibling::div"))
        except:
            list_fail.append("User's department is not displayed")

        try:
            with allure.step('Check avatar'):
                # check user avatar
                self.assertTrue(
                    driver.find_element_by_xpath("//div[contains(text(), 'Alexiel')]/../preceding-sibling::span"))
        except:
            list_fail.append("User's avatar is not displayed")

        try:
            with allure.step('Check week icons'):
                # check week icon
                week_icon = []
                for i in driver.find_elements_by_xpath(
                        "//div[contains(text(), 'Alexiel')]/../../following::div[1]/div/div[@class='day']"):
                    week_icon.append(i.text)
                self.assertEqual(week_icon, ['M', 'T', 'W', 'T', 'F', 'S', 'S'])
        except:
            list_fail.append("The week order is not ['M', 'T', 'W', 'T', 'F', 'S', 'S']")

        try:
            with allure.step('Check text Total'):
                # check text [Total: x/40]
                total_text = ''
                for i in driver.find_elements_by_xpath(
                        "//div[contains(text(), 'Alexiel')]/../../following-sibling::div[2]/span"):
                    total_text = total_text + i.text
                self.assertTrue('Total:' in total_text and '/40' in total_text)
        except:
            list_fail.append('The Total text is displayed in wrong format')

        try:
            with allure.step('Verify Vertical scrollbar'):
                offsetHeight = int(
                    driver.find_element_by_css_selector('div.resource-view').get_property('offsetHeight'))
                scrollHeight = int(
                    driver.find_element_by_css_selector('table.resource-table>tbody').get_property('scrollHeight'))

                self.assertTrue(scrollHeight > offsetHeight)
                time.sleep(0.01)
        except:
            list_fail.append('No Vertical scrollbar')
            pass

        try:
            with allure.step('Check today highlight color'):
                # check highlight colour
                self.assertEqual(rbga_to_hex(driver.find_element_by_css_selector('th.is-today').value_of_css_property(
                    'border-top-color')).upper(), '#7A8EBB')
        except:
            list_fail.append('The Today cell is highlighted with an incorrect color')

        try:
            with allure.step('Check work block'):
                # check Work block
                self.assertTrue(len(driver.find_elements_by_css_selector(
                    'div.scheduler-bg>table.scheduler-bg-table> * td>div')) > 0)
        except:
            list_fail.append('No work block is displayed')

        try:
            with allure.step('Check case icon color'):
                # check case icon colour
                a = driver.execute_script(
                    "return window.getComputedStyle(document.querySelector('.icon-biztrip'),':before')"
                    ".getPropertyValue('color')")
                self.assertEqual(rbg_to_hex(a).upper(), '#AC96F9')
        except:
            list_fail.append('The case icon color is [' + rbg_to_hex(a).upper() + '] instead of [#AC96F9]')

        self.assertEqual(len(list_fail), 0, list_fail)

    @allure.story('Check Week - Works table')
    def test_WR_Check_Week_work_table(self):
        driver = self.driver
        list_fail = []

        driver.refresh()
        time.sleep(5)

        try:
            with allure.step('Check Filter Options label'):
                # Filter Options label
                self.assertEqual(driver.find_element_by_css_selector('div.filter-div').text, 'Filter Options')
        except:
            list_fail.append('No Filter Options label')

        try:
            with allure.step('Check date name header'):
                # check table header
                for i in driver.find_elements_by_css_selector('table.scheduler-bg-table> * th'):
                    self.assertTrue(i.find_element_by_css_selector('* span.day-name').text in
                                    ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN'])
        except:
            list_fail.append("The header is not displayed as ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN']")

        try:
            with allure.step('Check date text header'):
                # check table header
                for i in driver.find_elements_by_css_selector('table.scheduler-bg-table> * th'):
                    self.assertTrue(datetime.strptime(i.find_element_by_css_selector('* span.day-num').text, '%d/%m'))
        except:
            list_fail.append('The date format is not DD/MM')

        try:
            with allure.step('Check today highlight color'):
                # check highlight colour
                self.assertEqual(rbga_to_hex(driver.find_element_by_css_selector('th.is-today').value_of_css_property(
                    'border-top-color')).upper(), '#7A8EBB')
        except:
            list_fail.append('The Today cell is highlighted with an incorrect color')

        try:
            with allure.step('Check Today color'):
                for i in driver.find_elements_by_css_selector('td.is-today'):
                    scroll_to(driver, i)
                    self.assertEqual(i.value_of_css_property('background-color'), 'rgb(250, 253, 255)')
                    time.sleep(0.2)
        except:
            list_fail.append('The Today cell is filled with an incorrect color')

        try:
            with allure.step('Check Sat/Sun background colour'):
                # check Sat bg colour
                sat_color = driver.find_element_by_css_selector('table.scheduler-bg-table> * th:nth-last-child(2)'). \
                    value_of_css_property('background-color')
                sun_color = driver.find_element_by_css_selector('table.scheduler-bg-table> * th:last-child'). \
                    value_of_css_property('background-color')

                self.assertEquals(sat_color, sun_color, 'rgba(225, 236, 255, 0.5)')
        except:
            list_fail.append('The background colour of Sat/Sun is not [rgba(225,236,255,0.5)]')

        try:
            with allure.step('Check user Alexiel'):
                # check current user name
                scroll_to(driver, driver.find_element_by_xpath("//div[contains(text(), 'Alexiel')]"))
                time.sleep(0.2)
        except:
            list_fail.append('Username Alexiel is not available')

        try:
            with allure.step('Check department'):
                # check user department
                self.assertTrue(
                    driver.find_element_by_xpath("//div[contains(text(), 'Alexiel')]/following-sibling::div"))
        except:
            list_fail.append("User's department is not displayed")

        try:
            with allure.step('Check avatar'):
                # check user avatar
                self.assertTrue(
                    driver.find_element_by_xpath("//div[contains(text(), 'Alexiel')]/../preceding-sibling::span"))
        except:
            list_fail.append("User's avatar is not displayed")

        try:
            with allure.step('Check week icons'):
                # check week icon
                week_icon = []
                for i in driver.find_elements_by_xpath(
                        "//div[contains(text(), 'Alexiel')]/../../following::div[1]/div/div[@class='day']"):
                    week_icon.append(i.text)
                self.assertEqual(week_icon, ['M', 'T', 'W', 'T', 'F', 'S', 'S'])
        except:
            list_fail.append("The week order is not ['M', 'T', 'W', 'T', 'F', 'S', 'S']")

        try:
            with allure.step('Check text Total'):
                # check text [Total: x/40]
                total_text = ''
                for i in driver.find_elements_by_xpath(
                        "//div[contains(text(), 'Alexiel')]/../../following-sibling::div[2]/span"):
                    total_text = total_text + i.text
                self.assertTrue('Total:' in total_text and '/40' in total_text)
        except:
            list_fail.append('The Total text is displayed in wrong format')

        try:
            with allure.step('Verify Vertical scrollbar'):
                offsetHeight = int(
                    driver.find_element_by_css_selector('div.resource-view').get_property('offsetHeight'))
                scrollHeight = int(
                    driver.find_element_by_css_selector('table.resource-table>tbody').get_property('scrollHeight'))

                self.assertTrue(scrollHeight > offsetHeight)
                time.sleep(0.01)
        except:
            list_fail.append('No Vertical scrollbar')
            pass

        try:
            with allure.step('Check work block'):
                # check Work block
                self.assertTrue(len(driver.find_elements_by_css_selector('a.timeline-event')) > 0)
        except:
            list_fail.append('No work block is displayed')

        self.assertEqual(len(list_fail), 0, list_fail)

    @allure.story('Check Work report page')
    def test_WR_Work_report_page(self):
        driver = self.driver
        list_fail = []

        driver.refresh()
        time.sleep(5)

        try:
            with allure.step('Check Week dropdown'):
                # check week Dropdown
                self.assertTrue(
                    len(driver.find_elements_by_css_selector('div.select-week>div.react-select__control')) > 0)
        except:
            list_fail.append('No Week dropdown')

        try:
            with allure.step('Select new week'):
                # click on dropdown and select random week
                ActionChains(driver).move_to_element(
                    driver.find_element_by_css_selector('div.select-week>div.react-select__control')).click().perform()
                time.sleep(0.5)

                selections = driver.find_elements_by_css_selector(
                    'div.react-select__menu-list>div:not(.react-select__option--is-selected)')

                ActionChains(driver).move_to_element(
                    selections[random.randint(0, len(selections) - 1)]).click().perform()
                time.sleep(5)
        except:
            list_fail.append('Unable to select another week')

        try:
            with allure.step('Check date name header'):
                # check table header
                for i in driver.find_elements_by_css_selector('table.scheduler-bg-table> * th'):
                    self.assertTrue(i.find_element_by_css_selector('* span.day-name').text in
                                    ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN'])
        except:
            list_fail.append("The header is not displayed as ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN']")

        try:
            with allure.step('Check date text header'):
                # check table header
                for i in driver.find_elements_by_css_selector('table.scheduler-bg-table> * th'):
                    self.assertTrue(datetime.strptime(i.find_element_by_css_selector('* span.day-num').text, '%d/%m'))
        except:
            list_fail.append('The date format is not DD/MM')

        try:
            with allure.step('Check user Alexiel'):
                # check current user name
                scroll_to(driver, driver.find_element_by_xpath("//div[contains(text(), 'Alexiel')]"))
                time.sleep(0.2)
        except:
            list_fail.append('Username Alexiel is not available')

        try:
            with allure.step('Check text Total'):
                # check text [Total: x/40]
                total_text = ''
                for i in driver.find_elements_by_xpath(
                        "//div[contains(text(), 'Alexiel')]/../../following-sibling::div[2]/span"):
                    total_text = total_text + i.text
                self.assertTrue('Total:' in total_text and '/40' in total_text)
        except:
            list_fail.append('The Total text is displayed in wrong format')

        self.assertEqual(len(list_fail), 0, list_fail)


class LPanel_Header(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.driver = webdriver.Chrome('chromedriver.exe')
        driver = self.driver
        driver.maximize_window()

        # Log in
        login(driver)
        time.sleep(5)

        # Moving to test page
        driver.get(url_root + '/project-visibility/work-report')
        time.sleep(10)

    @classmethod
    def tearDownClass(self):
        self.driver.quit()

    @allure.story('Check HVNJira tab')
    def test_WR_HVNJira(self):
        driver = self.driver
        list_fail = []

        driver.refresh()
        time.sleep(5)

        try:
            with allure.step('Check Search issues text box'):
                # check Search issues text box
                self.assertTrue(len(driver.find_elements_by_css_selector('div.main-left> * div.input-wrap>input')) > 0)
        except:
            list_fail.append('No Search issues text box')

        try:
            with allure.step('Check Search icon'):
                # check Search icon
                self.assertTrue(len(driver.find_elements_by_css_selector('span.icon-magnifying')) > 0)
        except:
            list_fail.append('No Search icon')

        try:
            with allure.step('Check Filter icon'):
                # check Filter icon
                self.assertTrue(len(driver.find_elements_by_css_selector('div.main-left> * span.icon-filter_list')) > 0)
        except:
            list_fail.append('No Filter icon')

        try:
            with allure.step('Check Project frame availability'):
                self.assertTrue(len(driver.find_elements_by_css_selector('div.main-left>div.bottom>div>div')) > 0)
        except:
            list_fail.append('The Project frame is not available at the moment')
        self.assertEqual(len(list_fail), 0, list_fail)

        try:
            with allure.step('Check project name/task text'):
                # check project name + task text
                for i in driver.find_elements_by_css_selector('div.main-left>div.bottom>div>div>div>div:first-child'):
                    self.assertTrue(i.text is not '')
        except:
            list_fail.append('A project does not have name/task')

        try:
            with allure.step('Check sort buttons for each project'):
                # check 2 sort buttons for each project
                for i in driver.find_elements_by_css_selector('div.main-left>div.bottom>div>div>div>div:first-child'):
                    scroll_to(driver, i)
                    time.sleep(1)

                    self.assertTrue(len(i.find_elements_by_css_selector('span.icon-triangle-up')) > 0)
                    time.sleep(0.1)

                    self.assertTrue(len(i.find_elements_by_css_selector('span.icon-triangle-down')) > 0)
                    time.sleep(0.1)
        except:
            list_fail.append('The sort button is missing in project ' + i.text)

        try:
            with allure.step('Check task for each project'):
                # check list task for each project
                for i in driver.find_elements_by_css_selector(
                        'div.main-left>div.bottom>div>div>div>div:first-child+div'):
                    scroll_to(driver, i)
                    time.sleep(1)

                    self.assertTrue(len(i.find_elements_by_css_selector('span')) > 0)
                    time.sleep(0.1)
        except:
            list_fail.append('The project ' + i.text + ' contains no task')

        # check scroll bar
        try:
            with allure.step('Verify Vertical scrollbar'):
                offsetHeight = int(
                    driver.find_element_by_css_selector('div.main-left>div.bottom').get_property('offsetHeight'))
                scrollHeight = int(
                    driver.find_element_by_css_selector('div.main-left>div.bottom').get_property('scrollHeight'))

                self.assertTrue(scrollHeight > offsetHeight)
                time.sleep(0.01)
        except:
            list_fail.append('No Vertical scrollbar')
            pass

        self.assertEqual(len(list_fail), 0, list_fail)

    @allure.story('Check WJira tab')
    def test_WR_WJira(self):
        driver = self.driver
        list_fail = []

        driver.refresh()
        time.sleep(5)

        try:
            with allure.step('Check WJira button'):
                # check WJira button
                self.assertTrue(
                    len(driver.find_elements_by_css_selector('div.main-left>div:first-child>div:nth-child(2)')) > 0)
        except:
            list_fail.append('No WJira button')

        try:
            with allure.step('Click WJira button'):
                # click WJira button
                ActionChains(driver).move_to_element(
                    driver.find_element_by_css_selector('div.main-left>div:first-child>div:nth-child(2)')) \
                    .click().perform()
                time.sleep(40)
        except:
            list_fail.append('Unable to click WJira button')

        try:
            with allure.step('Check Project frame availability'):
                self.assertTrue(len(driver.find_elements_by_css_selector('div.main-left>div.bottom>div>div')) > 0)
        except:
            list_fail.append('The Project frame is not available at the moment')

        self.assertEqual(len(list_fail), 0, list_fail)

        try:
            with allure.step('Check project name/task text'):
                # check project name + task text
                for i in driver.find_elements_by_css_selector('div.main-left>div.bottom>div>div>div>div:first-child'):
                    self.assertTrue(i.text is not '')
        except:
            list_fail.append('A project does not have name/task')

        try:
            with allure.step('Check sort buttons for each project'):
                # check 2 sort buttons for each project
                for i in driver.find_elements_by_css_selector('div.main-left>div.bottom>div>div>div>div:first-child'):
                    scroll_to(driver, i)
                    time.sleep(1)

                    self.assertTrue(len(i.find_elements_by_css_selector('span.icon-triangle-up')) > 0)
                    time.sleep(0.1)

                    self.assertTrue(len(i.find_elements_by_css_selector('span.icon-triangle-down')) > 0)
                    time.sleep(0.1)
        except:
            list_fail.append('The sort button is missing in project ' + i.text)

        try:
            with allure.step('Check task for each project'):
                # check list task for each project
                for i in driver.find_elements_by_css_selector(
                        'div.main-left>div.bottom>div>div>div>div:first-child+div'):
                    scroll_to(driver, i)
                    time.sleep(1)

                    self.assertTrue(len(i.find_elements_by_css_selector('span')) > 0)
                    time.sleep(0.1)
        except:
            list_fail.append('The project ' + i.text + ' contains no task')

        # check scroll bar
        try:
            with allure.step('Verify Vertical scrollbar'):
                offsetHeight = int(
                    driver.find_element_by_css_selector('div.main-left>div.bottom').get_property('offsetHeight'))
                scrollHeight = int(
                    driver.find_element_by_css_selector('div.main-left>div.bottom').get_property('scrollHeight'))

                self.assertTrue(scrollHeight > offsetHeight)
                time.sleep(0.01)
        except:
            list_fail.append('No Vertical scrollbar')
            pass

        self.assertEqual(len(list_fail), 0, list_fail)

    @allure.story('Check HQJira tab')
    def test_WR_HQJira(self):
        driver = self.driver
        list_fail = []

        driver.refresh()
        time.sleep(5)

        try:
            with allure.step('Check HQJira button'):
                # check HQJira button
                self.assertTrue(
                    len(driver.find_elements_by_css_selector('div.main-left>div:first-child>div:nth-child(3)')) > 0)
        except:
            list_fail.append('No HQJira button')

        try:
            with allure.step('Click HQJira button'):
                # click HQJira button
                ActionChains(driver).move_to_element(
                    driver.find_element_by_css_selector('div.main-left>div:first-child>div:nth-child(3)')) \
                    .click().perform()
                time.sleep(40)
        except:
            list_fail.append('Unable to click HQJira button')

        try:
            with allure.step('Check Project frame availability'):
                self.assertTrue(len(driver.find_elements_by_css_selector('div.main-left>div.bottom>div>div')) > 0)
        except:
            list_fail.append('The Project frame is not available at the moment')

        self.assertEqual(len(list_fail), 0, list_fail)

        try:
            with allure.step('Check project name/task text'):
                # check project name + task text
                for i in driver.find_elements_by_css_selector('div.main-left>div.bottom>div>div>div>div:first-child'):
                    self.assertTrue(i.text is not '')
        except:
            list_fail.append('A project does not have name/task')

        try:
            with allure.step('Check sort buttons for each project'):
                # check 2 sort buttons for each project
                for i in driver.find_elements_by_css_selector('div.main-left>div.bottom>div>div>div>div:first-child'):
                    scroll_to(driver, i)
                    time.sleep(1)

                    self.assertTrue(len(i.find_elements_by_css_selector('span.icon-triangle-up')) > 0)
                    time.sleep(0.1)

                    self.assertTrue(len(i.find_elements_by_css_selector('span.icon-triangle-down')) > 0)
                    time.sleep(0.1)
        except:
            list_fail.append('The sort button is missing in project ' + i.text)

        try:
            with allure.step('Check task for each project'):
                # check list task for each project
                for i in driver.find_elements_by_css_selector(
                        'div.main-left>div.bottom>div>div>div>div:first-child+div'):
                    scroll_to(driver, i)
                    time.sleep(1)

                    self.assertTrue(len(i.find_elements_by_css_selector('span')) > 0)
                    time.sleep(0.1)
        except:
            list_fail.append('The project ' + i.text + ' contains no task')

        # check scroll bar
        try:
            with allure.step('Verify Vertical scrollbar'):
                offsetHeight = int(
                    driver.find_element_by_css_selector('div.main-left>div.bottom').get_property('offsetHeight'))
                scrollHeight = int(
                    driver.find_element_by_css_selector('div.main-left>div.bottom').get_property('scrollHeight'))

                self.assertTrue(scrollHeight > offsetHeight)
                time.sleep(0.01)
        except:
            list_fail.append('No Vertical scrollbar')
            pass

        self.assertEqual(len(list_fail), 0, list_fail)


class RPanel_Header(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.driver = webdriver.Chrome('chromedriver.exe')
        driver = self.driver
        driver.maximize_window()

        # Log in
        login(driver)
        time.sleep(5)

        # Moving to test page
        driver.get(url_root + '/project-visibility/work-report')
        time.sleep(10)

    @classmethod
    def tearDownClass(self):
        self.driver.quit()

    @allure.story('Check Report page')
    def test_WR_Report_page(self):
        driver = self.driver
        list_fail = []

        driver.refresh()
        time.sleep(5)

        try:
            with allure.step('Check Report button'):
                # verify Report button
                self.assertTrue(len(driver.find_elements_by_css_selector(
                    'div.main-right>div.top-header> * div.footer>span>div.report')) > 0)
        except:
            list_fail.append('No Report button')

        self.assertEqual(len(list_fail), 0, list_fail)

        try:
            with allure.step('Click on Report button'):
                # click Report button
                ActionChains(driver).move_to_element(driver.find_element_by_css_selector(
                    'div.main-right>div.top-header> * div.footer>span>div.report')).click().perform()
                time.sleep(7)
        except:
            list_fail.append('Unable to click on Report button')

        try:
            with allure.step('Verify new window is opened'):
                # verify new window is opened
                self.assertTrue(len(driver.window_handles) == 2)
        except:
            list_fail.append('No new window is opened')

        self.assertEqual(len(list_fail), 0, list_fail)

        try:
            with allure.step('Switch to new window'):
                # switch to popup
                driver.switch_to.window(driver.window_handles[1])
        except:
            list_fail.append('Unable to switch to new window')

        self.assertEqual(len(list_fail), 0, list_fail)

        try:
            with allure.step('Check Employee label availability'):
                # check Employee label availability
                self.assertTrue(len(driver.find_elements_by_css_selector('div.assignee-wrap>div.title')) > 0)
        except:
            list_fail.append('Label Employee is not available')

        try:
            with allure.step('Check Employee label content'):
                # check Employee label content
                self.assertEqual(driver.find_element_by_css_selector('div.assignee-wrap>div.title').text, 'EMPLOYEE')
        except:
            list_fail.append('Label is displayed as ' +
                             driver.find_element_by_css_selector('div.assignee-wrap>div.title').text
                             + ' instead of [EMPLOYEE]')

        try:
            with allure.step(''):
                # check avatar
                self.assertTrue(len(
                    driver.find_elements_by_css_selector('div.assignee-wrap>div:nth-child(2)> * span.ant-avatar')) > 0)
        except:
            list_fail.append('Avatar area is not available')

        try:
            with allure.step('Check Fullname availability'):
                # check fullname availability
                self.assertTrue(len(
                    driver.find_elements_by_css_selector('div.assignee-wrap>div:nth-child(2)> * div.fullname')) > 0)
        except:
            list_fail.append('No Full name')

        try:
            with allure.step('Check Fullname content'):
                # check fullname content
                self.assertTrue(driver.find_element_by_css_selector(
                    'div.assignee-wrap>div:nth-child(2)> * div.fullname').text is not '')
        except:
            list_fail.append('The Full name is blank')

        try:
            with allure.step('Check English name availability'):
                # check english name availability
                self.assertTrue(len(driver.find_elements_by_css_selector(
                    'div.assignee-wrap>div:nth-child(2)> * div.engname')) > 0)
        except:
            list_fail.append('No English name')

        try:
            with allure.step('Check English name content'):
                # check english name content
                self.assertTrue(driver.find_element_by_css_selector(
                    'div.assignee-wrap>div:nth-child(2)> * div.engname').text is not '')
        except:
            list_fail.append('The English name is blank')

        try:
            with allure.step('Check Organization label availability'):
                # check Organization label availability
                self.assertTrue(len(driver.find_elements_by_css_selector(
                    'div.assignee-wrap>div:nth-child(3)>div.label')) > 0)
        except:
            list_fail.append('No Organization label')

        try:
            with allure.step('Check Organization label content'):
                # check Organization label content
                self.assertEqual(driver.find_element_by_css_selector(
                    'div.assignee-wrap>div:nth-child(3)>div.label').text, 'Organization')
        except:
            list_fail.append('Label is displayed as ' +
                             driver.find_element_by_css_selector('div.assignee-wrap>div:nth-child(3)>div.label').text
                             + ' instead of [Organization]')

        try:
            with allure.step('Check Work date label availability'):
                # check Work date label availability
                self.assertTrue(len(driver.find_elements_by_css_selector(
                    'div.assignee-wrap>div:nth-child(4)>div.label')) > 0)
        except:
            list_fail.append('No Work date label')

        try:
            with allure.step('Check Work date label content'):
                # check Work date label content
                self.assertEqual(driver.find_element_by_css_selector(
                    'div.assignee-wrap>div:nth-child(4)>div.label').text, 'Work Date')
        except:
            list_fail.append('Label [Work Date] is missing')

        try:
            with allure.step('Check Work date value'):
                # check value of Work date
                self.assertTrue(datetime.strptime(
                    driver.find_element_by_css_selector('span.day-text').text, '%d/%m/%Y'))
        except:
            list_fail.append('The Work date is not displayed in format DD/MM/YYYY')

        try:
            with allure.step('Check Daily Work label'):
                # check Daily Work Status Report label availability
                self.assertTrue(len(
                    driver.find_elements_by_css_selector('div.form-content>div>div:first-child>div>div.day-title')) > 0)
        except:
            list_fail.append('No Daily Work label')

        try:
            with allure.step('Check Daily Work Status Report label content'):
                # check Daily Work Status Report label content
                self.assertEqual(driver.find_element_by_css_selector(
                    'div.form-content>div>div:first-child>div>div.day-title').text, 'Daily Work Status Report')
        except:
            list_fail.append('Label is displayed as ' + driver.find_element_by_css_selector(
                'div.form-content>div>div:first-child>div>div.day-title').text + ' instead of [Daily Work Status '
                                                                                 'Report]')

        try:
            with allure.step('Check Daily Work value'):
                # check Daily Work value
                self.assertTrue(datetime.strptime(driver.find_element_by_css_selector(
                    'div.form-content>div>div:first-child>div>div.day-selecter>span').text, '%d/%m/%Y'))
        except:
            list_fail.append('The Daily Work date is not displayed in format DD/MM/YYYY')

        try:
            with allure.step('Check APPROVER label availability'):
                # check APPROVER label availability
                self.assertTrue(len(
                    driver.find_elements_by_css_selector('div.main-info-right>div>div:nth-child(2)> * label')) > 0)
        except:
            list_fail.append('No APPROVER label')

        try:
            with allure.step('Check APPROVER label content'):
                # check APPROVER label content
                self.assertEqual(driver.find_element_by_css_selector(
                    'div.main-info-right>div>div:nth-child(2)> * label').text, 'APPROVER')
        except:
            list_fail.append('Label is displayed as ' + driver.find_element_by_css_selector(
                'div.main-info-right>div>div:nth-child(2)> * label').text + ' instead of [APPROVER]')

        try:
            with allure.step('Check Reference Report label availability'):
                # check Reference Report label availability
                self.assertTrue(len(
                    driver.find_elements_by_css_selector('div.main-info-right>div>div:nth-child(3)> * label')) > 0)
        except:
            list_fail.append('No Reference Report label')

        try:
            with allure.step('Check Reference Report label content'):
                # check Reference Report label content
                self.assertEqual(driver.find_element_by_css_selector(
                    'div.main-info-right>div>div:nth-child(3)> * label').text, 'Reference Report')
        except:
            list_fail.append('Label is displayed as ' + driver.find_element_by_css_selector(
                'div.main-info-right>div>div:nth-child(3)> * label').text + ' instead of [Reference Report]')

        try:
            with allure.step('Check Work Time text'):
                # check Work Time text
                self.assertEqual(driver.find_element_by_css_selector('div.worktime-wrap>div.left-side').text,
                                 'Work Time 0/8 Hour(s)')
        except:
            list_fail.append('Text is displayed as ' +
                             driver.find_element_by_css_selector('div.worktime-wrap>div.left-side').text
                             + ' instead of [Work Time 0/8 Hour(s)]')

        try:
            with allure.step('Check Task text'):
                # check Task text
                self.assertEqual(driver.find_element_by_css_selector('div.worktime-wrap>div.right-side').text,
                                 'Task 0 Task(s)')
        except:
            list_fail.append('Text is displayed as ' +
                             driver.find_element_by_css_selector('div.worktime-wrap>div.right-side').text
                             + ' instead of [Task 0 Task(s)]')

        try:
            with allure.step('Check Submit button'):
                # check Submit button
                self.assertTrue(len(driver.find_elements_by_css_selector(
                    'div.form-function>button[type="submit"]')) > 0)
        except:
            list_fail.append('No Submit button')

        try:
            with allure.step('Close the window'):
                # close popup
                driver.close()
                time.sleep(0.5)
        except:
            list_fail.append('Unable to close window')

        try:
            with allure.step('Verify window is closed'):
                # verify popup is close
                self.assertTrue(len(driver.window_handles) == 1)
                driver.switch_to.window(driver.window_handles[0])
        except:
            list_fail.append('Window is not closed')

        self.assertEqual(len(list_fail), 0, list_fail)

    @allure.story('Check Submit Report now page')
    def test_WR_Submit_Report_now_page(self):
        driver = self.driver
        list_fail = []

        driver.refresh()
        time.sleep(5)

        try:
            with allure.step('Check Submit Report now link'):
                # verify Submit Report now link
                self.assertTrue(len(driver.find_elements_by_css_selector('div.save-draft')) > 0)
        except:
            list_fail.append('No Submit Report now link')

        self.assertEqual(len(list_fail), 0, list_fail)

        try:
            with allure.step('Click on Submit Report now link'):
                # click Submit Report now link
                ActionChains(driver).move_to_element(driver.find_element_by_css_selector(
                    'div.save-draft')).click().perform()
                time.sleep(7)
        except:
            list_fail.append('Unable to click on Submit Report now link')

        try:
            with allure.step('Verify new window is opened'):
                # verify new window is opened
                self.assertTrue(len(driver.window_handles) == 2)
        except:
            list_fail.append('No new window is opened')

        self.assertEqual(len(list_fail), 0, list_fail)

        try:
            with allure.step('Switch to new window'):
                # switch to popup
                driver.switch_to.window(driver.window_handles[1])
        except:
            list_fail.append('Unable to switch to new window')

        self.assertEqual(len(list_fail), 0, list_fail)

        try:
            with allure.step('Check Employee label availability'):
                # check Employee label availability
                self.assertTrue(len(driver.find_elements_by_css_selector('div.assignee-wrap>div.title')) > 0)
        except:
            list_fail.append('Label Employee is not available')

        try:
            with allure.step('Check Employee label content'):
                # check Employee label content
                self.assertEqual(driver.find_element_by_css_selector('div.assignee-wrap>div.title').text, 'EMPLOYEE')
        except:
            list_fail.append('Label is displayed as ' +
                             driver.find_element_by_css_selector('div.assignee-wrap>div.title').text
                             + ' instead of [EMPLOYEE]')

        try:
            with allure.step(''):
                # check avatar
                self.assertTrue(len(
                    driver.find_elements_by_css_selector('div.assignee-wrap>div:nth-child(2)> * span.ant-avatar')) > 0)
        except:
            list_fail.append('Avatar area is not available')

        try:
            with allure.step('Check Fullname availability'):
                # check fullname availability
                self.assertTrue(len(
                    driver.find_elements_by_css_selector('div.assignee-wrap>div:nth-child(2)> * div.fullname')) > 0)
        except:
            list_fail.append('No Full name')

        try:
            with allure.step('Check Fullname content'):
                # check fullname content
                self.assertTrue(driver.find_element_by_css_selector(
                    'div.assignee-wrap>div:nth-child(2)> * div.fullname').text is not '')
        except:
            list_fail.append('The Full name is blank')

        try:
            with allure.step('Check English name availability'):
                # check english name availability
                self.assertTrue(len(driver.find_elements_by_css_selector(
                    'div.assignee-wrap>div:nth-child(2)> * div.engname')) > 0)
        except:
            list_fail.append('No English name')

        try:
            with allure.step('Check English name content'):
                # check english name content
                self.assertTrue(driver.find_element_by_css_selector(
                    'div.assignee-wrap>div:nth-child(2)> * div.engname').text is not '')
        except:
            list_fail.append('The English name is blank')

        try:
            with allure.step('Check Organization label availability'):
                # check Organization label availability
                self.assertTrue(len(driver.find_elements_by_css_selector(
                    'div.assignee-wrap>div:nth-child(3)>div.label')) > 0)
        except:
            list_fail.append('No Organization label')

        try:
            with allure.step('Check Organization label content'):
                # check Organization label content
                self.assertEqual(driver.find_element_by_css_selector(
                    'div.assignee-wrap>div:nth-child(3)>div.label').text, 'Organization')
        except:
            list_fail.append('Label is displayed as ' +
                             driver.find_element_by_css_selector('div.assignee-wrap>div:nth-child(3)>div.label').text
                             + ' instead of [Organization]')

        try:
            with allure.step('Check Work date label availability'):
                # check Work date label availability
                self.assertTrue(len(driver.find_elements_by_css_selector(
                    'div.assignee-wrap>div:nth-child(4)>div.label')) > 0)
        except:
            list_fail.append('No Work date label')

        try:
            with allure.step('Check Work date label content'):
                # check Work date label content
                self.assertEqual(driver.find_element_by_css_selector(
                    'div.assignee-wrap>div:nth-child(4)>div.label').text, 'Work Date')
        except:
            list_fail.append('Label is displayed as ' + driver.find_element_by_css_selector(
                'div.assignee-wrap>div:nth-child(4)>div.label').text + ' instead of [Work Date]')

        try:
            with allure.step('Check Work date value'):
                # check value of Work date
                self.assertTrue(datetime.strptime(driver.find_element_by_css_selector(
                    'div.assignee-wrap>div:nth-child(4)>div.value').text, '%d/%m/%Y'))
        except:
            list_fail.append('The Work date is not displayed in format DD/MM/YYYY')

        try:
            with allure.step('Check Daily Work label'):
                # check Daily Work Status Report label availability
                self.assertTrue(len(
                    driver.find_elements_by_css_selector('div.form-content>div>div:first-child>div>div.day-title')) > 0)
        except:
            list_fail.append('No Daily Work label')

        try:
            with allure.step('Check Daily Work Status Report label content'):
                # check Daily Work Status Report label content
                self.assertEqual(driver.find_element_by_css_selector(
                    'div.form-content>div>div:first-child>div>div.day-title').text, 'Daily Work Status Report')
        except:
            list_fail.append('Label is displayed as ' + driver.find_element_by_css_selector(
                'div.form-content>div>div:first-child>div>div.day-title').text + ' instead of [Daily Work Status '
                                                                                 'Report]')

        try:
            with allure.step('Check Daily Work value'):
                # check Daily Work value
                self.assertTrue(datetime.strptime(driver.find_element_by_css_selector(
                    'div.form-content>div>div:first-child>div>div.day-selecter>span').text, '%d/%m/%Y'))
        except:
            list_fail.append('The Daily Work date is not displayed in format DD/MM/YYYY')

        try:
            with allure.step('Check APPROVER label availability'):
                # check APPROVER label availability
                self.assertTrue(len(
                    driver.find_elements_by_css_selector('div.main-info-right>div>div:nth-child(2)> * label')) > 0)
        except:
            list_fail.append('No APPROVER label')

        try:
            with allure.step('Check APPROVER label content'):
                # check APPROVER label content
                self.assertEqual(driver.find_element_by_css_selector(
                    'div.main-info-right>div>div:nth-child(2)> * label').text, 'APPROVER')
        except:
            list_fail.append('Label is displayed as ' + driver.find_element_by_css_selector(
                'div.main-info-right>div>div:nth-child(2)> * label').text + ' instead of [APPROVER]')

        try:
            with allure.step('Check Reference Report label availability'):
                # check Reference Report label availability
                self.assertTrue(len(
                    driver.find_elements_by_css_selector('div.main-info-right>div>div:nth-child(3)> * label')) > 0)
        except:
            list_fail.append('No Reference Report label')

        try:
            with allure.step('Check Reference Report label content'):
                # check Reference Report label content
                self.assertEqual(driver.find_element_by_css_selector(
                    'div.main-info-right>div>div:nth-child(3)> * label').text, 'Reference Report')
        except:
            list_fail.append('Label is displayed as ' + driver.find_element_by_css_selector(
                'div.main-info-right>div>div:nth-child(3)> * label').text + ' instead of [Reference Report]')

        try:
            with allure.step('Check Work Time text'):
                # check Work Time text
                self.assertEqual(driver.find_element_by_css_selector('div.worktime-wrap>div.left-side').text,
                                 'Work Time 0/8 Hour(s)')
        except:
            list_fail.append('Text is displayed as ' +
                             driver.find_element_by_css_selector('div.worktime-wrap>div.left-side').text
                             + ' instead of [Work Time 0/8 Hour(s)]')

        try:
            with allure.step('Check Task text'):
                # check Task text
                self.assertEqual(driver.find_element_by_css_selector('div.worktime-wrap>div.right-side').text,
                                 'Task 0 Task(s)')
        except:
            list_fail.append('Text is displayed as ' +
                             driver.find_element_by_css_selector('div.worktime-wrap>div.right-side').text
                             + ' instead of [Task 0 Task(s)]')

        try:
            with allure.step('Check Submit button'):
                # check Submit button
                self.assertTrue(len(driver.find_elements_by_css_selector(
                    'div.form-function>button[type="submit"]')) > 0)
        except:
            list_fail.append('No Submit button')

        try:
            with allure.step('Close the window'):
                # close popup
                driver.close()
                time.sleep(0.5)
        except:
            list_fail.append('Unable to close window')

        try:
            with allure.step('Verify window is closed'):
                # verify popup is close
                self.assertTrue(len(driver.window_handles) == 1)
                driver.switch_to.window(driver.window_handles[0])
        except:
            list_fail.append('Window is not closed')

        self.assertEqual(len(list_fail), 0, list_fail)


class LPanel_Body(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.driver = webdriver.Chrome('chromedriver.exe')
        driver = self.driver
        driver.maximize_window()

        # Log in
        login(driver)
        time.sleep(5)

        # Moving to test page
        driver.get(url_root + '/project-visibility/work-report')
        time.sleep(10)

    @classmethod
    def tearDownClass(self):
        self.driver.quit()

    @allure.story('Check Report page')
    def test_WR_Sort_project_name(self):
        driver = self.driver
        list_fail = []

        driver.refresh()
        time.sleep(5)

        try:
            with allure.step('Check project QUAL availability'):
                # check project QUAL availability
                self.assertTrue(len(driver.find_elements_by_xpath('//div[contains(text(), "QUAL")]')) > 0)
        except:
            list_fail.append('The QUAL project is not available, skipping test')

        self.assertEqual(len(list_fail), 0, list_fail)

        try:
            with allure.step('Check sort buttons for QUAL project'):
                # check 2 sort buttons for each project
                self.assertTrue(len(driver.find_elements_by_xpath(
                    '//div[contains(text(), "QUAL")]/div/span[@class="icon-triangle-up"]')) > 0)
                time.sleep(0.1)

                self.assertTrue(len(driver.find_elements_by_xpath(
                    '//div[contains(text(), "QUAL")]/div/span[@class="icon-triangle-down"]')) > 0)
                time.sleep(0.1)
        except:
            list_fail.append('The sort button is missing in project QUAL')

        self.assertEqual(len(list_fail), 0, list_fail)

        try:
            with allure.step('Fetch list issue from Jira of QUAL project'):
                # fetch task list for QUAL
                jira_tasks = []
                for i in driver.find_elements_by_xpath('//div[contains(text(), "QUAL")]/following-sibling::div//a'):
                    scroll_to(driver, i)
                    jira_tasks.append(i.text)
                    time.sleep(0.05)

                # jira sort up
                jira_tasks.sort()

                # scroll to Sort up button then click
                scroll_to(driver, driver.find_element_by_xpath(
                    '//div[contains(text(), "QUAL")]/div/span[@class="icon-triangle-up"]'))
                ActionChains(driver).move_to_element(driver.find_element_by_xpath(
                    '//div[contains(text(), "QUAL")]/div/span[@class="icon-triangle-up"]')).click().perform()
                time.sleep(0.5)
        except:
            list_fail.append('An error occurs when clicking on Sort up button')

        try:
            with allure.step('Check Sort up button color after clicking'):
                # check Sort up button color
                self.assertEqual(rbga_to_hex(driver.find_element_by_xpath(
                    '//div[contains(text(), "QUAL")]/div/span[contains(@class, "icon-triangle-up")]')
                                             .value_of_css_property('color')).upper(), '#427FEC')
        except:
            list_fail.append('After clicking, the Sort up button is highlighted with incorrect color')

        try:
            with allure.step('Compare Sort up list'):
                # fetch the actual list after clicking Sort up
                list_sort_up = []
                for i in driver.find_elements_by_xpath('//div[contains(text(), "QUAL")]/following-sibling::div//a'):
                    scroll_to(driver, i)
                    list_sort_up.append(i.text)
                    time.sleep(0.05)

                # compare Sort up result
                self.assertEqual(list_sort_up, jira_tasks)
                time.sleep(1)
        except:
            list_fail.append('Sort up feature works incorrectly')

        try:
            with allure.step('Click on Sort down button'):
                # scroll to Sort down button then click
                scroll_to(driver, driver.find_element_by_xpath(
                    '//div[contains(text(), "QUAL")]/div/span[@class="icon-triangle-down"]'))
                ActionChains(driver).move_to_element(driver.find_element_by_xpath(
                    '//div[contains(text(), "QUAL")]/div/span[@class="icon-triangle-down"]')).click().perform()
                time.sleep(0.5)

            # fetch the actual list after clicking Sort down
            list_sort_down = []
            for i in driver.find_elements_by_xpath('//div[contains(text(), "QUAL")]/following-sibling::div//a'):
                scroll_to(driver, i)
                list_sort_down.append(i.text)
                time.sleep(0.05)
        except:
            list_fail.append('An error occurs when clicking on Sort up button')

        try:
            with allure.step('Check Sort down button color after clicking'):
                # check Sort down button color
                self.assertEqual(rbga_to_hex(driver.find_element_by_xpath(
                    '//div[contains(text(), "QUAL")]/div/span[contains(@class, "icon-triangle-down")]')
                                             .value_of_css_property('color')).upper(), '#427FEC')
        except:
            list_fail.append('After clicking, the Sort down button is highlighted with incorrect color')

        try:
            with allure.step('Compare Sort down list'):
                # jira sort down
                jira_tasks.sort(reverse=True)

                # compare Sort down result
                self.assertEqual(list_sort_down, jira_tasks)
        except:
            list_fail.append('Sort down feature works incorrectly')

        self.assertEqual(len(list_fail), 0, list_fail)

    @allure.story('Check Filter Options Assignee')
    def test_WR_Filter_Options_Assignee(self):
        driver = self.driver
        list_fail = []

        # driver.refresh()
        # time.sleep(5)

        try:
            with allure.step('Check Filter button'):
                # verify Filter button
                self.assertTrue(len(driver.find_elements_by_css_selector(
                    'div.main-right> * span.icon-filter_list')) > 0)
        except:
            list_fail.append('No Filter button')

        try:
            with allure.step('Click Filter button'):
                # click Filter button
                ActionChains(driver).move_to_element(driver.find_element_by_css_selector(
                    'div.main-right> * span.icon-filter_list')).click().perform()
                time.sleep(0.5)
        except:
            list_fail.append('Unable to click on Filter button')

        try:
            with allure.step('Check dropdown'):
                # check dropdown
                self.assertTrue(len(driver.find_elements_by_css_selector(
                    'div.main-right> * div.search-filter-wrapper')) > 0)
        except:
            list_fail.append('No dropdown appears after clicking on Filter button')

        try:
            with allure.step('Check Member option'):
                # check Project Name option
                self.assertTrue(len(driver.find_elements_by_css_selector(
                    'div.main-right> * div.search-filter-wrapper>div:last-child>div>div')) > 0)
        except:
            list_fail.append('No Member option')

        try:
            with allure.step('Click Member option'):
                # click Project Name
                ActionChains(driver).move_to_element(driver.find_element_by_css_selector(
                    'div.main-right> * div.search-filter-wrapper>div:last-child>div>div')).click().perform()
                time.sleep(0.5)
        except:
            list_fail.append('Unable to click on Member')

        try:
            with allure.step('Select random member'):
                driver.find_element_by_css_selector('input.input-search-menu-list').send_keys('Alexiel')
                time.sleep(1)
                ActionChains(driver).move_to_element(driver.find_element_by_css_selector(
                    'div.ReactVirtualized__Grid__innerScrollContainer> * span.ant-checkbox')).click().perform()
                time.sleep(1)
        except:
            list_fail.append('Unable to select project visibility')

        try:
            with allure.step('Verify list employee'):
                # click to HVN Jira to dismiss dropdown
                ActionChains(driver).click(driver.find_element_by_css_selector(
                    'div.main-left>div:first-child>div:first-child')).perform()
                time.sleep(3)

                # verify list employee working on project > 0
                self.assertTrue(len(driver.find_elements_by_css_selector(
                    'table.resource-table>tbody>tr>td> * div.text-primary.text-bold')) == 1)
        except:
            list_fail.append('There is no employee')

        self.assertEqual(len(list_fail), 0, list_fail)

    @allure.story('Check Filter Options Clear all')
    def test_WR_Filter_Options_Clear_all(self):
        driver = self.driver
        list_fail = []

        driver.refresh()
        time.sleep(5)

        try:
            with allure.step('Check Filter button'):
                # verify Filter button
                self.assertTrue(len(driver.find_elements_by_css_selector(
                    'div.main-right> * span.icon-filter_list')) > 0)
        except:
            list_fail.append('No Filter button')

        try:
            with allure.step('Click Filter button'):
                # click Filter button
                ActionChains(driver).move_to_element(driver.find_element_by_css_selector(
                    'div.main-right> * span.icon-filter_list')).click().perform()
                time.sleep(0.5)
        except:
            list_fail.append('Unable to click on Filter button')

        try:
            with allure.step('Check dropdown'):
                # check dropdown
                self.assertTrue(len(driver.find_elements_by_css_selector(
                    'div.main-right> * div.search-filter-wrapper')) > 0)
        except:
            list_fail.append('No dropdown appears after clicking on Filter button')

        try:
            with allure.step('Check Project Name option'):
                # check Project Name option
                self.assertTrue(len(driver.find_elements_by_css_selector(
                    'div.main-right> * div.search-filter-wrapper>div:first-child>div>div')) > 0)
        except:
            list_fail.append('No Project Name option')

        try:
            with allure.step('Click Project Name option'):
                # click Project Name
                ActionChains(driver).move_to_element(driver.find_element_by_css_selector(
                    'div.main-right> * div.search-filter-wrapper>div:first-child>div>div')).click().perform()
                time.sleep(0.5)
        except:
            list_fail.append('Unable to click on Project Name')

        try:
            with allure.step('Select project visibility'):
                # enter project name
                driver.find_element_by_css_selector('input.input-search-menu-list').send_keys('project visibility')

                ActionChains(driver).move_to_element(driver.find_element_by_css_selector(
                    'div.ReactVirtualized__Grid__innerScrollContainer>div')).click().perform()
                time.sleep(3)
        except:
            list_fail.append('Unable to select project visibility')

        try:
            with allure.step('Click Clear all link'):
                ActionChains(driver).move_to_element(driver.find_element_by_css_selector(
                    'div.main-right> * span.filter-header+span')).click().perform()
                time.sleep(0.5)
        except:
            list_fail.append('Unable to click Clear all link')

        try:
            with allure.step('Check the Project Name value'):
                self.assertEqual(driver.find_element_by_css_selector(
                    'div.main-right> * div.search-filter-wrapper>div:first-child> * '
                    'div.select__value-container>div').text, 'All')
        except:
            list_fail.append('The selected option is not cleared')

        self.assertEqual(len(list_fail), 0, list_fail)

    @allure.story('Check Filter System Project name')
    def test_WR_Filter_System_Project_name(self):
        driver = self.driver
        list_fail = []

        driver.refresh()
        time.sleep(5)

        try:
            with allure.step('Check Filter button'):
                # verify Filter button
                self.assertTrue(len(driver.find_elements_by_css_selector(
                    'div.main-left> * span.icon-filter_list')) > 0)
        except:
            list_fail.append('No Filter button')

        try:
            with allure.step('Click Filter button'):
                # click Filter button
                ActionChains(driver).move_to_element(driver.find_element_by_css_selector(
                    'div.main-left> * span.icon-filter_list')).click().perform()
                time.sleep(0.5)
        except:
            list_fail.append('Unable to click on Filter button')

        try:
            with allure.step('Check dropdown'):
                # check dropdown
                self.assertTrue(len(driver.find_elements_by_css_selector(
                    'div.main-left> * div.search-filter-wrapper')) > 0)
        except:
            list_fail.append('No dropdown appears after clicking on Filter button')

        # **************************************************************************************************************

        try:
            with allure.step('Check Project Name option'):
                # check Project Name option
                self.assertTrue(len(driver.find_elements_by_css_selector(
                    'div.main-left> * div.search-filter-wrapper>div:first-child>div>div')) > 0)
        except:
            list_fail.append('No Project Name option')

        try:
            with allure.step('Check Project Name default value'):
                # check Project Name default value
                self.assertEqual(driver.find_element_by_css_selector(
                    'div.main-left> * div.search-filter-wrapper>div:first-child> * '
                    'div.select__value-container>div').text, 'All')
        except:
            list_fail.append('The default value of Project Name is ' + driver.find_element_by_css_selector(
                'div.main-left> * div.search-filter-wrapper>div:first-child> * '
                'div.select__value-container>div').text)

        # **************************************************************************************************************
        try:
            with allure.step('Check Type option'):
                # check Type option
                self.assertTrue(len(driver.find_elements_by_css_selector(
                    'div.main-left> * div.search-filter-wrapper>div:nth-child(2)>div>div')) > 0)
        except:
            list_fail.append('No Type option')

        try:
            with allure.step('Check Type default value'):
                # check Type default value
                self.assertEqual(driver.find_element_by_css_selector(
                    'div.main-left> * div.search-filter-wrapper>div:nth-child(2)> * '
                    'div.select__value-container>div').text, 'All')
        except:
            list_fail.append('The default value of Type is ' + driver.find_element_by_css_selector(
                'div.main-left> * div.search-filter-wrapper>div:nth-child(2)> * '
                'div.select__value-container>div').text)

        # **************************************************************************************************************
        try:
            with allure.step('Check Status option'):
                # check Status option
                self.assertTrue(len(driver.find_elements_by_css_selector(
                    'div.main-left> * div.search-filter-wrapper>div:nth-child(3)>div>div')) > 0)
        except:
            list_fail.append('No Status option')

        try:
            with allure.step('Check Status default value'):
                # check Status default value
                self.assertEqual(driver.find_element_by_css_selector(
                    'div.main-left> * div.search-filter-wrapper>div:nth-child(3)> * '
                    'div.select__value-container>div').text, 'All')
        except:
            list_fail.append('The default value of Status is ' + driver.find_element_by_css_selector(
                'div.main-left> * div.search-filter-wrapper>div:nth-child(3)> * '
                'div.select__value-container>div').text)

        # **************************************************************************************************************
        try:
            with allure.step('Check Priority option'):
                # check Priority option
                self.assertTrue(len(driver.find_elements_by_css_selector(
                    'div.main-left> * div.search-filter-wrapper>div:nth-child(4)>div>div')) > 0)
        except:
            list_fail.append('No Priority option')

        try:
            with allure.step('Check Priority default value'):
                # check Priority default value
                self.assertEqual(driver.find_element_by_css_selector(
                    'div.main-left> * div.search-filter-wrapper>div:nth-child(4)> * '
                    'div.select__value-container>div').text, 'All')
        except:
            list_fail.append('The default value of Priority is ' + driver.find_element_by_css_selector(
                'div.main-left> * div.search-filter-wrapper>div:nth-child(4)> * '
                'div.select__value-container>div').text)

        # **************************************************************************************************************
        try:
            with allure.step('Check Resolution option'):
                # check Resolution option
                self.assertTrue(len(driver.find_elements_by_css_selector(
                    'div.main-left> * div.search-filter-wrapper>div:nth-child(5)>div>div')) > 0)
        except:
            list_fail.append('No Resolution option')

        try:
            with allure.step('Check Resolution default value'):
                # check Resolution default value
                self.assertEqual(driver.find_element_by_css_selector(
                    'div.main-left> * div.search-filter-wrapper>div:nth-child(5)> * '
                    'div.select__value-container>div').text, 'All')
        except:
            list_fail.append('The default value of Resolution is ' + driver.find_element_by_css_selector(
                'div.main-left> * div.search-filter-wrapper>div:nth-child(5)> * '
                'div.select__value-container>div').text)

        # **************************************************************************************************************
        try:
            with allure.step('Check Assignee option'):
                # check Assignee option
                self.assertTrue(len(driver.find_elements_by_css_selector(
                    'div.main-left> * div.search-filter-wrapper>div:nth-child(6)>div>div')) > 0)
        except:
            list_fail.append('No Assignee option')

        try:
            with allure.step('Check Assignee default value'):
                # check Assignee default value
                self.assertEqual(driver.find_element_by_css_selector(
                    'div.main-left> * div.search-filter-wrapper>'
                    'div:nth-child(6)>div>div>div>div:last-child').text, 'Me')
        except:
            list_fail.append('The default value of Assignee is ' + driver.find_element_by_css_selector(
                'div.main-left> * div.search-filter-wrapper>div:nth-child(6)>div>div>div>div:last-child').text)

        # **************************************************************************************************************
        try:
            with allure.step('Check Clear all link'):
                self.assertTrue(len(driver.find_elements_by_css_selector(
                    'div.main-left> * span.filter-header+span')) > 0)
                time.sleep(0.5)
        except:
            list_fail.append('No Clear all link')

        # **************************************************************************************************************
        try:
            with allure.step('Click Project Name option'):
                # click Project Name
                ActionChains(driver).move_to_element(driver.find_element_by_css_selector(
                    'div.main-left> * div.search-filter-wrapper>div:first-child>div>div')).click().perform()
                time.sleep(0.5)
        except:
            list_fail.append('Unable to click on Project Name')

        try:
            with allure.step('Check Search textbox'):
                # check Search textbox
                self.assertTrue(len(driver.find_elements_by_css_selector('input.input-search-menu-list')) > 0)
        except:
            list_fail.append('After clicking on Project Name option, no Search textbox is displayed')

        try:
            with allure.step('Verify check box of Project name'):
                self.assertTrue(len(driver.find_elements_by_css_selector(
                    'div.ReactVirtualized__Grid__innerScrollContainer>div')) > 0)
        except:
            list_fail.append('No check box under Project name')

        try:
            with allure.step('Select Quality'):
                # enter project name
                driver.find_element_by_css_selector('input.input-search-menu-list').send_keys('Quality')

                ActionChains(driver).move_to_element(driver.find_element_by_css_selector(
                    'div.ReactVirtualized__Grid__innerScrollContainer>div')).click().perform()
                time.sleep(3)
        except:
            list_fail.append('Unable to select Quality')

        try:
            with allure.step('Verify selection color'):
                # verify selection color
                self.assertEqual(driver.find_element_by_xpath(
                    '//div[@class="main-left"]//label[contains(text(), "Quality")]')
                                 .value_of_css_property('color'), 'rgba(66, 127, 236, 1)')
        except:
            list_fail.append('The selected option is displayed in incorrect color')

        # click to HVN Jira to dismiss dropdown
        ActionChains(driver).click(driver.find_element_by_css_selector(
            'div.main-left>div:first-child>div:first-child')).perform()
        time.sleep(3)

        try:
            with allure.step('Check project name/task text'):
                # check project name + task text
                self.assertTrue('QUAL' in driver.find_element_by_css_selector(
                    'div.main-left>div.bottom>div>div>div>div:first-child').text)
        except:
            list_fail.append('The result is not QUAL project')

        try:
            with allure.step('Check Jira task list'):
                # check Jira task list
                for i in driver.find_elements_by_css_selector(
                        'div.main-left>div.bottom>div>div>div>div:first-child+div> * a'):
                    scroll_to(driver, i)
                    self.assertTrue('QUAL' in i.text)
                    time.sleep(0.01)
        except:
            list_fail.append('The result is not QUAL project')

        self.assertEqual(len(list_fail), 0, list_fail)

    @allure.story('Check Filter System Clear all')
    def test_WR_Filter_System_Clear_all(self):
        driver = self.driver
        list_fail = []

        driver.refresh()
        time.sleep(5)

        try:
            with allure.step('Check Filter button'):
                # verify Filter button
                self.assertTrue(len(driver.find_elements_by_css_selector(
                    'div.main-left> * span.icon-filter_list')) > 0)
        except:
            list_fail.append('No Filter button')

        try:
            with allure.step('Click Filter button'):
                # click Filter button
                ActionChains(driver).move_to_element(driver.find_element_by_css_selector(
                    'div.main-left> * span.icon-filter_list')).click().perform()
                time.sleep(0.5)
        except:
            list_fail.append('Unable to click on Filter button')

        try:
            with allure.step('Check dropdown'):
                # check dropdown
                self.assertTrue(len(driver.find_elements_by_css_selector(
                    'div.main-left> * div.search-filter-wrapper')) > 0)
        except:
            list_fail.append('No dropdown appears after clicking on Filter button')

        try:
            with allure.step('Click Project Name option'):
                # click Project Name
                ActionChains(driver).move_to_element(driver.find_element_by_css_selector(
                    'div.main-left> * div.search-filter-wrapper>div:first-child>div>div')).click().perform()
                time.sleep(0.5)
        except:
            list_fail.append('Unable to click on Project Name')

        try:
            with allure.step('Select Quality'):
                # enter project name
                driver.find_element_by_css_selector('input.input-search-menu-list').send_keys('Quality')

                ActionChains(driver).move_to_element(driver.find_element_by_css_selector(
                    'div.ReactVirtualized__Grid__innerScrollContainer>div')).click().perform()
                time.sleep(3)
        except:
            list_fail.append('Unable to select Quality')

        try:
            with allure.step('Click Clear all link'):
                ActionChains(driver).move_to_element(driver.find_element_by_css_selector(
                    'div.main-left> * span.filter-header+span')).click().perform()
                time.sleep(0.5)
        except:
            list_fail.append('Unable to click Clear all link')

        try:
            with allure.step('Check Project Name default value'):
                # check Project Name default value
                self.assertEqual(driver.find_element_by_css_selector(
                    'div.main-left> * div.search-filter-wrapper>div:first-child> * '
                    'div.select__value-container>div').text, 'All')
        except:
            list_fail.append('The default value of Project Name is ' + driver.find_element_by_css_selector(
                'div.main-left> * div.search-filter-wrapper>div:first-child> * '
                'div.select__value-container>div').text)

        self.assertEqual(len(list_fail), 0, list_fail)

    @allure.story('Check feature Search issue ID')
    def test_WR_Search_issue_ID(self):
        driver = self.driver
        list_fail = []

        driver.refresh()
        time.sleep(5)

        # filter all issues assigned for user Alexiel
        Jira_tasks = []

        for i in api_GetTasksByQuery('tmtuan')['QueryResult']['items']:
            Jira_tasks.append(i['key'])

        try:
            with allure.step('Check Search issues text box'):
                # check Search issues text box
                self.assertTrue(len(driver.find_elements_by_css_selector('div.main-left> * div.input-wrap>input')) > 0)
        except:
            list_fail.append('No Search issues text box')

        with allure.step('Search 3 random issues'):
            # search 3 randoms issues
            for i in range(3):
                driver.find_element_by_css_selector('div.main-left> * div.input-wrap>input').clear()
                time.sleep(0.5)

                issue = Jira_tasks[random.randint(0, len(Jira_tasks) - 1)]
                driver.find_element_by_css_selector('div.main-left> * div.input-wrap>input')\
                    .send_keys(issue)
                time.sleep(0.5)

                ActionChains(driver).move_to_element(
                    driver.find_element_by_css_selector('span.icon-magnifying')).click().perform()
                time.sleep(5)

                try:
                    self.assertTrue(len(driver.find_elements_by_css_selector(
                        'div.main-left>div.bottom>div>div>div>div:first-child+div> * a')) > 0)
                except:
                    list_fail.append('Search returns failure at issue ' + issue)

        self.assertEqual(len(list_fail), 0, list_fail)


# class GUI(unittest.TestCase):
#
#     @classmethod
#     def setUpClass(self):
#         self.driver = webdriver.Chrome('chromedriver.exe')
#         driver = self.driver
#         driver.maximize_window()
#
#         # Log in
#         login(driver)
#         time.sleep(5)
#
#         # Moving to test page
#         driver.get(url_root + '/project-visibility/work-report')
#         time.sleep(10)
#
#     @classmethod
#     def tearDownClass(self):
#         self.driver.quit()
#
#     def test_WR_GUI_default(self):
#         driver = self.driver
#         list_fail = []
#
#         try:
#             with allure.step('Check HQJira button'):
#                 # check HQJira button
#                 self.assertTrue(
#                     len(driver.find_elements_by_css_selector('div.main-left>div:first-child>div:nth-child(3)')) > 0)
#                 self.assertEqual(driver.find_element_by_css_selector('div.main-left>div:first-child>div:nth-child('
#                                                                      '3)').text, 'HQ Jira')
#         except:
#             list_fail.append('No HQJira button')
#
#         try:
#             with allure.step('Check WJira button'):
#                 # check WJira button
#                 self.assertTrue(
#                     len(driver.find_elements_by_css_selector('div.main-left>div:first-child>div:nth-child(2)')) > 0)
#                 self.assertEqual(driver.find_element_by_css_selector('div.main-left>div:first-child>div:nth-child('
#                                                                      '2)').text, 'WJira')
#         except:
#             list_fail.append('No WJira button')
#
#         try:
#             with allure.step('Check HVNJira button'):
#                 # check WJira button
#                 self.assertTrue(
#                     len(driver.find_elements_by_css_selector('div.main-left>div:first-child>div:nth-child(1)')) > 0)
#                 self.assertEqual(driver.find_element_by_css_selector('div.main-left>div:first-child>div:nth-child('
#                                                                      '1)').text, 'HVN Jira')
#         except:
#             list_fail.append('No WJira button')
#
#         # check Feature Road Map
#         self.assertEqual(driver.find_element_by_css_selector('div.road-map>div:first-child').text, 'Feature Road Map')
#
#         # check Feature Road Map dropdown
#         self.assertTrue(len(driver.find_elements_by_css_selector('div.road-map>div:last-child>div')) > 0)
#
#         # check Days menu
#         self.assertTrue(len(driver.find_elements_by_css_selector('div.main-right>div.top-header>div:last-child')) > 0)
#
#         # check no of day in Days menu
#         actual = []
#         for i in driver.find_elements_by_css_selector('div.main-right>div.top-header>div:last-child> * span.name'):
#             actual.append(i.text)
#         self.assertEqual(actual, ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN'])
#
#         # check highlight today
#         self.assertEqual(rbga_to_hex(driver.find_element_by_css_selector(
#             'div.main-right>div.top-header>div:last-child>div.is-today')
#                                     .value_of_css_property('border-left-color')).upper(), '#7A8EBB')
#
#         self.assertEqual(driver.find_element_by_css_selector(
#             'div.main-right>div.top-header>div:last-child>div.is-today')
#                          .value_of_css_property('border-left-style'), 'dashed')
#
#         self.assertEqual(rbga_to_hex(driver.find_element_by_css_selector(
#             'div.main-right>div.top-header>div:last-child>div.is-today')
#                                      .value_of_css_property('border-right-color')).upper(), '#7A8EBB')
#
#         self.assertEqual(driver.find_element_by_css_selector(
#             'div.main-right>div.top-header>div:last-child>div.is-today')
#                          .value_of_css_property('border-right-style'), 'dashed')
#
#         # check label WEEKLY REPORT
#         self.assertEqual(len(driver.find_elements_by_css_selector('span.weekly-report')) > 0)
#
#         # check day of label
#         self.assertEqual(len(driver.find_elements_by_css_selector(
#             'div.main-right>div.top-header>div+div>div:nth-child(5)>span.weekly-report')) > 0)
#
#         # check hour label
#         for i in driver.find_elements_by_css_selector('div.main-right>div.top-header>div+div>div>div.hour'):
#             self.assertEqual(i.text, '0/8(h)')
#
#         self.assertEqual(len(list_fail), 0, list_fail)


if __name__ == '__main__':
    # unittest.main()
    HTMLTestRunner.main()
