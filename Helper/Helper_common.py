import datetime
import json
import sys

import pyodbc
import requests
from selenium.webdriver import ActionChains

sys.path.append('../')
import time
import requests
import pymysql
import pymysql.cursors
from read_configs import *
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium import webdriver
import pyodbc
import sys
import requests
import json

sys.path.append('../')
import time
import openpyxl
import pymysql
import pymysql.cursors
from read_configs import *
from selenium.webdriver.common.action_chains import ActionChains
from datetime import datetime


def login(driver):
    driver.get(url_root)
    time.sleep(3)
    driver.find_element_by_css_selector('#username').send_keys(username)
    driver.find_element_by_css_selector('#password').send_keys(password)
    driver.find_element_by_css_selector('button.login-btn').click()
    time.sleep(3)


def login_payroll(driver):
    driver.get(url_payroll)
    time.sleep(3)
    driver.find_element_by_css_selector('#username').send_keys(username)
    driver.find_element_by_css_selector('#password').send_keys(password)
    driver.find_element_by_css_selector('button.login-btn').click()
    time.sleep(3)


def scroll_to(driver, element):
    driver.execute_script("arguments[0].scrollIntoView(true);", element)


def api_get_all():
    url_login = 'http://192.168.99.47:9000/Account/signin'
    data = {
        "userName": 'tmtuan@humaxdigital.com',
        "password": '@admin'
    }
    res_login = requests.post(url=url_login, json=data)
    token = json.loads(res_login.text)["token"]

    url_request = 'http://192.168.99.47:9000/ProjectInfo/GetAllProjectInfoList'
    headers = {
        "accept": "application/json",
        "Authorization": token
    }
    res = requests.post(url=url_request, headers=headers)
    json_data = json.loads(res.text)
    return json_data


def rbga_to_hex(rbga):
    rbg = rbga.split('(')[1].split(')')[0].split(',')
    return ('#%02x%02x%02x' % (int(rbg[0]), int(rbg[1]), int(rbg[2])))


def rbg_to_hex(rbg):
    rbg = rbg[4:][:-1].split(',')
    return ('#%02x%02x%02x' % (int(rbg[0]), int(rbg[1]), int(rbg[2])))


def read_column(driver, col):
    # col = name
    elem1 = driver.find_element_by_xpath("//span[contains(text(),'" + col + "')]/../../..")
    lst = driver.find_elements_by_xpath("//div[@class='ant-table-scroll']/div[1]/table/thead/tr/th")
    a = lst.index(elem1) + 1

    time.sleep(2)
    list_pages = driver.find_elements_by_xpath('//ul//li//a')
    data = []
    info_each_row = []
    if len(list_pages) != 0:
        num_pages = int(list_pages[-2].text)
        for num in range(1, num_pages + 1):
            num_rows = driver.find_elements_by_css_selector('.ant-table-scroll tr.ant-table-row-level-0')
            for row in range(0, len(num_rows)):
                if a > 2:
                    num_cells = driver.find_elements_by_css_selector(
                        "div.ant-table-scroll> * tr>td:nth-child(" + str(a) + ")")
                    scroll_to(driver, num_cells[row])
                    # ActionChains(driver).move_to_element(num_cells[row]).click().perform()
                    time.sleep(0.01)
                    data.append(num_cells[row].text)
                else:
                    num_cells = driver.find_elements_by_css_selector(
                        ".ant-table-fixed-left tr>td:nth-child(" + str(a) + ")")
                    scroll_to(driver, num_cells[row])
                    # ActionChains(driver).move_to_element(num_cells[row]).click().perform()
                    time.sleep(0.01)
                    data.append(num_cells[row].text)
            list_pages[-1].click()
            time.sleep(2)
    else:
        total_result = int(driver.find_element_by_xpath('//span[@class="result-text "]').text.split(': ')[1])
        if total_result != 0:
            num_rows = driver.find_elements_by_css_selector('.ant-table-scroll tr.ant-table-row-level-0')
            for row in range(0, len(num_rows)):
                if a > 2:
                    num_cells = driver.find_elements_by_css_selector(
                        ".ant-table-scroll tr>td:nth-child(" + str(a) + ")")
                    ActionChains(driver).move_to_element(num_cells[row]).perform()
                    data.append(num_cells[row].text)
                else:
                    num_cells = driver.find_elements_by_css_selector(
                        ".ant-table-fixed-left tr>td:nth-child(" + str(a) + ")")
                    ActionChains(driver).move_to_element(num_cells[row]).perform()
                    data.append(num_cells[row].text)
        else:
            data = 'No data'
    return data


def sorting_increase_column(driver, column):
    list_result_search = []
    time.sleep(2)
    cols_name = driver.find_elements_by_xpath('//div[@class="ant-table-scroll"]//tr/th')
    cols_fix = driver.find_elements_by_xpath('//div[@class="ant-table-fixed-left"]//tr/th')
    cols_name[0] = cols_fix[0]
    cols_name[1] = cols_fix[1]
    # Click sort decrease
    while 1:
        if column != 'Name':
            button = cols_name[3].find_element_by_xpath('//span[text()="' + column + '"]')
            ActionChains(driver).move_to_element(button).click().perform()
        else:
            button = cols_name[1].find_element_by_xpath('//span[text()="' + column + '"]')
            ActionChains(driver).move_to_element(button).click().perform()
        # Check arrow sorter down is active
        check_active = button.find_elements_by_xpath(
            '//div/i[@class="anticon anticon-caret-up ant-table-column-sorter-up on"]')
        time.sleep(1)
        if len(check_active) != 0:
            break

    return read_column(driver, column)


def sorting_decrease_column(driver, column):
    list_result_search = []
    time.sleep(2)
    cols_name = driver.find_elements_by_xpath('//div[@class="ant-table-scroll"]//tr/th')
    cols_fix = driver.find_elements_by_xpath('//div[@class="ant-table-fixed-left"]//tr/th')
    cols_name[0] = cols_fix[0]
    cols_name[1] = cols_fix[1]
    # Click sort decrease
    while 1:
        if column != 'Name':
            button = cols_name[3].find_element_by_xpath('//span[text()="' + column + '"]')
            ActionChains(driver).move_to_element(button).click().perform()
        else:
            button = cols_name[1].find_element_by_xpath('//span[text()="' + column + '"]')
            ActionChains(driver).move_to_element(button).click().perform()
        # Check arrow sorter down is active
        check_active = button.find_elements_by_xpath(
            '//div/i[@class="anticon anticon-caret-down ant-table-column-sorter-down on"]')
        time.sleep(1)
        if len(check_active) != 0:
            break

    return read_column(driver, column)


def get_all_table_info(driver):
    list_pages = driver.find_elements_by_xpath('//ul//li//a')
    info_each_row = []
    if len(list_pages) != 0:
        num_pages = int(list_pages[-2].text)
        for num in range(1, num_pages + 1):
            num_rows = driver.find_elements_by_css_selector('.ant-table-scroll tr.ant-table-row-level-0')
            for row in range(1, len(num_rows) + 1):
                num_cells = driver.find_elements_by_css_selector('.ant-table-scroll tr:nth-child(' + str(row) + ')>td')
                cell_info = []
                for cell in range(1, len(num_cells) + 1):
                    cell_value = driver.find_element_by_css_selector(
                        '.ant-table-scroll tr:nth-child(' + str(row) + ')>td:nth-child(' + str(cell) + ')')
                    ActionChains(driver).move_to_element(cell_value).perform()
                    cell_info.append(cell_value.text)
                # Fix column Project ID and Name
                cell_value_fix_id = driver.find_element_by_css_selector(
                    '.ant-table-fixed-left tr:nth-child(' + str(row) + ')>td:nth-child(1)')
                cell_value_fix_name = driver.find_element_by_css_selector(
                    '.ant-table-fixed-left tr:nth-child(' + str(row) + ')>td:nth-child(2)')
                cell_info[0] = cell_value_fix_id.text
                cell_info[1] = cell_value_fix_name.text
                info_each_row.append(cell_info)
            list_pages[-1].click()
            time.sleep(0.5)
    else:
        total_result = int(driver.find_element_by_xpath('//span[@class="result-text "]').text.split(': ')[1])
        if total_result != 0:
            num_rows = driver.find_elements_by_css_selector('.ant-table-scroll tr.ant-table-row-level-0')
            for row in range(1, len(num_rows) + 1):
                num_cells = driver.find_elements_by_css_selector('.ant-table-scroll tr:nth-child(' + str(row) + ')>td')
                cell_info = []
                for cell in range(1, len(num_cells) + 1):
                    cell_value = driver.find_element_by_css_selector(
                        '.ant-table-scroll tr:nth-child(' + str(row) + ')>td:nth-child(' + str(cell) + ')')
                    ActionChains(driver).move_to_element(cell_value).perform()
                    cell_info.append(cell_value.text)
                # Fix column Project ID and Name
                cell_value_fix_id = driver.find_element_by_css_selector(
                    '.ant-table-fixed-left tr:nth-child(' + str(row) + ')>td:nth-child(1)')
                cell_value_fix_name = driver.find_element_by_css_selector(
                    '.ant-table-fixed-left tr:nth-child(' + str(row) + ')>td:nth-child(2)')
                cell_info[0] = cell_value_fix_id.text
                cell_info[1] = cell_value_fix_name.text
                info_each_row.append(cell_info)
        else:
            info_each_row = 'No data'
    return info_each_row


def api_project_info():
    url_login = 'http://192.168.99.47:9000/Account/signin'
    data = {
        "userName": 'dccanh@humaxdigital.com',
        "password": '@admin'
    }
    res_login = requests.post(url=url_login, json=data)
    token = json.loads(res_login.text)["token"]

    url_request = 'http://192.168.99.47:9000/ProjectInfo/GetAllProjectInfoList'
    headers = {
        "accept": "application/json",
        "Authorization": token
    }
    res = requests.post(url=url_request, headers=headers)
    json_data = json.loads(res.text)
    return json_data


def api_get_field_config():
    url_login = 'http://192.168.99.47:9000/Account/signin'
    data = {
        "userName": 'dccanh@humaxdigital.com',
        "password": '@admin'
    }
    res_login = requests.post(url=url_login, json=data)
    token = json.loads(res_login.text)["token"]
    url_request = 'http://192.168.99.47:9000/FieldConfig/GetAll'
    headers = {
        "accept": "application/json",
        "Authorization": token
    }
    res = requests.post(url=url_request, headers=headers)
    json_data = json.loads(res.text)
    return json_data


def api_full_employees():
    url_login = 'http://192.168.99.47:9000/Account/signin'
    data = {
        "userName": 'dccanh@humaxdigital.com',
        "password": '@admin'
    }
    res_login = requests.post(url=url_login, json=data)
    token = json.loads(res_login.text)["token"]

    url_request = 'http://192.168.99.47:9000/Employee/GetAllFullEmployee'
    headers = {
        "accept": "application/json",
        "Authorization": token
    }
    res = requests.post(url=url_request, headers=headers)
    json_data = json.loads(res.text)
    return json_data


def api_field_config():
    url_login = 'http://192.168.99.47:9000/Account/signin'
    data = {
        "userName": 'dccanh@humaxdigital.com',
        "password": '@admin'
    }
    res_login = requests.post(url=url_login, json=data)
    token = json.loads(res_login.text)["token"]

    url_request = 'http://192.168.99.47:9000/FieldConfig/GetAll'
    headers = {
        "accept": "application/json",
        "Authorization": token
    }
    res = requests.post(url=url_request, headers=headers)
    json_data = json.loads(res.text)
    return json_data


def api_project_detail(id):
    url_login = 'http://192.168.99.47:9000/Account/signin'
    data = {
        "userName": 'dccanh@humaxdigital.com',
        "password": '@admin'
    }
    res_login = requests.post(url=url_login, json=data)
    token = json.loads(res_login.text)["token"]

    url_request = 'http://192.168.99.47:9000/ProjectInfo/GetProjectDetail'
    headers = {
        "accept": "application/json",
        "Authorization": token,
        "Content-Type": "application/json-patch+json"
    }
    body = "'" + id + "'"
    res = requests.post(url=url_request, headers=headers, data=body)

    json_data = json.loads(res.text)
    return json_data


def api_project_info_list():
    url_login = 'http://192.168.99.47:9000/Account/signin'
    data = {
        "userName": 'dccanh@humaxdigital.com',
        "password": '@admin'
    }
    res_login = requests.post(url=url_login, json=data)
    token = json.loads(res_login.text)["token"]

    url_request = 'http://192.168.99.47:9000/ProjectInfo/GetAllProjectInfoList'
    headers = {
        "accept": "application/json",
        "Authorization": token,
        "Content-Type": "application/json-patch+json"
    }
    res = requests.post(url=url_request, headers=headers)
    json_data = json.loads(res.text)
    return json_data


def api_GetTasksByQuery(username):
    url_login = 'http://192.168.99.47:9000/Account/signin'
    data = {
        "userName": 'tmtuan@humaxdigital.com',
        "password": '@admin'
    }
    res_login = requests.post(url=url_login, json=data)
    token = json.loads(res_login.text)["token"]

    url_request = 'http://192.168.99.47:9000/ProjectInfo/GetTasksByQuery'
    headers = {
        "Authorization": token,
        "Content-Type": "application/json"
    }
    body = '{query: " assignee in (' + username + ') ", key: "key=''", server: "https://humaxvina.atlassian.net"}'
    res = requests.post(url=url_request, headers=headers, data=body)

    json_data = json.loads(res.text)
    return json_data


def querydb(query):
    conn = pyodbc.connect('DRIVER={SQL Server};'
                          'SERVER=192.168.99.47,1433;'
                          'DATABASE=VinaHR_Pro;'
                          'UID=qateam;'
                          'PWD=qateam;')
    cur = conn.cursor()
    cur.execute(query)
    return cur.fetchall()


def waitFor(maxSecond, runFunction):
    while maxSecond:
        try:
            len(runFunction)
            break
        except:
            time.sleep(0.5)
            maxSecond -= 0.5


def get_download_excel_info(excel_path):
    wb = openpyxl.load_workbook(excel_path)
    ws = wb['ProjectInfo']
    chart_info = []
    for r in range(2, ws.max_row + 1):
        row_info = []
        for c in range(1, ws.max_column + 1):
            row_info.append(ws.cell(row=r, column=c).value)
        chart_info.append(row_info)
    info = []
    for i in range(len(chart_info)):
        row = chart_info[i][:9]
        row.append(chart_info[i][9:])
        info.append(row)
    a = info
    count = 0
    for i in range(len(a)):
        if a[i][0] is None:
            count += 1
            for j in range(len(a[i])):
                if a[i][j] is not None:
                    a[i - 1][j] = [a[i - 1][j]]
                    a[i - count][j].insert(len(a[i - count][j]), a[i][j])
        else:
            count = 0
    expected = []
    for i in range(len(a)):
        if a[i][0] is not None:
            expected.append(a[i])
    return expected


def validate_date(date_text):
    try:
        datetime.strptime(date_text, '%d/%b/%Y')
    except ValueError:
        raise ValueError("Incorrect data format, should be DD/MM/YYYY")