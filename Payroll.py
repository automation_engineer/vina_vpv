import calendar
import datetime
import re
import sys

import pyautogui
from selenium.webdriver.common.keys import Keys

sys.path.append('../')

import pytest
import random

import time
import unittest
import allure
from selenium import webdriver
from selenium.webdriver import ActionChains

from Helper.Helper_common import *

import glob
import os
import unittest
import configparser
import HTMLTestRunner
import openpyxl
from read_configs import *
from winreg import *
import Helper.Helper_common
from datetime import datetime


class Search(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.driver = webdriver.Chrome('chromedriver.exe')
        driver = self.driver
        driver.maximize_window()

        # Log in
        login_payroll(driver)
        time.sleep(5)

        # Moving to test page
        driver.get(url_payroll + '/payroll')
        time.sleep(10)

    @classmethod
    def tearDownClass(self):
        self.driver.quit()

    @allure.story('Check sort employee')
    def test_PR_Search_full_name(self):
        driver = self.driver
        list_fail = []

        driver.refresh()
        time.sleep(5)

        try:
            with allure.step('Check default text of search box'):
                # default text
                default_text = driver.find_element_by_css_selector('div.ant-select-selection__placeholder').text
                self.assertEqual(default_text, 'Search')
        except:
            list_fail.append('The default text is ' + default_text + ' instead of [Search]')

        # fetch list name from api
        available_names = []
        for i in api_full_employees():
            available_names.append(i['name'])

        driver.find_element_by_css_selector('div.smart-search> * input').clear()
        time.sleep(0.2)

        random_name = available_names[random.randint(0, len(available_names)) - 1]

        # enter random name
        driver.find_element_by_css_selector('div.smart-search> * input')\
            .send_keys(random_name)
        time.sleep(0.2)

        try:
            with allure.step('Check value of current text'):
                # compare entered text and current text box
                self.assertEqual(driver.find_element_by_css_selector(
                    'div.smart-search> * input').get_attribute('value'), random_name)
        except:
            list_fail.append('The current text is not the same as entered text')

        # click Search button
        ActionChains(driver).move_to_element(
            driver.find_element_by_css_selector('div.smart-search>button.search-btn')).click().perform()
        time.sleep(2)

        try:
            with allure.step('Check search result'):
                for i in driver.find_elements_by_css_selector('div.payroll-table__row>div:nth-child(3)'):
                    self.assertTrue(i.text in random_name)
        except:
            list_fail.append('The search result is incorrect')

        self.assertEqual(len(list_fail), 0, list_fail)

    @allure.story('Check recently Search')
    def test_PR_Recently_Search(self):
        driver = self.driver
        list_fail = []

        driver.refresh()
        time.sleep(5)

        # fetch list name from api
        available_names = []
        for i in api_full_employees():
            available_names.append(i['englishName'])

        driver.find_element_by_css_selector('div.smart-search> * input').clear()
        time.sleep(0.2)

        random_name = available_names[random.randint(0, len(available_names)) - 1]

        # enter random name
        driver.find_element_by_css_selector('div.smart-search> * input') \
            .send_keys(random_name)
        time.sleep(0.5)

        try:
            with allure.step('Click Search button'):
                # click Search button
                ActionChains(driver).move_to_element(
                    driver.find_element_by_css_selector('div.smart-search>button.search-btn')).click().perform()
                time.sleep(2)
        except:
            list_fail.append('Unable to click Search button')

        try:
            with allure.step('Check recently searched list'):
                # check name in recently searched
                recent_list = []
                for i in driver.find_elements_by_css_selector('span.suggestion'):
                    recent_list.append(i.text)
                self.assertTrue(random_name in recent_list)
                time.sleep(0.5)
        except:
            list_fail.append('The recently searched list does not contain the entered name')

        try:
            with allure.step('Check recently searched highlight'):
                # check highlight
                for i in driver.find_elements_by_css_selector('span.suggestion'):
                    self.assertEqual(i.value_of_css_property('color'), 'rgba(66, 127, 236, 1)')
        except:
            list_fail.append('The highlight of recently searched is incorrect')

        # refresh page
        driver.refresh()
        time.sleep(5)

        # click on recent search
        for i in driver.find_elements_by_css_selector('span.suggestion'):
            if i.text == random_name:
                ActionChains(driver).move_to_element(driver.find_element_by_xpath(
                    '//span[@class="suggestion"][contains(text(), "' + i.text + '")]')).click().perform()
                time.sleep(1)

        try:
            with allure.step('Check text is copied to text box'):
                # verify text is copied to text box
                self.assertEqual(driver.find_element_by_css_selector(
                    'div.smart-search> * input').get_attribute('value'), random_name)
        except:
            list_fail.append('The text is not copied to text box')

        # click Search button
        ActionChains(driver).move_to_element(
            driver.find_element_by_css_selector('div.smart-search>button.search-btn')).click().perform()
        time.sleep(2)

        try:
            with allure.step('Check search result'):
                for i in driver.find_elements_by_css_selector(
                        'div.payroll-table__table-frozen-left>div.payroll-table__body> * '
                        'div[role="gridcell"]:nth-child(2)'):
                    self.assertTrue(i.text in random_name)
        except:
            list_fail.append('The search result from Recently searched is incorrect')

        self.assertEqual(len(list_fail), 0, list_fail)

    @allure.story('Check Humbager icon')
    def test_PR_Humbager_icon(self):
        driver = self.driver
        list_fail = []

        driver.refresh()
        time.sleep(5)

        try:
            with allure.step('Verify Hambager icon'):
                ico_ham = driver.find_element_by_css_selector('span.icon-menu')
        except:
            list_fail.append('Unable to locate Hambager')

        try:
            with allure.step('Click on Hambager'):
                ActionChains(driver).move_to_element(ico_ham).click().perform()
                time.sleep(1)
        except:
            list_fail.append('Unable to click on Hambager')

        try:
            with allure.step('Verify menu appearance'):
                self.assertTrue(driver.find_element_by_css_selector(
                    'div.ant-drawer-wrapper-body').is_displayed() == True)
                time.sleep(1)
        except:
            list_fail.append('Menu is not displayed after clicking')

        try:
            with allure.step('Click on Hambager again'):
                ActionChains(driver).move_to_element(ico_ham).click().perform()
                time.sleep(1)
        except:
            list_fail.append('Issue when click on Hambager again')

        try:
            with allure.step('Verify menu disappear'):
                self.assertTrue(len(driver.find_elements_by_css_selector(
                    'div.ant-drawer-open')) == 0)
                time.sleep(1)
        except:
            list_fail.append('Menu is not closed')

        self.assertEqual(len(list_fail), 0, list_fail)

    @allure.story('Check Custormize colume Default')
    def test_PR_Custormize_Colume_Default(self):
        driver = self.driver
        list_fail = []

        driver.refresh()
        time.sleep(5)

        try:
            with allure.step("1. Choose Custormize colume icon"):
                setting_btn = driver.find_element_by_css_selector("i.payroll-icon.payroll-icon--column").click()
        except:
            list_fail.append("Unable choose Custormize colume icon")

        try:
            with allure.step("1.1 Check label Custormize colume"):
                label_customize_col = driver.find_element_by_css_selector("div.ant-modal-content div.dialog-title").text
                self.assertEqual(label_customize_col, "CUSTOMIZE COLUMNS")
        except AssertionError:
            list_fail.append("Label Custormize colume display wrong")

        try:
            with allure.step("1.2 Check check box Empolyee Code"):
                check_box = driver.find_element_by_xpath(
                    "//div[@class='ant-modal-content']//*[contains(text(),'Employee Code')]/..//span[@class='ant-checkbox ant-checkbox-checked']").click()

                check_box = driver.find_elements_by_xpath(
                    "//div[@class='ant-modal-content']//*[contains(text(),'Employee Code')]/..//span[@class='ant-checkbox ant-checkbox-checked']")
                self.assertEqual(len(check_box), 1)
        except AssertionError:
            list_fail.append("Check box Empolyee Code not disable")

        try:
            with allure.step("1.3 Check check box English Name"):
                check_box = driver.find_element_by_xpath(
                    "//div[@class='ant-modal-content']//*[contains(text(),'English Name')]/..//span[@class='ant-checkbox ant-checkbox-checked']").click()

                check_box = driver.find_elements_by_xpath(
                    "//div[@class='ant-modal-content']//*[contains(text(),'English Name')]/..//span[@class='ant-checkbox ant-checkbox-checked']")
                self.assertEqual(len(check_box), 1)
        except AssertionError:
            list_fail.append("Check box English Name not disable")

        try:
            with allure.step("1.4 Check list checkbox of column"):
                lst_column = driver.find_elements_by_css_selector(
                    "label.custom-checkbox.payroll-column__checkbox.ant-checkbox-wrapper")
                lst_checkbox = driver.find_elements_by_css_selector("input.ant-checkbox-input")
                self.assertEqual(len(lst_checkbox), len(lst_column))
        except AssertionError:
            list_fail.append("List checkbox of column display wrong")

        try:
            with allure.step("2. Click Contract type field"):
                check_box = driver.find_element_by_xpath(
                    "//div[@class='ant-modal-content']//*[contains(text(),'Contract Type')]/..//span[@class='ant-checkbox ant-checkbox-checked']").click()
                check_box = driver.find_elements_by_xpath(
                    "//div[@class='ant-modal-content']//*[contains(text(),'Contract Type')]/..//span[@class='ant-checkbox ant-checkbox-checked']")
                self.assertEqual(len(check_box), 0)
        except:
            list_fail.append("Can not click Contract type field")

        try:
            with allure.step("3. Click x to close pop up"):
                close_btn = driver.find_element_by_css_selector("div.ant-modal-content span.icon-ico_cancel").click()
        except:
            list_fail.append("Click x to close pop up")

        try:
            with allure.step("3.1 Check pop up close or not"):
                pop_up = driver.find_elements_by_css_selector("div.ant-modal-content")
                self.assertEqual(len(pop_up), 0)
        except AssertionError:
            list_fail.append("Pop up setting do not close")

        try:
            with allure.step("3.2 Check Contract Type on Payroll Screen"):
                elem = driver.find_elements_by_xpath("//div[contains(text(),'Contract Type')]")
                self.assertEqual(len(elem), 0)
        except AssertionError:
            list_fail.append("Contract Type still display on Payroll Screen")

        self.assertEqual(len(list_fail), 0, list_fail)

    @allure.story("Check GUI default")
    def test_PR_GUI_Default(self):
        driver = self.driver
        list_fail = []
        time.sleep(2)

        try:
            with allure.step("1 Check Humbager Icon"):
                hambager = driver.find_elements_by_css_selector("button.header__btn>span.icon-menu")
                self.assertEqual(len(hambager), 1)
        except AssertionError:
            list_fail.append("Hambager Icon is not exist")

        try:
            with allure.step("2 Check label HUMAX Payroll"):
                label_humax_payroll = driver.find_element_by_css_selector("a.header__info>span.header__name")
                self.assertEqual(label_humax_payroll.text, 'Payroll')
        except AssertionError:
            list_fail.append("Label HUMAX Payroll is not exist")

        try:
            with allure.step("3 Check menu bar"):
                menu_bar = driver.find_element_by_css_selector("ul.nav__list").text
                self.assertEqual(menu_bar, 'PayrollCompensationSettings')
        except AssertionError:
            list_fail.append("Menu bar display wrong")

        try:
            with allure.step("4 Check label Payroll on menu"):
                label_payroll = driver.find_element_by_css_selector("ul.nav__list>li:first-child>a")
                self.assertEqual(label_payroll.text, 'Payroll')
                self.assertEqual(label_payroll.value_of_css_property("color"), 'rgba(225, 236, 255, 1)')
        except AssertionError:
            list_fail.append("label Payroll on menu wrong")

        try:
            with allure.step("5 Check icon search"):
                icon_search = driver.find_elements_by_css_selector("button.ant-btn.search-btn.ant-btn-primary")
                self.assertEqual(len(icon_search), 1)
        except AssertionError:
            list_fail.append("Icon search is not exist")

        try:
            with allure.step("6 Check Recently searched"):
                recently_search = driver.find_elements_by_css_selector("div.recent-search>span")
                self.assertEqual(len(recently_search), 1)
        except AssertionError:
            list_fail.append("Recently searched is not exist")

        try:
            with allure.step("7 Check icon filter by month, year"):
                icon_calendar_filter = driver.find_elements_by_css_selector(
                    "i.payroll-icon.payroll-icon--calendar.ant-calendar-picker-icon")
                self.assertEqual(len(icon_calendar_filter), 1)
        except AssertionError:
            list_fail.append("Icon filter by month, year not exist")

        try:
            with allure.step("8 Check button download excel file"):
                btn_download_excel = driver.find_elements_by_css_selector("button.payroll-btn>span.payroll-btn__text")
                self.assertEqual(len(btn_download_excel), 1)
        except AssertionError:
            list_fail.append("Button download excel file is not exist")

        try:
            with allure.step("9. Check icon Customize Colums"):
                icon_customize_col = driver.find_elements_by_css_selector("button.payroll-btn>i.payroll-icon--column")
                self.assertEqual(len(icon_customize_col), 1)
        except AssertionError:
            list_fail.append("Icon Customize Colums is not exist")

        try:
            with allure.step("10 Check icon Download Settings"):
                icon_download_setting = driver.find_elements_by_css_selector(
                    "button.payroll-btn>span.payroll-btn__icon.icon-settings")
                self.assertEqual(len(icon_download_setting), 1)
        except AssertionError:
            list_fail.append("Icon Download Settings is not exist")

        try:
            with allure.step("11 Check format table"):
                expected = []
                col = driver.find_elements_by_css_selector(
                    "div.payroll-table__table.payroll-table__table-frozen-left>* div[role='rowgroup']>div>div>div:first-child")
                for i in range(len(col)):
                    ActionChains(driver).move_to_element(col[i]).perform()
                    expected.append(col[i].text)

                col = driver.find_elements_by_css_selector(
                    "div.payroll-table__table.payroll-table__table-main>* div[role='rowgroup']>div>div>div:first-child")
                for i in range(len(col)):
                    ActionChains(driver).move_to_element(col[i]).perform()
                    expected.append(col[i].text)

                actual = ['EMPLOYEE CODE', 'ENGLISH NAME', 'FULL NAME', 'CONTRACT TYPE', 'SALARY TYPE', 'STATUS',
                          'TEAM',
                          'PART', 'POSITION', 'JOB GRADE', 'JOB FAMILY', 'DATE JOIN', 'RESIGNATION DATE',
                          'CONTRACTED BS',
                          'DEPENDENT', 'DAY(S) OF UNPAID', 'ADVANCE PAYMENT', 'OTHERS', 'MONTH OF WORKING YEAR',
                          'DAY OF REAL WORKING ALLOWANCE', 'DAY OF TOTAL NET WORKING', 'OT SALARY RELIEF', 'OT SALARY',
                          'MEAL ALLOWNCE']

                self.assertListEqual(expected, actual)

        except AssertionError:
            list_fail.append("Format table wrong")

        try:
            with allure.step("12 Check scroll bar vertical/horizontal"):
                abc = driver.find_element_by_css_selector(
                    "div.payroll-table__table.payroll-table__table-main>div.payroll-table__body")

                scrollHeight = int(abc.get_attribute("scrollHeight"))
                offsetHeight = int(abc.get_attribute("offsetHeight"))
                self.assertTrue(scrollHeight > offsetHeight)

                scrollWidth = int(abc.get_attribute("scrollWidth"))
                offsetWidth = int(abc.get_attribute("offsetWidth"))
                self.assertTrue(scrollWidth > offsetWidth)

        except AssertionError:
            list_fail.append("Scroll bar vertical/horizontal")

        try:
            with allure.step("13 Check sort icon"):
                ele = driver.find_elements_by_xpath("//div[@class='payroll-table__header-cell-text']")
                for i in range(len(ele)):
                    icon_sort = ele[i].find_elements_by_xpath("../div[2]")
                    self.assertEqual(len(icon_sort), 1)
        except AssertionError:
            list_fail.append("Sort icon")

        try:
            with allure.step("14 Check button Caculation"):
                btn_calculator = driver.find_elements_by_css_selector("button.ant-btn.ant-btn-task.ant-btn-lg")
                self.assertEqual(len(btn_calculator), 1)
        except AssertionError:
            list_fail.append("Button Caculation is not exist")

        try:
            with allure.step("15 Check button Submit"):
                btn_submit = driver.find_elements_by_css_selector("button.ant-btn.ant-btn-submit.ant-btn-lg")
                self.assertEqual(len(btn_submit), 1)
        except AssertionError:
            list_fail.append("button Submit is not exist")

        try:
            with allure.step("16 Check hyperlink on English name field"):
                eng_name = driver.find_elements_by_xpath(
                    "//div[@class='payroll-table__table payroll-table__table-frozen-left']//div[@class='payroll-table__row']")
                for i in range(len(eng_name)):
                    hyper_link = eng_name[i].find_elements_by_xpath("./div[2]/a[@href='#']")
                    self.assertEqual(len(hyper_link), 1)
        except AssertionError:
            list_fail.append("Hyperlink on English name field wrong")

        self.assertEqual(len(list_fail), 0, list_fail)


if __name__ == '__main__':
    # unittest.main()
    HTMLTestRunner.main()
