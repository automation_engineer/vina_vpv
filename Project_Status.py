import datetime
import re
import sys

import pyautogui
from selenium.webdriver.common.keys import Keys

sys.path.append('../')

import pytest
import random

import time
import unittest
import allure
from selenium import webdriver
from selenium.webdriver import ActionChains

from Helper.Helper_common import *

import glob
import os
import unittest
import configparser
import HTMLTestRunner
import openpyxl
from read_configs import *
from winreg import *
import Helper.Helper_common
from datetime import datetime


# **********************************************************************************************************************


class GUI(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.driver = webdriver.Chrome('chromedriver.exe')
        driver = self.driver
        driver.maximize_window()

        # Log in
        login(driver)
        time.sleep(5)

        # Moving to test page
        driver.get(url_root + '/project-visibility/project-status')
        time.sleep(10)

    @classmethod
    def tearDownClass(self):
        self.driver.quit()

    # QUAL-616
    @allure.story('Check GUI default')
    def test_PS_Default_GUI(self):
        driver = self.driver
        list_fail = []

        try:
            with allure.step('Verify Hambager icon'):
                ico_ham = driver.find_element_by_css_selector('span.icon-menu.menu-toggle')
        except:
            list_fail.append('No Hambaguer icon')
            pass

        try:
            with allure.step('Check Humax logo'):
                self.assertEqual(len(driver.find_elements_by_css_selector(
                    'img[src="/images/humax.png"]')), 1)
                time.sleep(0.1)
        except:
            list_fail.append('[Version 2]No Humax logo')
            pass

        try:
            with allure.step('Check Project Visibility text'):
                self.assertEqual(driver.find_element_by_css_selector('div.pvs').text, "Project Visibility")
                time.sleep(0.1)
        except:
            list_fail.append('No Project Visibility text')
            pass

        try:
            with allure.step('Check Header bar'):
                self.assertEqual(len(driver.find_elements_by_css_selector(
                    'div.employee-info-header-nav>div.nav-main')), 1)
                time.sleep(0.1)
        except:
            list_fail.append('No Header bar')
            pass

        try:
            with allure.step('Check Project Status text'):
                self.assertEqual(len(driver.find_elements_by_xpath('//a[contains(text(), "Project Status")]')), 1)
                time.sleep(0.01)
        except:
            list_fail.append('No Project Status text')
            pass

        try:
            with allure.step('Check Search bar'):
                self.assertEqual(len(driver.find_elements_by_css_selector(
                    'li.ant-select-search> * input.ant-input')), 1)
                time.sleep(0.01)
        except:
            list_fail.append('No Search bar')
            pass

        try:
            with allure.step("Check Search bar's default text"):
                self.assertEqual(driver.find_element_by_css_selector('div[unselectable="on"]').text, "Search")
                time.sleep(0.01)
        except:
            list_fail.append('The default text in Search bar is not [Search]')
            pass

        try:
            with allure.step('Check Recently searched text'):
                self.assertTrue(
                    "Recently searched" in driver.find_element_by_css_selector('div.recent-search>span').text)
                time.sleep(0.01)
        except:
            list_fail.append('Text [Recently searched] is not displayed')
            pass

        try:
            with allure.step('Check Total Projects text'):
                self.assertTrue("Total Projects" in driver.find_element_by_css_selector('span.result-text').text)
                time.sleep(0.01)
        except:
            list_fail.append('Text [Total Projects] is not displayed')
            pass

        try:
            with allure.step('Check Filter button'):
                self.assertEqual(len(driver.find_elements_by_css_selector('span.icon-filter_list')), 1)
                time.sleep(0.01)
        except:
            list_fail.append('No Filter button')
            pass

        try:
            with allure.step('Click Filter button'):
                ActionChains(driver).move_to_element(
                    driver.find_element_by_css_selector('span.icon-filter_list')).click().perform()
                time.sleep(0.5)
        except:
            list_fail.append('Issue when clicking on Filter button')
            pass

        try:
            with allure.step('Verify Filter menu'):
                expected = ["Project Type", "Customer's company", "Customer's department", "Status"]
                actual = []

                for i in driver.find_elements_by_css_selector('div.ant-popover-inner-content> * div.left>div'):
                    actual.append(i.text)
                    time.sleep(0.01)

                self.assertEqual(expected, actual)
                time.sleep(0.01)
        except:
            list_fail.append('The Filter is not ordered correctly')
            pass

        try:
            with allure.step('Verify Add new project button'):
                self.assertEqual(len(driver.find_elements_by_css_selector('div.search-box+div> * button.blue')), 1)
                time.sleep(0.01)
        except:
            list_fail.append('No Add new project button')
            pass

        try:
            with allure.step('Verify Download excel file button'):
                self.assertEqual(len(driver.find_elements_by_css_selector(
                    'div.search-box+div> * div.download-excel>span.first')), 1)
                time.sleep(0.01)
        except:
            list_fail.append('No Download excel file button')
            pass

        try:
            with allure.step('Verify Download settings button'):
                self.assertEqual(len(driver.find_elements_by_css_selector(
                    'div.search-box+div> * div.download-excel>span.icon-settings')), 1)
                time.sleep(0.01)
        except:
            list_fail.append('No Download settings button')
            pass

        try:
            with allure.step('Verify table headers'):
                expected = ['Customer', 'Department', 'PO', 'Total MM', 'Real MM', 'Total Budget',
                            'Status', 'Resolved Issues', 'Expense', 'Project ID', 'Name']
                actual = []

                for i in driver.find_elements_by_css_selector('thead.ant-table-thead>tr>th>div'):
                    # scroll_to(driver, i)
                    if i.text != '':
                        actual.append(i.text)
                        time.sleep(0.1)

                self.assertEqual(expected, actual)
                time.sleep(0.01)
        except:
            list_fail.append('The table headers is incorrect')
            pass

        try:
            with allure.step('Verify number of Sort icons'):
                self.assertEqual(len(driver.find_elements_by_css_selector(
                    'div.ant-table-scroll> * table> * th> * div[title="Sort"]')), 10)
                time.sleep(0.01)
        except:
            list_fail.append('The number of Sort icon is less than 10')
            pass

        try:
            with allure.step('Verify Vertical scrollbar'):
                tbl = driver.find_element_by_css_selector('div.ant-table-scroll>div.ant-table-body')

                scrollHeight = int(tbl.get_attribute("scrollHeight"))
                offsetHeight = int(tbl.get_attribute("offsetHeight"))

                self.assertTrue(scrollHeight > offsetHeight)
                time.sleep(0.01)
        except:
            list_fail.append('No Vertical scrollbar')
            pass

        try:
            with allure.step('Verify Horizontal scrollbar'):
                scrollWidth = int(tbl.get_attribute("scrollWidth"))
                offsetWidth = int(tbl.get_attribute("offsetWidth"))

                self.assertTrue(scrollWidth > offsetWidth)
                time.sleep(0.01)
        except:
            list_fail.append('No Horizontal scrollbar')
            pass

        try:
            with allure.step('Verify pagination'):
                self.assertEqual(len(driver.find_elements_by_css_selector('ul.ant-table-pagination')), 1)
                time.sleep(0.01)
        except:
            list_fail.append('No pagination')
            pass

        try:
            with allure.step('Verify table settings icon'):
                self.assertEqual(1, 2, 'No table settings icon')
                # assert 1 == 2, "No table settings icon"
                time.sleep(0.01)
        except:
            list_fail.append('No table settings icon')
            pass

        try:
            with allure.step('Verify new windows when clicking on a project'):
                lst = driver.find_elements_by_css_selector(
                    'div.ant-table-fixed-left> * table.ant-table-fixed>tbody>tr>td:last-child>div>div:first-child')

                ActionChains(driver).move_to_element(lst[random.randint(0, len(lst) - 1)]).click().perform()
                time.sleep(10)

                self.assertTrue(len(driver.window_handles) > 1)
                time.sleep(0.01)

                # switch to popup then close
                driver.switch_to.window(driver.window_handles[1])
                time.sleep(0.5)
                driver.close()
                time.sleep(3)

                # switch to main window
                driver.switch_to.window(driver.window_handles[0])
                time.sleep(3)
        except:
            list_fail.append('No new windows is opened when clicking on a project')
            pass

        self.assertEqual(len(list_fail), 0, list_fail)

    # QUAL-644
    @allure.story('Check Humbager icon')
    def test_PS_Humbager_icon(self):
        driver = self.driver
        list_fail = []

        try:
            with allure.step('Verify Hambager icon'):
                ico_ham = driver.find_element_by_css_selector('span.icon-menu.menu-toggle')
        except:
            list_fail.append('Unable to locate Hambager')

        try:
            with allure.step('Click on Hambager'):
                ActionChains(driver).move_to_element(ico_ham).click().perform()
                time.sleep(1)
        except:
            list_fail.append('Unable to click on Hambager')

        try:
            with allure.step('Verify menu appearance'):
                self.assertTrue(driver.find_element_by_css_selector(
                    'div.ant-drawer-wrapper-body').is_displayed() == True)
                time.sleep(1)
        except:
            list_fail.append('Menu is not displayed after clicking')

        try:
            with allure.step('Click on Hambager again'):
                ActionChains(driver).move_to_element(ico_ham).click().perform()
                time.sleep(1)
        except:
            list_fail.append('Issue when click on Hambager again')

        try:
            with allure.step('Verify menu disappear'):
                self.assertTrue(len(driver.find_elements_by_css_selector(
                    'ant-drawer-open')) == 0)
                time.sleep(1)
        except:
            list_fail.append('Menu is not closed')

        self.assertEqual(len(list_fail), 0, list_fail)


class AddNewProject(unittest.TestCase):

    # @classmethod
    def setUp(self):
        self.driver = webdriver.Chrome('chromedriver.exe')
        driver = self.driver
        driver.maximize_window()

        # Log in
        login(driver)
        time.sleep(5)

        # Moving to test page
        driver.get(url_root + '/project-visibility/project-status')
        time.sleep(10)

    # @classmethod
    def tearDown(self):
        self.driver.quit()

    # QUAL-659
    @allure.story('Check Cancel button for adding new Project')
    def test_PS_AddNewProject_Cancel(self):
        driver = self.driver
        list_fail = []

        try:
            with allure.step('Check button Add new project'):
                btn_add_project = driver.find_element_by_css_selector('div.search-box+div> * button.blue')

                ActionChains(driver).move_to_element(btn_add_project).click().perform()
                time.sleep(3)

                driver.switch_to.window(driver.window_handles[1])
                time.sleep(1)
        except:
            list_fail.append('No new windows is opened when creating a project')

        try:
            with allure.step('Input project name'):
                driver.find_element_by_css_selector('label.label-input+label>input.material-input').send_keys(
                    'HVN_' + str(time.time())[:8])
                time.sleep(3)
        except:
            list_fail.append('Unable to input new project name')

        try:
            with allure.step('Select an option for all dropdowns'):
                dropdowns = driver.find_elements_by_css_selector('div.react-select__value-container')
                for dropdown in dropdowns:
                    scroll_to(driver, dropdown)
                    ActionChains(driver).move_to_element(dropdown).click().perform()
                    time.sleep(0.5)

                    selectable = driver.find_elements_by_css_selector('div.react-select__menu-list>div')

                    ActionChains(driver).move_to_element(
                        selectable[random.randint(0, len(selectable) - 1)]).click().perform()
                    time.sleep(0.5)
        except:
            list_fail.append('Issue with dropdowns')

        try:
            with allure.step('Select Approver/PO'):
                btn_plus = driver.find_elements_by_css_selector('div.plus-btn')
                for i in btn_plus:
                    scroll_to(driver, i)
                    ActionChains(driver).move_to_element(i).click().perform()
                    time.sleep(0.5)

                    selectable = driver.find_elements_by_css_selector('ul[role="listbox"]>li')
                    a = selectable[random.randint(0, len(selectable) - 1)]

                    scroll_to(driver, a)
                    ActionChains(driver).move_to_element(a).click().perform()
                    time.sleep(0.5)
        except:
            list_fail.append('Issue when selecting Approver/PO')

        try:
            with allure.step('Select startDate/endDate'):
                startDate = driver.find_element_by_css_selector('input[name="startDate"]')
                scroll_to(driver, startDate)
                ActionChains(driver).move_to_element(startDate).click().perform()
                time.sleep(0.5)
                days = driver.find_elements_by_css_selector('div.react-datepicker__month>div>div.react-datepicker__day')
                ActionChains(driver).move_to_element(days[random.randint(0, len(days) - 1)]).click().perform()
                time.sleep(0.5)

                endDate = driver.find_element_by_css_selector('input[name="endDate"]')
                scroll_to(driver, endDate)
                ActionChains(driver).move_to_element(endDate).click().perform()
                time.sleep(0.5)
                ActionChains(driver).move_to_element(
                    driver.find_element_by_css_selector('div.justify-center>i.anticon-arrow-right')).click().perform()
                time.sleep(1)
                days = driver.find_elements_by_css_selector('div.react-datepicker__month>div>div.react-datepicker__day')
                ActionChains(driver).move_to_element(days[random.randint(0, len(days) - 1)]).click().perform()
                time.sleep(0.5)

                feature_startDate = driver.find_element_by_css_selector('input[name="features[0].startDate"]')
                scroll_to(driver, feature_startDate)
                ActionChains(driver).move_to_element(feature_startDate).click().perform()
                time.sleep(0.5)
                days = driver.find_elements_by_css_selector(
                    'div.react-datepicker__month>div>div.react-datepicker__day:not(.react-datepicker__day--disabled)')
                ActionChains(driver).move_to_element(days[random.randint(0, len(days) - 1)]).click().perform()
                time.sleep(0.5)

                feature_endDate = driver.find_element_by_css_selector('input[name="features[0].endDate"]')
                scroll_to(driver, feature_endDate)
                ActionChains(driver).move_to_element(feature_endDate).click().perform()
                time.sleep(0.5)
                days = driver.find_elements_by_css_selector(
                    'div.react-datepicker__month>div>div.react-datepicker__day:not(.react-datepicker__day--disabled)')
                ActionChains(driver).move_to_element(days[random.randint(0, len(days) - 1)]).click().perform()
                time.sleep(0.5)
        except:
            list_fail.append('Issue when selecting startDate/endDate')

        try:
            with allure.step('Enter description for Feature Map'):
                driver.find_element_by_css_selector('textarea[name="features[0].description"]').send_keys(
                    str(time.time()))
                time.sleep(2)
        except:
            list_fail.append('Unable to enter description for Feature Map')

        try:
            with allure.step('Enter description for Statement'):
                driver.find_element_by_css_selector('textarea[name="description"]').send_keys(str(time.time()))
                time.sleep(2)
        except:
            list_fail.append('Unable to enter description for Statement')

        try:
            with allure.step('Verify Cancel button'):
                btn_cancel = driver.find_element_by_css_selector('div.form-function>button:last-child')
                scroll_to(driver, btn_cancel)
                time.sleep(1)
        except:
            list_fail.append('No Cancel button')

        try:
            with allure.step('Click on Cancel button'):
                ActionChains(driver).move_to_element(btn_cancel).click().perform()
                time.sleep(1)
        except:
            list_fail.append('Unable to click on Cancel button')

        try:
            with allure.step('Verify the popup is closed'):
                self.assertEqual(len(driver.window_handles), 1)
                time.sleep(0.5)

                # switch to main window
                driver.switch_to.window(driver.window_handles[0])
                time.sleep(3)
        except:
            list_fail.append('The popup is not closed')

        self.assertEqual(len(list_fail), 0, list_fail)

    # QUAL-658
    @allure.story('Check Fail when adding new project')
    def test_PS_AddNewProject_Fail(self):
        driver = self.driver
        list_fail = []

        # switch to main window
        # driver.switch_to.window(driver.window_handles[0])
        # time.sleep(3)

        try:
            with allure.step('Check button Add new project'):
                btn_add_project = driver.find_element_by_css_selector('div.search-box+div> * button.blue')

                ActionChains(driver).move_to_element(btn_add_project).click().perform()
                time.sleep(3)

                self.assertTrue(len(driver.window_handles) > 1)
                time.sleep(0.01)

                # switch to popup
                driver.switch_to.window(driver.window_handles[1])
        except:
            list_fail.append('No new windows is opened when creating a project')

        try:
            with allure.step('Verify the project name is blanked'):
                actual = driver.find_element_by_css_selector('label.label-input+label>input.material-input').text
                time.sleep(1)

                self.assertEqual(actual, '')
                time.sleep(1)
        except:
            list_fail.append('The project name is not blanked by default')

        # try:
        #     with allure.step('Verify the Submit button is disabled'):
        #         btn_submit = driver.find_element_by_css_selector('div.form-function>button:first-child')
        #
        #         self.assertEqual(btn_submit.get_property('disabled'), True)
        #         time.sleep(0.5)
        # except:
        #     list_fail.append('The Submit button is not disabled')

        # Click on Submit button and verify the red dot
        try:
            with allure.step('Verify red dot'):
                ActionChains(driver).move_to_element(driver.find_element_by_css_selector(
                    'div.form-function>button:first-child')).click().perform()
                time.sleep(3)
                self.assertTrue(len(driver.find_elements_by_css_selector('span.error-field')) > 0)
        except:
            list_fail.append('There is no red dot')

        try:
            driver.close()
            time.sleep(3)

            # switch to main window
            driver.switch_to.window(driver.window_handles[0])
        except:
            list_fail.append('Error when closing the Create popup')

        self.assertEqual(len(list_fail), 0, list_fail)

    # QUAL-660
    @allure.story('Check Save Draft button')
    def test_PS_AddNewProject_SaveDraft(self):
        driver = self.driver
        list_fail = []

        try:
            with allure.step('Check button Add new project'):
                btn_add_project = driver.find_element_by_css_selector('div.search-box+div> * button.blue')

                ActionChains(driver).move_to_element(btn_add_project).click().perform()
                time.sleep(3)

                self.assertTrue(len(driver.window_handles) > 1)
                time.sleep(0.01)

                # switch to popup
                driver.switch_to.window(driver.window_handles[1])
                driver.maximize_window()
                time.sleep(1)
        except:
            list_fail.append('No new windows is opened when creating a project')

        try:
            with allure.step('Input project name'):
                project_name = 'HVN_' + str(time.time())
                driver.find_element_by_css_selector('label.label-input+label>input.material-input') \
                    .send_keys(project_name)
                time.sleep(3)
        except:
            list_fail.append('Unable to input new project name')

        try:
            with allure.step('Select an option for all dropdowns'):
                for i in driver.find_elements_by_css_selector('div.react-select__value-container'):
                    scroll_to(driver, i)
                    ActionChains(driver).move_to_element(i).click().perform()
                    time.sleep(1)

                    ActionChains(driver).move_to_element(driver.find_elements_by_css_selector(
                        'div.react-select__menu-list>div')[random.randint(0, len(
                        driver.find_elements_by_css_selector(
                            'div.react-select__menu-list>div')) - 1)]).click().perform()
                    time.sleep(0.5)
        except:
            list_fail.append('Issue with dropdowns')

        try:
            with allure.step('Select Approver/PO'):
                btn_plus = driver.find_elements_by_css_selector('div.plus-btn')
                for i in btn_plus:
                    scroll_to(driver, i)
                    ActionChains(driver).move_to_element(i).click().perform()
                    time.sleep(2)

                    selectable = driver.find_elements_by_css_selector('ul[role="listbox"]>li')
                    a = selectable[random.randint(0, len(selectable) - 1)]

                    scroll_to(driver, a)
                    ActionChains(driver).move_to_element(a).click().perform()
                    time.sleep(2)

            requestor = driver.find_element_by_css_selector('div.project-requester-wrap> * div.english-name').text
            approver = driver.find_element_by_css_selector(
                'div.project-approver-wrap> * div.english-name>span:first-child').text
            product_owner = driver.find_element_by_css_selector(
                'div.product-owner-wrap> * div.english-name>span:first-child').text

        except:
            list_fail.append('Issue when selecting Approver/PO')

        try:
            with allure.step('Select startDate/endDate'):
                startDate = driver.find_element_by_css_selector('input[name="startDate"]')
                scroll_to(driver, startDate)
                ActionChains(driver).move_to_element(startDate).click().perform()
                time.sleep(1)
                days = driver.find_elements_by_css_selector('div.react-datepicker__month>div>div.react-datepicker__day')
                ActionChains(driver).move_to_element(days[random.randint(0, len(days) - 1)]).click().perform()
                time.sleep(1)

                endDate = driver.find_element_by_css_selector('input[name="endDate"]')
                scroll_to(driver, endDate)
                ActionChains(driver).move_to_element(endDate).click().perform()
                time.sleep(1)
                for i in range(3):
                    ActionChains(driver).move_to_element(
                        driver.find_element_by_css_selector(
                            'div.justify-center>i.anticon-arrow-right')).click().perform()
                    time.sleep(1.5)
                days = driver.find_elements_by_css_selector('div.react-datepicker__month>div>div.react-datepicker__day')
                ActionChains(driver).move_to_element(days[random.randint(0, len(days) - 1)]).click().perform()
                time.sleep(1)

                feature_startDate = driver.find_element_by_css_selector('input[name="features[0].startDate"]')
                scroll_to(driver, feature_startDate)
                ActionChains(driver).move_to_element(feature_startDate).click().perform()
                time.sleep(1)
                days = driver.find_elements_by_css_selector(
                    'div.react-datepicker__month>div>div.react-datepicker__day:not(.react-datepicker__day--disabled)')
                ActionChains(driver).move_to_element(days[random.randint(0, len(days) - 1)]).click().perform()
                time.sleep(1)

                feature_endDate = driver.find_element_by_css_selector('input[name="features[0].endDate"]')
                scroll_to(driver, feature_endDate)
                ActionChains(driver).move_to_element(feature_endDate).click().perform()
                time.sleep(1)
                ActionChains(driver).move_to_element(
                    driver.find_element_by_css_selector('div.justify-center>i.anticon-arrow-right')).click().perform()
                time.sleep(1)
                days = driver.find_elements_by_css_selector(
                    'div.react-datepicker__month>div>div.react-datepicker__day:not(.react-datepicker__day--disabled)')
                ActionChains(driver).move_to_element(days[random.randint(0, len(days) - 1)]).click().perform()
                time.sleep(1)
        except:
            list_fail.append('Issue when selecting startDate/endDate')

        try:
            with allure.step('Enter description for Feature Map'):
                scroll_to(driver, driver.find_element_by_css_selector('textarea[name="features[0].description"]'))
                time.sleep(1)

                driver.find_element_by_css_selector('textarea[name="features[0].description"]').send_keys(
                    str(time.time()))
                time.sleep(2)
        except:
            list_fail.append('Unable to enter description for Feature Map')

        try:
            with allure.step('Enter description for Statement'):
                scroll_to(driver, driver.find_element_by_css_selector('textarea[name="description"]'))
                time.sleep(1)

                driver.find_element_by_css_selector('textarea[name="description"]').send_keys(str(time.time()))
                time.sleep(2)
        except:
            list_fail.append('Unable to enter description for Statement')

        try:
            with allure.step('Check Save as Draft button'):
                btn_save_draft = driver.find_element_by_css_selector(
                    'div.form-function>button:first-child+button:not(:last-child)')
                time.sleep(0.5)
        except:
            list_fail.append('The Save as Draft button is not available')

        try:
            with allure.step('Click on Save as Draft button'):
                scroll_to(driver, btn_save_draft)

                ActionChains(driver).move_to_element(btn_save_draft).click().perform()
                time.sleep(5)
        except:
            list_fail.append('Unable to click on Save as Draft button')

        try:
            with allure.step('Closing Add New Project popup'):
                self.assertEqual(len(driver.window_handles), 1)
                time.sleep(0.5)
        except:
            list_fail.append('Unable to close the Add New Project popup')

        try:
            with allure.step('Return to main window and reload'):
                driver.switch_to.window(driver.window_handles[0])
                time.sleep(2)

                driver.refresh()
                time.sleep(5)
        except:
            list_fail.append('Error when switching to main window and reload')

        try:
            with allure.step('Search for newly added project'):
                driver.find_element_by_css_selector('li.ant-select-search> * input').send_keys('#name: ' + project_name)
                time.sleep(3)

                ActionChains(driver).move_to_element(
                    driver.find_element_by_css_selector('div.smart-search>button')).click().perform()
                time.sleep(3)
        except:
            list_fail.append('Error when searching text: ' + project_name)

        try:
            with allure.step('Verify only 1 result is displayed'):
                self.assertEqual(len(driver.find_elements_by_css_selector('div.ant-table-scroll> * tbody>tr')), 1)
                time.sleep(0.5)
        except:
            list_fail.append('The search result returns more than 1 record')

        try:
            with allure.step('Verify the status in record'):
                self.assertTrue('Save Draft' in driver.find_element_by_css_selector(
                    'div.ant-table-scroll> * tbody>tr>td:nth-child(9)>div').text)
                time.sleep(0.5)
        except:
            list_fail.append('The status is ' + driver.find_element_by_css_selector(
                'div.ant-table-scroll> * tbody>tr>td:nth-child(9)>div').text + ' instead of [Save Draft]')

        self.assertEqual(len(list_fail), 0, list_fail)

    # QUAL-657
    @allure.story('Check Add successful')
    def test_PS_AddNewProject_All_fields_successfully(self):
        driver = self.driver
        list_fail = []

        # restore the status of page
        # driver.refresh()
        # time.sleep(5)

        try:
            with allure.step('Check button Add new project'):
                btn_add_project = driver.find_element_by_css_selector('div.search-box+div> * button.blue')

                ActionChains(driver).move_to_element(btn_add_project).click().perform()
                time.sleep(3)

                self.assertTrue(len(driver.window_handles) > 1)
                time.sleep(0.01)

                # switch to popup
                driver.switch_to.window(driver.window_handles[1])
                driver.maximize_window()
                time.sleep(1)
        except:
            list_fail.append('No new windows is opened when creating a project')

        try:
            with allure.step('Input project name'):
                project_name = 'HVN_' + str(time.time())
                driver.find_element_by_css_selector('label.label-input+label>input.material-input') \
                    .send_keys(project_name)
                time.sleep(3)
        except:
            list_fail.append('Unable to input new project name')

        try:
            with allure.step('Select an option for all dropdowns'):
                for i in driver.find_elements_by_css_selector('div.react-select__value-container'):
                    scroll_to(driver, i)
                    ActionChains(driver).move_to_element(i).click().perform()
                    time.sleep(1)

                    ActionChains(driver).move_to_element(driver.find_elements_by_css_selector(
                        'div.react-select__menu-list>div')[random.randint(0, len(
                        driver.find_elements_by_css_selector(
                            'div.react-select__menu-list>div')) - 1)]).click().perform()
                    time.sleep(0.5)
        except:
            list_fail.append('Issue with dropdowns')

        try:
            with allure.step('Select Approver/PO'):
                btn_plus = driver.find_elements_by_css_selector('div.plus-btn')
                for i in btn_plus:
                    scroll_to(driver, i)
                    ActionChains(driver).move_to_element(i).click().perform()
                    time.sleep(2)

                    selectable = driver.find_elements_by_css_selector('ul[role="listbox"]>li')
                    a = selectable[random.randint(0, len(selectable) - 1)]

                    scroll_to(driver, a)
                    ActionChains(driver).move_to_element(a).click().perform()
                    time.sleep(2)

            requestor = driver.find_element_by_css_selector('div.project-requester-wrap> * div.english-name').text
            approver = driver.find_element_by_css_selector(
                'div.project-approver-wrap> * div.english-name>span:first-child').text
            product_owner = driver.find_element_by_css_selector(
                'div.product-owner-wrap> * div.english-name>span:first-child').text

        except:
            list_fail.append('Issue when selecting Approver/PO')

        try:
            with allure.step('Select startDate/endDate'):
                startDate = driver.find_element_by_css_selector('input[name="startDate"]')
                scroll_to(driver, startDate)
                ActionChains(driver).move_to_element(startDate).click().perform()
                time.sleep(1)
                days = driver.find_elements_by_css_selector('div.react-datepicker__month>div>div.react-datepicker__day')
                ActionChains(driver).move_to_element(days[random.randint(0, len(days) - 1)]).click().perform()
                time.sleep(1)

                endDate = driver.find_element_by_css_selector('input[name="endDate"]')
                scroll_to(driver, endDate)
                ActionChains(driver).move_to_element(endDate).click().perform()
                time.sleep(1)
                for i in range(3):
                    ActionChains(driver).move_to_element(
                        driver.find_element_by_css_selector(
                            'div.justify-center>i.anticon-arrow-right')).click().perform()
                    time.sleep(1.5)
                days = driver.find_elements_by_css_selector('div.react-datepicker__month>div>div.react-datepicker__day')
                ActionChains(driver).move_to_element(days[random.randint(0, len(days) - 1)]).click().perform()
                time.sleep(1)

                feature_startDate = driver.find_element_by_css_selector('input[name="features[0].startDate"]')
                scroll_to(driver, feature_startDate)
                ActionChains(driver).move_to_element(feature_startDate).click().perform()
                time.sleep(1)
                days = driver.find_elements_by_css_selector(
                    'div.react-datepicker__month>div>div.react-datepicker__day:not(.react-datepicker__day--disabled)')
                ActionChains(driver).move_to_element(days[random.randint(0, len(days) - 1)]).click().perform()
                time.sleep(1)

                feature_endDate = driver.find_element_by_css_selector('input[name="features[0].endDate"]')
                scroll_to(driver, feature_endDate)
                ActionChains(driver).move_to_element(feature_endDate).click().perform()
                time.sleep(1)
                ActionChains(driver).move_to_element(
                    driver.find_element_by_css_selector('div.justify-center>i.anticon-arrow-right')).click().perform()
                time.sleep(1)
                days = driver.find_elements_by_css_selector(
                    'div.react-datepicker__month>div>div.react-datepicker__day:not(.react-datepicker__day--disabled)')
                ActionChains(driver).move_to_element(days[random.randint(0, len(days) - 1)]).click().perform()
                time.sleep(1)
        except:
            list_fail.append('Issue when selecting startDate/endDate')

        try:
            with allure.step('Enter description for Feature Map'):
                scroll_to(driver, driver.find_element_by_css_selector('textarea[name="features[0].description"]'))
                time.sleep(1)

                driver.find_element_by_css_selector('textarea[name="features[0].description"]').send_keys(
                    str(time.time()))
                time.sleep(2)
        except:
            list_fail.append('Unable to enter description for Feature Map')

        try:
            with allure.step('Enter description for Statement'):
                scroll_to(driver, driver.find_element_by_css_selector('textarea[name="description"]'))
                time.sleep(1)

                driver.find_element_by_css_selector('textarea[name="description"]').send_keys(str(time.time()))
                time.sleep(2)
        except:
            list_fail.append('Unable to enter description for Statement')

        try:
            with allure.step('Enter Warranty period'):
                scroll_to(driver, driver.find_element_by_css_selector('input[name="warrantyPeriod"]'))
                time.sleep(1)

                driver.find_element_by_css_selector('input[name="warrantyPeriod"]').send_keys(
                    str(random.randint(1, 12)))
                time.sleep(1)
        except:
            list_fail.append('Unable to enter warranty month')

        try:
            with allure.step('Enter Warranty description'):
                driver.find_element_by_css_selector('textarea[name="warrantyDes"]').send_keys(
                    """Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. 
                    Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.""")
                time.sleep(2)
        except:
            list_fail.append('Unable to enter description for Warranty')

        try:
            with allure.step('Verify Submit button'):
                self.assertEqual(len(driver.find_elements_by_css_selector('div.form-function>button:first-child')), 1)
                time.sleep(0.5)
        except:
            list_fail.append('The Submit button is not displayed')

        try:
            with allure.step('Click on Submit button'):
                ActionChains(driver).move_to_element(
                    driver.find_element_by_css_selector('div.form-function>button:first-child')).click().perform()
                time.sleep(1)
        except:
            list_fail.append('Unable to click on Submit button')

        driver.switch_to.window(driver.window_handles[0])
        time.sleep(1)

        try:
            with allure.step('Verify the status popup'):
                self.assertEqual(driver.find_element_by_css_selector('div.ant-notification-notice-description').text,
                                 'The project is submitted')
                time.sleep(0.5)
        except:
            list_fail.append('The Submit process is not success')

        self.assertEqual(len(list_fail), 0, list_fail)

        requestor_email = ''
        approver_email = ''
        product_owner_email = ''

        for i in api_full_employees():
            if i['englishName'] == requestor:
                requestor_email = i['email']
            elif i['englishName'] == approver:
                approver_email = i['email']
            elif i['englishName'] == product_owner:
                product_owner_email = i['email']

        # **************************************************************************************************************
        # Check email from dtb
        query_email = """
                        Select * from dbo.Common_SystemEmail 
                        Where Subject like '%Submit project """ + str(project_name) + "%' Order by CreateDate Desc"

        email_dtb = querydb(query_email)
        email_dtb = list(email_dtb[0])
        time.sleep(0.5)

        try:
            with allure.step('Check Title field in email'):
                self.assertEqual(email_dtb[2], 'Submit project ' + project_name)
        except:
            list_fail.append('The email title is displayed as [' + email_dtb[2] + '] instead of [Submit project ' +
                             project_name + ']')
            pass

        try:
            with allure.step('Check To field in email'):
                self.assertTrue(approver_email in email_dtb[3])
                time.sleep(0.5)
        except:
            list_fail.append('The email is sent TO: [' + email_dtb[3] + '] instead of [' + approver_email + ']')
            pass

        try:
            with allure.step('Check requestor in email CC'):
                self.assertTrue(requestor_email in email_dtb[4])
                time.sleep(0.5)
        except:
            list_fail.append('The requestor email is not included in CC list')
            pass

        try:
            with allure.step('Check PO in email CC'):
                self.assertTrue(product_owner_email in email_dtb[4])
                time.sleep(0.5)
        except:
            list_fail.append('The PO email is not included in CC list')
            pass

        try:
            with allure.step('Check image in email'):
                self.assertTrue('<img src="' in email_dtb[6])
                time.sleep(0.5)
        except:
            list_fail.append('No image src in email content')
            pass

        try:
            with allure.step('Check Go to project text'):
                self.assertTrue(re.search(r"Go to project", email_dtb[6]))
                time.sleep(0.5)
        except:
            list_fail.append('The Go to request text does not exist')
            pass

        try:
            with allure.step('Check Go to project link'):
                self.assertTrue(re.search(r'<a href="http(.*?)>Go to project<\/a>', email_dtb[6]))
                time.sleep(0.5)
        except:
            list_fail.append('The Go to request does not contain any link')
            pass

        # **************************************************************************************************************
        # extract action link and reject
        try:
            with allure.step('Fetch the URL, login with approver account and reject the request'):
                action_url = re.split(r'<a href="(.*?)" style', email_dtb[6])[1]

                options = webdriver.ChromeOptions()
                options.add_argument("--incognito")

                driver = webdriver.Chrome(options=options)
                driver.get(action_url)

                driver.maximize_window()
                time.sleep(0.5)

                driver.find_element_by_css_selector('#username').send_keys(approver_email)
                driver.find_element_by_css_selector('#password').send_keys('@admin')
                driver.find_element_by_css_selector('button.login-btn').click()
                time.sleep(10)

                driver.get(action_url)
                time.sleep(5)

                ActionChains(driver).move_to_element(
                    driver.find_element_by_css_selector('div.form-function>button:last-child')).click().perform()
                time.sleep(1)
        except:
            list_fail.append('The rejection process encounters an error')

        try:
            with allure.step('Verify that the submit is rejected'):
                driver.get(url_root + '/project-visibility/project-status')
                time.sleep(7)

                driver.find_element_by_css_selector('div.smart-search> * input').send_keys(project_name)
                time.sleep(0.5)

                ActionChains(driver).move_to_element(
                    driver.find_element_by_css_selector('div.smart-search>button')).click().perform()
                time.sleep(2)

                self.assertEqual(
                    driver.find_element_by_css_selector('div.ant-table-scroll> * td:nth-child(9)>div').text,
                    'REJECTED')
                time.sleep(0.5)
        except:
            list_fail.append('The project status is ' + driver.find_element_by_css_selector(
                'div.ant-table-scroll> * td:nth-child(9)>div').text + ' instead of [REJECTED]')
            pass

        try:
            with allure.step('Verify text Rejected'):
                self.assertEqual(
                    driver.find_element_by_css_selector('div.ant-table-scroll> * td:nth-child(9)>div').text,
                    'REJECTED')
                time.sleep(0.5)
        except:
            list_fail.append('The status is ' +
                             driver.find_element_by_css_selector('div.ant-table-scroll> * td:nth-child(9)>div').text +
                             ' instead of [REJECTED]')

        time.sleep(0.5)

        self.assertEqual(len(list_fail), 0, list_fail)

    # QUAL-656
    @allure.story('Check Add mandatory fields successful')
    def test_PS_AddNewProject_Mandatory_fields_successfully(self):
        driver = self.driver
        list_fail = []

        try:
            with allure.step('Check button Add new project'):
                btn_add_project = driver.find_element_by_css_selector('div.search-box+div> * button.blue')

                ActionChains(driver).move_to_element(btn_add_project).click().perform()
                time.sleep(3)

                self.assertTrue(len(driver.window_handles) > 1)
                time.sleep(0.01)

                # switch to popup
                driver.switch_to.window(driver.window_handles[1])
                driver.maximize_window()
                time.sleep(1)
        except:
            list_fail.append('No new windows is opened when creating a project')

        try:
            with allure.step('Input project name'):
                project_name = 'HVN_' + str(time.time())
                driver.find_element_by_css_selector('label.label-input+label>input.material-input') \
                    .send_keys(project_name)
                time.sleep(3)
        except:
            list_fail.append('Unable to input new project name')

        try:
            with allure.step('Select an option for all dropdowns'):
                for i in driver.find_elements_by_css_selector('div.react-select__value-container'):
                    scroll_to(driver, i)
                    ActionChains(driver).move_to_element(i).click().perform()
                    time.sleep(1)

                    ActionChains(driver).move_to_element(driver.find_elements_by_css_selector(
                        'div.react-select__menu-list>div')[random.randint(0, len(
                        driver.find_elements_by_css_selector(
                            'div.react-select__menu-list>div')) - 1)]).click().perform()
                    time.sleep(0.5)
        except:
            list_fail.append('Issue with dropdowns')

        try:
            with allure.step('Select Approver/PO'):
                btn_plus = driver.find_elements_by_css_selector('div.plus-btn')
                for i in btn_plus:
                    scroll_to(driver, i)
                    ActionChains(driver).move_to_element(i).click().perform()
                    time.sleep(2)

                    selectable = driver.find_elements_by_css_selector('ul[role="listbox"]>li')
                    a = selectable[random.randint(0, len(selectable) - 1)]

                    scroll_to(driver, a)
                    ActionChains(driver).move_to_element(a).click().perform()
                    time.sleep(2)

            requestor = driver.find_element_by_css_selector('div.project-requester-wrap> * div.english-name').text
            approver = driver.find_element_by_css_selector(
                'div.project-approver-wrap> * div.english-name>span:first-child').text
            product_owner = driver.find_element_by_css_selector(
                'div.product-owner-wrap> * div.english-name>span:first-child').text

        except:
            list_fail.append('Issue when selecting Approver/PO')

        try:
            with allure.step('Select startDate/endDate'):
                startDate = driver.find_element_by_css_selector('input[name="startDate"]')
                scroll_to(driver, startDate)
                ActionChains(driver).move_to_element(startDate).click().perform()
                time.sleep(1)
                days = driver.find_elements_by_css_selector('div.react-datepicker__month>div>div.react-datepicker__day')
                ActionChains(driver).move_to_element(days[random.randint(0, len(days) - 1)]).click().perform()
                time.sleep(1)

                endDate = driver.find_element_by_css_selector('input[name="endDate"]')
                scroll_to(driver, endDate)
                ActionChains(driver).move_to_element(endDate).click().perform()
                time.sleep(1)
                for i in range(3):
                    ActionChains(driver).move_to_element(
                        driver.find_element_by_css_selector(
                            'div.justify-center>i.anticon-arrow-right')).click().perform()
                    time.sleep(1.5)
                days = driver.find_elements_by_css_selector('div.react-datepicker__month>div>div.react-datepicker__day')
                ActionChains(driver).move_to_element(days[random.randint(0, len(days) - 1)]).click().perform()
                time.sleep(1)

                feature_startDate = driver.find_element_by_css_selector('input[name="features[0].startDate"]')
                scroll_to(driver, feature_startDate)
                ActionChains(driver).move_to_element(feature_startDate).click().perform()
                time.sleep(1)
                days = driver.find_elements_by_css_selector(
                    'div.react-datepicker__month>div>div.react-datepicker__day:not(.react-datepicker__day--disabled)')
                ActionChains(driver).move_to_element(days[random.randint(0, len(days) - 1)]).click().perform()
                time.sleep(1)

                feature_endDate = driver.find_element_by_css_selector('input[name="features[0].endDate"]')
                scroll_to(driver, feature_endDate)
                ActionChains(driver).move_to_element(feature_endDate).click().perform()
                time.sleep(1)
                ActionChains(driver).move_to_element(
                    driver.find_element_by_css_selector('div.justify-center>i.anticon-arrow-right')).click().perform()
                time.sleep(1)
                days = driver.find_elements_by_css_selector(
                    'div.react-datepicker__month>div>div.react-datepicker__day:not(.react-datepicker__day--disabled)')
                ActionChains(driver).move_to_element(days[random.randint(0, len(days) - 1)]).click().perform()
                time.sleep(1)
        except:
            list_fail.append('Issue when selecting startDate/endDate')

        try:
            with allure.step('Enter description for Feature Map'):
                scroll_to(driver, driver.find_element_by_css_selector('textarea[name="features[0].description"]'))
                time.sleep(1)

                driver.find_element_by_css_selector('textarea[name="features[0].description"]').send_keys(
                    str(time.time()))
                time.sleep(2)
        except:
            list_fail.append('Unable to enter description for Feature Map')

        try:
            with allure.step('Enter description for Statement'):
                scroll_to(driver, driver.find_element_by_css_selector('textarea[name="description"]'))
                time.sleep(1)

                driver.find_element_by_css_selector('textarea[name="description"]').send_keys(str(time.time()))
                time.sleep(2)
        except:
            list_fail.append('Unable to enter description for Statement')

        try:
            with allure.step('Enter Warranty period'):
                scroll_to(driver, driver.find_element_by_css_selector('input[name="warrantyPeriod"]'))
                time.sleep(1)

                driver.find_element_by_css_selector('input[name="warrantyPeriod"]').send_keys(
                    str(random.randint(1, 12)))
                time.sleep(1)
        except:
            list_fail.append('Unable to enter warranty month')

        try:
            with allure.step('Enter Warranty description'):
                driver.find_element_by_css_selector('textarea[name="warrantyDes"]').send_keys(
                    """Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. 
                    Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.""")
                time.sleep(2)
        except:
            list_fail.append('Unable to enter description for Warranty')

        try:
            with allure.step('Click on Add Managers button'):
                scroll_to(driver, driver.find_element_by_css_selector('div.manager-area> * button'))

                ActionChains(driver).move_to_element(driver.find_element_by_css_selector(
                    'div.manager-area> * button:not(.ant-btn-icon-only)')).click().perform()
                time.sleep(1.5)

                self.assertEqual(len(driver.find_elements_by_css_selector(
                    'div.manager-area>div.table-body>div.ant-empty')), 0)
        except:
            list_fail.append('After clicking on Add manager, the input row is not displayed')

        try:
            with allure.step('Add Manager name'):
                ActionChains(driver).move_to_element(
                    driver.find_element_by_css_selector('div.manager-area> * div.manager-wrap> * input'))\
                    .click().perform()
                time.sleep(1.5)

                selectable = driver.find_elements_by_css_selector('ul.ant-select-dropdown-menu>li')
                ActionChains(driver).move_to_element(
                    selectable[random.randint(0, len(selectable) - 1)]).click().perform()
                time.sleep(1.5)
        except:
            list_fail.append('Unable to select Manager name')

        try:
            with allure.step('Add Manager role'):
                scroll_to(driver, driver.find_element_by_css_selector('div.manager-area> * div.react-select__control'))
                ActionChains(driver).move_to_element(
                    driver.find_element_by_css_selector('div.manager-area> * div.react-select__control')) \
                    .click().perform()
                time.sleep(1.5)

                selectable = driver.find_elements_by_css_selector('div.react-select__menu-list>div')
                ActionChains(driver).move_to_element(
                    selectable[random.randint(0, len(selectable) - 1)]).click().perform()
                time.sleep(1.5)
        except:
            list_fail.append('Unable to select Manager role')

        try:
            with allure.step('Add MM'):
                driver.find_element_by_css_selector(
                    'div.manager-area> * input.material-input').send_keys(random.randint(0, 10))
                time.sleep(1.5)
        except:
            list_fail.append('Unable to enter Managers MM value')

        try:
            with allure.step('Click on Add member button'):
                scroll_to(driver, driver.find_element_by_css_selector('div.feature-area> * button.ant-btn-circle'))

                ActionChains(driver).move_to_element(
                    driver.find_element_by_css_selector('div.feature-area> * button.ant-btn-circle')).click().perform()
                time.sleep(1.5)

                self.assertEqual(len(driver.find_elements_by_css_selector(
                    'div.feature-area>div.table-body>div.ant-empty')), 0)
        except:
            list_fail.append('After clicking on Add member, the input row is not displayed')

        try:
            with allure.step('Add member name'):
                ActionChains(driver).move_to_element(
                    driver.find_element_by_css_selector('div.feature-area> * div.manager-wrap> * input'))\
                    .click().perform()
                time.sleep(1.5)

                selectable = driver.find_elements_by_css_selector('ul.ant-select-dropdown-menu>li')
                ActionChains(driver).move_to_element(
                    selectable[random.randint(0, len(selectable) - 1)]).click().perform()
                time.sleep(1)

                if len(driver.find_elements_by_css_selector('div.dialog-title')) == 1:
                    ActionChains(driver).move_to_element(
                        driver.find_element_by_css_selector(
                            'div.warning-workload-wrap>div.bottom-function>button')).click().perform()
                    time.sleep(1.5)
        except:
            list_fail.append('Unable to select member name')

        try:
            with allure.step('Add MM'):
                driver.find_element_by_css_selector(
                    'div.feature-area> * input.material-input').send_keys(random.randint(0, 10))
                time.sleep(1.5)
        except:
            list_fail.append('Unable to enter Feature Map MM value')

        try:
            with allure.step('Verify Submit button'):
                self.assertEqual(len(driver.find_elements_by_css_selector('div.form-function>button:first-child')), 1)
                time.sleep(1.5)
        except:
            list_fail.append('The Submit button is not displayed')

        try:
            with allure.step('Click on Submit button'):
                ActionChains(driver).move_to_element(
                    driver.find_element_by_css_selector('div.form-function>button:first-child')).click().perform()
                time.sleep(1)
        except:
            list_fail.append('Unable to click on Submit button')

        driver.switch_to.window(driver.window_handles[0])
        time.sleep(1)

        try:
            with allure.step('Verify the status popup'):
                self.assertEqual(driver.find_element_by_css_selector('div.ant-notification-notice-description').text,
                                 'The project is submitted')
                time.sleep(0.5)
        except:
            list_fail.append('The Submit process is not success')

        self.assertEqual(len(list_fail), 0, list_fail)

        requestor_email = ''
        approver_email = ''
        product_owner_email = ''

        for i in api_full_employees():
            if i['englishName'] == requestor:
                requestor_email = i['email']
            elif i['englishName'] == approver:
                approver_email = i['email']
            elif i['englishName'] == product_owner:
                product_owner_email = i['email']

        # **************************************************************************************************************
        # Check email from dtb
        query_email = """
                                Select * from dbo.Common_SystemEmail 
                                Where Subject like '%Submit project """ + str(
            project_name) + "%' Order by CreateDate Desc"

        email_dtb = querydb(query_email)
        email_dtb = list(email_dtb[0])
        time.sleep(0.5)

        try:
            with allure.step('Check Title field in email'):
                self.assertEqual(email_dtb[2], 'Submit project ' + project_name)
        except:
            list_fail.append('The email title is displayed as [' + email_dtb[2] + '] instead of [Submit project ' +
                             project_name + ']')
            pass

        try:
            with allure.step('Check To field in email'):
                self.assertTrue(approver_email in email_dtb[3])
                time.sleep(0.5)
        except:
            list_fail.append('The email is sent TO: [' + email_dtb[3] + '] instead of [' + approver_email + ']')
            pass

        try:
            with allure.step('Check requestor in email CC'):
                self.assertTrue(requestor_email in email_dtb[4])
                time.sleep(0.5)
        except:
            list_fail.append('The requestor email is not included in CC list')
            pass

        try:
            with allure.step('Check PO in email CC'):
                self.assertTrue(product_owner_email in email_dtb[4])
                time.sleep(0.5)
        except:
            list_fail.append('The PO email is not included in CC list')
            pass

        try:
            with allure.step('Check image in email'):
                self.assertTrue('<img src="' in email_dtb[6])
                time.sleep(0.5)
        except:
            list_fail.append('No image src in email content')
            pass

        try:
            with allure.step('Check Go to project text'):
                self.assertTrue(re.search(r"Go to project", email_dtb[6]))
                time.sleep(0.5)
        except:
            list_fail.append('The Go to request text does not exist')
            pass

        try:
            with allure.step('Check Go to project link'):
                self.assertTrue(re.search(r'<a href="http(.*?)>Go to project<\/a>', email_dtb[6]))
                time.sleep(0.5)
        except:
            list_fail.append('The Go to request does not contain any link')
            pass

        # **************************************************************************************************************
        # extract action link and approve
        try:
            with allure.step('Fetch the URL, login with approver account and reject the request'):
                action_url = re.split(r'<a href="(.*?)" style', email_dtb[6])[1]

                options = webdriver.ChromeOptions()
                options.add_argument("--incognito")

                driver = webdriver.Chrome(options=options)
                driver.get(action_url)

                driver.maximize_window()
                time.sleep(0.5)

                driver.find_element_by_css_selector('#username').send_keys(approver_email)
                driver.find_element_by_css_selector('#password').send_keys('@admin')
                driver.find_element_by_css_selector('button.login-btn').click()
                time.sleep(10)

                driver.get(action_url)
                time.sleep(5)

                self.assertEqual(
                    driver.find_element_by_css_selector(
                        'div.form-header> * div.common-field:last-child> * label+div:not(.custom-select)').text,
                    'WAITING FOR APPROVAL')
                time.sleep(0.5)
        except:
            list_fail.append('The status of project is [' + driver.find_element_by_css_selector(
                        'div.form-header> * div.common-field:last-child> * label+div:not(.custom-select)').text +
                             '] instead of [WAITING FOR APPROVAL]')
            pass

        try:
            with allure.step('Approve the project'):
                ActionChains(driver).move_to_element(
                    driver.find_element_by_css_selector('div.form-function>button:first-child')).click().perform()
                time.sleep(1)
        except:
            list_fail.append('The approve process encounters an error')

        try:
            with allure.step('Verify that the submit is approved'):
                driver.get(url_root + '/project-visibility/project-status')
                time.sleep(7)

                driver.find_element_by_css_selector('div.smart-search> * input').send_keys(project_name)
                time.sleep(0.5)

                ActionChains(driver).move_to_element(
                    driver.find_element_by_css_selector('div.smart-search>button')).click().perform()
                time.sleep(2)

                self.assertEqual(
                    driver.find_element_by_css_selector('div.ant-table-scroll> * td:nth-child(9)>div').text,
                    'APPROVED')
                time.sleep(0.5)
        except:
            list_fail.append('The request is not approved')
            pass

        try:
            with allure.step('Verify text Approved'):
                self.assertEqual(
                    driver.find_element_by_css_selector('div.ant-table-scroll> * td:nth-child(9)>div').text,
                    'APPROVED')
                time.sleep(0.5)
        except:
            list_fail.append('The status is ' +
                             driver.find_element_by_css_selector('div.ant-table-scroll> * td:nth-child(9)>div').text +
                             ' instead of [WAITING FOR APPROVAL]')

        time.sleep(0.5)

        self.assertEqual(len(list_fail), 0, list_fail)


class ProjectDetail(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.driver = webdriver.Chrome('chromedriver.exe')
        driver = self.driver
        driver.maximize_window()

        # Log in
        login(driver)
        time.sleep(5)

        # Moving to test page
        driver.get(url_root + '/project-visibility/project-status')
        time.sleep(10)

    @classmethod
    def tearDownClass(self):
        self.driver.quit()

    # QUAL-655
    @allure.story('Check download project detail as pdf file')
    def test_PS_ProjectDetail_download_as_PDF(self):
        driver = self.driver
        list_fail = []

        try:
            with allure.step('Verify new windows when clicking on a project'):
                lst = driver.find_elements_by_css_selector(
                    'div.ant-table-fixed-left> * table.ant-table-fixed>tbody>tr>td:last-child>div>div:first-child')

                ActionChains(driver).move_to_element(lst[random.randint(0, len(lst) - 1)]).click().perform()
                time.sleep(10)

                self.assertTrue(len(driver.window_handles) > 1)
                time.sleep(0.01)

                # switch to popup
                driver.switch_to.window(driver.window_handles[1])
                time.sleep(0.5)
        except:
            list_fail.append('No new windows is opened when clicking on a project')

        try:
            with allure.step('Check Download as PDF button'):
                self.assertTrue(len(driver.find_elements_by_css_selector('button.btn-pdf')) > 0)
                time.sleep(0.5)
        except:
            list_fail.append('The Download as PDF button is not displayed')

        try:
            # Check default files in download folder
            download_directory = 'C:/Users/tmtuan/Downloads'
            a = len([name for name in os.listdir(download_directory)
                     if os.path.isfile(os.path.join(download_directory, name))])

            with allure.step('Click on Download as PDF button'):
                ActionChains(driver).move_to_element(
                    driver.find_element_by_css_selector('button.btn-pdf')).click().perform()
                time.sleep(0.5)
        except:
            list_fail.append('The Download as PDF button is unclickable')

        try:
            time.sleep(5)
            with allure.step('Verify that file has been downloaded'):
                b = len([name for name in os.listdir(download_directory)
                         if os.path.isfile(os.path.join(download_directory, name))])
                self.assertNotEqual(a, b)
                time.sleep(0.5)
        except:
            list_fail.append('No file is downloaded')

        self.assertListEqual(list_fail, [])


# **********************************************************************************************************************

# class Sort(unittest.TestCase):
#
#     @classmethod
#     def setUpClass(self):
#         self.driver = webdriver.Chrome('chromedriver.exe')
#         driver = self.driver
#         driver.maximize_window()
#         # Log in
#         login(driver)
#         time.sleep(5)
#         driver.get(url_root + '/project-visibility/project-status')
#         time.sleep(10)
#
#     @classmethod
#     def tearDownClass(self):
#         self.driver.quit()
#
#     @allure.story('Check sort by Name')
#     def test_PS_Sort_name(self):
#         list_step_fail = []
#         driver = self.driver
#         column = 'Name'
#         # Sort increase
#         sorted_increase = Helper.Helper_common.sorting_increase_column(driver, column)
#         expected = sorted(sorted_increase)
#         try:
#             with allure.step('Check result sorted Increase by ' + column):
#                 self.assertListEqual(sorted_increase, expected, 'Check sort increase by ' + column)
#         except AssertionError:
#             list_step_fail.append('1.1 Check sort increase by ' + column)
#
#         actual_color = driver.find_element_by_xpath(
#             '//div[@class="ant-table-fixed-left"]//tr/th//span[text()="' + column
#             + '"]/..//div/i[@class="anticon anticon-caret-up ant-table-column-sorter-up on"]')
#         actual_color = actual_color.value_of_css_property('color')
#         actual_color = Helper.Helper_common.rbga_to_hex(actual_color)
#         expected_color = '#1890ff'
#         try:
#             with allure.step('Check icon arrow up highlight'):
#                 self.assertEqual(actual_color, expected_color, 'Check increase sort is highlight')
#         except AssertionError:
#             list_step_fail.append('1.2 Check icon arrow up highlight')
#
#         # Click back to first page
#         list_page = driver.find_elements_by_css_selector('.ant-pagination>li')
#         if len(list_page):
#             driver.find_element_by_css_selector('.ant-pagination>li.ant-pagination-item-1').click()
#
#         # Sort Decrease
#         sorted_decrease = Helper.Helper_common.sorting_decrease_column(driver, column)
#         expected = sorted(sorted_decrease, reverse=True)
#         try:
#             with allure.step('Check result sorted Decrease by ' + column):
#                 self.assertListEqual(sorted_decrease, expected, 'Check sort decrease by ' + column)
#         except AssertionError:
#             list_step_fail.append('2.1 Check sort decrease by ' + column)
#
#         actual_color = driver.find_element_by_xpath(
#             '//div[@class="ant-table-fixed-left"]//tr/th//span[text()="' + column
#             + '"]/..//div/i[@class="anticon anticon-caret-down ant-table-column-sorter-down on"]')
#         actual_color = actual_color.value_of_css_property('color')
#         actual_color = Helper.Helper_common.rbga_to_hex(actual_color)
#         expected_color = '#1890ff'
#         try:
#             with allure.step('Check icon arrow down highlight'):
#                 self.assertEqual(actual_color, expected_color, 'Check decrease sort is highlight')
#         except AssertionError:
#             list_step_fail.append('2.2 Check icon arrow down highlight')
#
#         self.assertListEqual(list_step_fail, [])
#
#     def test_PS_Sort_customer(self):
#         list_step_fail = []
#         driver = self.driver
#         driver.refresh()
#         time.sleep(5)
#         column = 'Customer'
#         # Sort increase
#         sorted_increase = Helper.Helper_common.sorting_increase_column(driver, column)
#         expected = sorted(sorted_increase, key=str.lower)
#         try:
#             with allure.step('Check result sorted Increase by ' + column):
#                 self.assertListEqual(sorted_increase, expected, 'Check sort increase by ' + column)
#         except AssertionError:
#             list_step_fail.append('1.1 Check sort increase by ' + column)
#
#         actual_color = driver.find_element_by_xpath(
#             '//div[@class="ant-table-scroll"]//tr/th//span[text()="' + column
#             + '"]/..//div/i[@class="anticon anticon-caret-up ant-table-column-sorter-up on"]')
#         actual_color = actual_color.value_of_css_property('color')
#         actual_color = Helper.Helper_common.rbga_to_hex(actual_color)
#         expected_color = '#1890ff'
#         try:
#             with allure.step('Check icon arrow up highlight'):
#                 self.assertEqual(actual_color, expected_color, 'Check increase sort is highlight')
#         except AssertionError:
#             list_step_fail.append('1.2 Check icon arrow up highlight')
#
#         # Click back to first page
#         list_page = driver.find_elements_by_css_selector('.ant-pagination>li')
#         if len(list_page):
#             driver.find_element_by_css_selector('.ant-pagination>li.ant-pagination-item-1').click()
#
#         driver.refresh()
#
#         # Sort Decrease
#         sorted_decrease = Helper.Helper_common.sorting_decrease_column(driver, column)
#         expected = sorted(sorted_decrease, key=str.lower, reverse=True)
#         try:
#             with allure.step('Check result sorted Decrease by ' + column):
#                 self.assertListEqual(sorted_decrease, expected, 'Check sort decrease by ' + column)
#         except AssertionError:
#             list_step_fail.append('2.1 Check sort decrease by ' + column)
#
#         actual_color = driver.find_element_by_xpath(
#             '//div[@class="ant-table-scroll"]//tr/th//span[text()="' + column
#             + '"]/..//div/i[@class="anticon anticon-caret-down ant-table-column-sorter-down on"]')
#         actual_color = actual_color.value_of_css_property('color')
#         actual_color = Helper.Helper_common.rbga_to_hex(actual_color)
#         expected_color = '#1890ff'
#         try:
#             with allure.step('Check icon arrow down highlight'):
#                 self.assertEqual(actual_color, expected_color, 'Check decrease sort is highlight')
#         except AssertionError:
#             list_step_fail.append('2.2 Check icon arrow down highlight')
#
#         self.assertListEqual(list_step_fail, [])
#
#     def test_PS_Sort_department(self):
#         list_step_fail = []
#         driver = self.driver
#         driver.refresh()
#         time.sleep(5)
#         column = 'Department'
#         # Sort increase
#         sorted_increase = Helper.Helper_common.sorting_increase_column(driver, column)
#         expected = sorted(sorted_increase, key=str.lower)
#         try:
#             with allure.step('Check result sorted Increase by ' + column):
#                 self.assertListEqual(sorted_increase, expected, 'Check sort increase by ' + column)
#         except AssertionError:
#             list_step_fail.append('1.1 Check sort increase by ' + column)
#
#         actual_color = driver.find_element_by_xpath(
#             '//div[@class="ant-table-scroll"]//tr/th//span[text()="' + column
#             + '"]/..//div/i[@class="anticon anticon-caret-up ant-table-column-sorter-up on"]')
#         actual_color = actual_color.value_of_css_property('color')
#         actual_color = Helper.Helper_common.rbga_to_hex(actual_color)
#         expected_color = '#1890ff'
#         try:
#             with allure.step('Check icon arrow up highlight'):
#                 self.assertEqual(actual_color, expected_color, 'Check increase sort is highlight')
#         except AssertionError:
#             list_step_fail.append('1.2 Check icon arrow up highlight')
#
#         # Click back to first page
#         list_page = driver.find_elements_by_css_selector('.ant-pagination>li')
#         if len(list_page):
#             driver.find_element_by_css_selector('.ant-pagination>li.ant-pagination-item-1').click()
#
#         driver.refresh()
#
#         # Sort Decrease
#         sorted_decrease = Helper.Helper_common.sorting_decrease_column(driver, column)
#         expected = sorted(sorted_decrease, key=str.lower, reverse=True)
#         try:
#             with allure.step('Check result sorted Decrease by ' + column):
#                 self.assertListEqual(sorted_decrease, expected, 'Check sort decrease by ' + column)
#         except AssertionError:
#             list_step_fail.append('2.1 Check sort decrease by ' + column)
#
#         actual_color = driver.find_element_by_xpath(
#             '//div[@class="ant-table-scroll"]//tr/th//span[text()="' + column
#             + '"]/..//div/i[@class="anticon anticon-caret-down ant-table-column-sorter-down on"]')
#         actual_color = actual_color.value_of_css_property('color')
#         actual_color = Helper.Helper_common.rbga_to_hex(actual_color)
#         expected_color = '#1890ff'
#         try:
#             with allure.step('Check icon arrow down highlight'):
#                 self.assertEqual(actual_color, expected_color, 'Check decrease sort is highlight')
#         except AssertionError:
#             list_step_fail.append('2.2 Check icon arrow down highlight')
#
#         self.assertListEqual(list_step_fail, [])
#
#     def test_PS_Sort_total_budget(self):
#         list_step_fail = []
#         driver = self.driver
#         driver.refresh()
#         time.sleep(5)
#         column = 'Total budget'
#         # Sort increase
#         sorted_increase = Helper.Helper_common.sorting_increase_column(driver, column)
#         expected = sorted(sorted_increase, reverse=True)
#         try:
#             with allure.step('Check result sorted Increase by ' + column):
#                 self.assertListEqual(sorted_increase, expected, 'Check sort increase by ' + column)
#         except AssertionError:
#             list_step_fail.append('1.1 Check sort increase by ' + column)
#
#         actual_color = driver.find_element_by_xpath(
#             '//div[@class="ant-table-scroll"]//tr/th//span[text()="' + column
#             + '"]/..//div/i[@class="anticon anticon-caret-up ant-table-column-sorter-up on"]')
#         actual_color = actual_color.value_of_css_property('color')
#         actual_color = Helper.Helper_common.rbga_to_hex(actual_color)
#         expected_color = '#1890ff'
#         try:
#             with allure.step('Check icon arrow up highlight'):
#                 self.assertEqual(actual_color, expected_color, 'Check increase sort is highlight')
#         except AssertionError:
#             list_step_fail.append('1.2 Check icon arrow up highlight')
#
#         # Click back to first page
#         list_page = driver.find_elements_by_css_selector('.ant-pagination>li')
#         if len(list_page):
#             driver.find_element_by_css_selector('.ant-pagination>li.ant-pagination-item-1').click()
#
#         driver.refresh()
#
#         # Sort Decrease
#         sorted_decrease = Helper.Helper_common.sorting_decrease_column(driver, column)
#         expected = sorted(sorted_decrease)
#         try:
#             with allure.step('Check result sorted Decrease by ' + column):
#                 self.assertListEqual(sorted_decrease, expected, 'Check sort decrease by ' + column)
#         except AssertionError:
#             list_step_fail.append('2.1 Check sort decrease by ' + column)
#
#         actual_color = driver.find_element_by_xpath(
#             '//div[@class="ant-table-scroll"]//tr/th//span[text()="' + column
#             + '"]/..//div/i[@class="anticon anticon-caret-down ant-table-column-sorter-down on"]')
#         actual_color = actual_color.value_of_css_property('color')
#         actual_color = Helper.Helper_common.rbga_to_hex(actual_color)
#         expected_color = '#1890ff'
#         try:
#             with allure.step('Check icon arrow down highlight'):
#                 self.assertEqual(actual_color, expected_color, 'Check decrease sort is highlight')
#         except AssertionError:
#             list_step_fail.append('2.2 Check icon arrow down highlight')
#
#         self.assertListEqual(list_step_fail, [])
#
#     def test_PS_Sort_status(self):
#         list_step_fail = []
#         driver = self.driver
#         driver.refresh()
#         time.sleep(5)
#         column = 'Status'
#         # Sort increase
#         sorted_increase = Helper.Helper_common.sorting_increase_column(driver, column)
#         expected = sorted(sorted_increase, key=str.lower)
#         try:
#             with allure.step('Check result sorted Increase by ' + column):
#                 self.assertListEqual(sorted_increase, expected, 'Check sort increase by ' + column)
#         except AssertionError:
#             list_step_fail.append('1.1 Check sort increase by ' + column)
#
#         actual_color = driver.find_element_by_xpath(
#             '//div[@class="ant-table-scroll"]//tr/th//span[text()="' + column
#             + '"]/..//div/i[@class="anticon anticon-caret-up ant-table-column-sorter-up on"]')
#         actual_color = actual_color.value_of_css_property('color')
#         actual_color = Helper.Helper_common.rbga_to_hex(actual_color)
#         expected_color = '#1890ff'
#         try:
#             with allure.step('Check icon arrow up highlight'):
#                 self.assertEqual(actual_color, expected_color, 'Check increase sort is highlight')
#         except AssertionError:
#             list_step_fail.append('1.2 Check icon arrow up highlight')
#
#         # Click back to first page
#         list_page = driver.find_elements_by_css_selector('.ant-pagination>li')
#         if len(list_page):
#             driver.find_element_by_css_selector('.ant-pagination>li.ant-pagination-item-1').click()
#
#         driver.refresh()
#
#         # Sort Decrease
#         sorted_decrease = Helper.Helper_common.sorting_decrease_column(driver, column)
#         expected = sorted(sorted_decrease, key=str.lower, reverse=True)
#         try:
#             with allure.step('Check result sorted Decrease by ' + column):
#                 self.assertListEqual(sorted_decrease, expected, 'Check sort decrease by ' + column)
#         except AssertionError:
#             list_step_fail.append('2.1 Check sort decrease by ' + column)
#
#         actual_color = driver.find_element_by_xpath(
#             '//div[@class="ant-table-scroll"]//tr/th//span[text()="' + column
#             + '"]/..//div/i[@class="anticon anticon-caret-down ant-table-column-sorter-down on"]')
#         actual_color = actual_color.value_of_css_property('color')
#         actual_color = Helper.Helper_common.rbga_to_hex(actual_color)
#         expected_color = '#1890ff'
#         try:
#             with allure.step('Check icon arrow down highlight'):
#                 self.assertEqual(actual_color, expected_color, 'Check decrease sort is highlight')
#         except AssertionError:
#             list_step_fail.append('2.2 Check icon arrow down highlight')
#
#         self.assertListEqual(list_step_fail, [])
#
#     def test_PS_Sort_resolved_issues(self):
#         list_step_fail = []
#         driver = self.driver
#         driver.refresh()
#         time.sleep(5)
#         column = 'Resolved Issues'
#         # Sort increase
#         sorted_increase = Helper.Helper_common.sorting_increase_column(driver, column)
#         expected = sorted(sorted_increase, key=str.lower)
#         try:
#             with allure.step('Check result sorted Increase by ' + column):
#                 self.assertListEqual(sorted_increase, expected, 'Check sort increase by ' + column)
#         except AssertionError:
#             list_step_fail.append('1.1 Check sort increase by ' + column)
#
#         actual_color = driver.find_element_by_xpath(
#             '//div[@class="ant-table-scroll"]//tr/th//span[text()="' + column
#             + '"]/..//div/i[@class="anticon anticon-caret-up ant-table-column-sorter-up on"]')
#         actual_color = actual_color.value_of_css_property('color')
#         actual_color = Helper.Helper_common.rbga_to_hex(actual_color)
#         expected_color = '#1890ff'
#         try:
#             with allure.step('Check icon arrow up highlight'):
#                 self.assertEqual(actual_color, expected_color, 'Check increase sort is highlight')
#         except AssertionError:
#             list_step_fail.append('1.2 Check icon arrow up highlight')
#
#         # Click back to first page
#         list_page = driver.find_elements_by_css_selector('.ant-pagination>li')
#         if len(list_page):
#             driver.find_element_by_css_selector('.ant-pagination>li.ant-pagination-item-1').click()
#
#         driver.refresh()
#
#         # Sort Decrease
#         sorted_decrease = Helper.Helper_common.sorting_decrease_column(driver, column)
#         expected = sorted(sorted_decrease, key=str.lower, reverse=True)
#         try:
#             with allure.step('Check result sorted Decrease by ' + column):
#                 self.assertListEqual(sorted_decrease, expected, 'Check sort decrease by ' + column)
#         except AssertionError:
#             list_step_fail.append('2.1 Check sort decrease by ' + column)
#
#         actual_color = driver.find_element_by_xpath(
#             '//div[@class="ant-table-scroll"]//tr/th//span[text()="' + column
#             + '"]/..//div/i[@class="anticon anticon-caret-down ant-table-column-sorter-down on"]')
#         actual_color = actual_color.value_of_css_property('color')
#         actual_color = Helper.Helper_common.rbga_to_hex(actual_color)
#         expected_color = '#1890ff'
#         try:
#             with allure.step('Check icon arrow down highlight'):
#                 self.assertEqual(actual_color, expected_color, 'Check decrease sort is highlight')
#         except AssertionError:
#             list_step_fail.append('2.2 Check icon arrow down highlight')
#
#         self.assertListEqual(list_step_fail, [])
#
#     def test_PS_Sort_expense(self):
#         list_step_fail = []
#         driver = self.driver
#         driver.refresh()
#         time.sleep(5)
#         column = 'Expense'
#         # Sort increase
#         sorted_increase = Helper.Helper_common.sorting_increase_column(driver, column)
#         expected = sorted(sorted_increase, key=str.lower)
#         try:
#             with allure.step('Check result sorted Increase by ' + column):
#                 self.assertListEqual(sorted_increase, expected, 'Check sort increase by ' + column)
#         except AssertionError:
#             list_step_fail.append('1.1 Check sort increase by ' + column)
#
#         actual_color = driver.find_element_by_xpath(
#             '//div[@class="ant-table-scroll"]//tr/th//span[text()="' + column
#             + '"]/..//div/i[@class="anticon anticon-caret-up ant-table-column-sorter-up on"]')
#         actual_color = actual_color.value_of_css_property('color')
#         actual_color = Helper.Helper_common.rbga_to_hex(actual_color)
#         expected_color = '#1890ff'
#         try:
#             with allure.step('Check icon arrow up highlight'):
#                 self.assertEqual(actual_color, expected_color, 'Check increase sort is highlight')
#         except AssertionError:
#             list_step_fail.append('1.2 Check icon arrow up highlight')
#
#         # Click back to first page
#         list_page = driver.find_elements_by_css_selector('.ant-pagination>li')
#         if len(list_page):
#             driver.find_element_by_css_selector('.ant-pagination>li.ant-pagination-item-1').click()
#
#         driver.refresh()
#
#         # Sort Decrease
#         sorted_decrease = Helper.Helper_common.sorting_decrease_column(driver, column)
#         expected = sorted(sorted_decrease, key=str.lower, reverse=True)
#         try:
#             with allure.step('Check result sorted Decrease by ' + column):
#                 self.assertListEqual(sorted_decrease, expected, 'Check sort decrease by ' + column)
#         except AssertionError:
#             list_step_fail.append('2.1 Check sort decrease by ' + column)
#
#         actual_color = driver.find_element_by_xpath(
#             '//div[@class="ant-table-scroll"]//tr/th//span[text()="' + column
#             + '"]/..//div/i[@class="anticon anticon-caret-down ant-table-column-sorter-down on"]')
#         actual_color = actual_color.value_of_css_property('color')
#         actual_color = Helper.Helper_common.rbga_to_hex(actual_color)
#         expected_color = '#1890ff'
#         try:
#             with allure.step('Check icon arrow down highlight'):
#                 self.assertEqual(actual_color, expected_color, 'Check decrease sort is highlight')
#         except AssertionError:
#             list_step_fail.append('2.2 Check icon arrow down highlight')
#
#         self.assertListEqual(list_step_fail, [])
#
#     def test_PS_Sort_warranty(self):
#         list_step_fail = []
#         driver = self.driver
#         driver.refresh()
#         time.sleep(5)
#         column = 'Warranty'
#         # Sort increase
#         sorted_increase = Helper.Helper_common.sorting_increase_column(driver, column)
#         actual = []
#         for i in sorted_increase:
#             actual.append(float(i.split('months')[0]))
#         expected = sorted(actual, key=str.lower)
#
#         try:
#             with allure.step('Check result sorted Increase by ' + column):
#                 self.assertListEqual(actual, expected, 'Check sort increase by ' + column)
#         except AssertionError:
#             list_step_fail.append('1.1 Check sort increase by ' + column)
#
#         actual_color = driver.find_element_by_xpath(
#             '//div[@class="ant-table-scroll"]//tr/th//span[text()="' + column
#             + '"]/..//div/i[@class="anticon anticon-caret-up ant-table-column-sorter-up on"]')
#         actual_color = actual_color.value_of_css_property('color')
#         actual_color = Helper.Helper_common.rbga_to_hex(actual_color)
#         expected_color = '#1890ff'
#         try:
#             with allure.step('Check icon arrow up highlight'):
#                 self.assertEqual(actual_color, expected_color, 'Check increase sort is highlight')
#         except AssertionError:
#             list_step_fail.append('1.2 Check icon arrow up highlight')
#
#         # Click back to first page
#         list_page = driver.find_elements_by_css_selector('.ant-pagination>li')
#         if len(list_page):
#             driver.find_element_by_css_selector('.ant-pagination>li.ant-pagination-item-1').click()
#
#         driver.refresh()
#
#         # Sort Decrease
#         sorted_decrease = Helper.Helper_common.sorting_decrease_column(driver, column)
#         actual = []
#         for i in sorted_decrease:
#             actual.append(float(i.split('months')[0]))
#         expected = sorted(actual, key=str.lower, reverse=True)
#         try:
#             with allure.step('Check result sorted Decrease by ' + column):
#                 self.assertListEqual(actual, expected, 'Check sort decrease by ' + column)
#         except AssertionError:
#             list_step_fail.append('2.1 Check sort decrease by ' + column)
#
#         actual_color = driver.find_element_by_xpath(
#             '//div[@class="ant-table-scroll"]//tr/th//span[text()="' + column
#             + '"]/..//div/i[@class="anticon anticon-caret-down ant-table-column-sorter-down on"]')
#         actual_color = actual_color.value_of_css_property('color')
#         actual_color = Helper.Helper_common.rbga_to_hex(actual_color)
#         expected_color = '#1890ff'
#         try:
#             with allure.step('Check icon arrow down highlight'):
#                 self.assertEqual(actual_color, expected_color, 'Check decrease sort is highlight')
#         except AssertionError:
#             list_step_fail.append('2.2 Check icon arrow down highlight')
#
#         self.assertListEqual(list_step_fail, [])
#
#     def test_PS_Sort_start_date(self):
#         list_step_fail = []
#         driver = self.driver
#         driver.refresh()
#         time.sleep(5)
#         column = 'Start date'
#         # Sort increase
#         sorted_increase = Helper.Helper_common.sorting_increase_column(driver, column)
#
#         # when blank data -> convert date time to 01/01/0001
#         str_2_date = []
#         for i in sorted_increase:
#             if i == '':
#                 str_2_date.append(datetime.strptime('01/01/0001', '%d/%m/%Y'))
#             else:
#                 str_2_date.append(datetime.strptime(i, '%d/%m/%Y'))
#         str_2_date = sorted(str_2_date)
#
#         # convert back to string, if date time = 01/01/0001 -> append blank
#         date_2_str = []
#         for i in str_2_date:
#             if i.strftime('%d/%m/%Y') == '01/01/0001':
#                 date_2_str.append('')
#             else:
#                 date_2_str.append(i.strftime('%d/%m/%Y'))
#
#         try:
#             with allure.step('Check result sorted Increase by ' + column):
#                 self.assertListEqual(sorted_increase, date_2_str, 'Check sort increase by ' + column)
#         except AssertionError:
#             list_step_fail.append('1.1 Check sort increase by ' + column)
#
#         actual_color = driver.find_element_by_xpath(
#             '//div[@class="ant-table-scroll"]//tr/th//span[text()="' + column
#             + '"]/..//div/i[@class="anticon anticon-caret-up ant-table-column-sorter-up on"]')
#         actual_color = actual_color.value_of_css_property('color')
#         actual_color = Helper.Helper_common.rbga_to_hex(actual_color)
#         expected_color = '#1890ff'
#         try:
#             with allure.step('Check icon arrow up highlight'):
#                 self.assertEqual(actual_color, expected_color, 'Check increase sort is highlight')
#         except AssertionError:
#             list_step_fail.append('1.2 Check icon arrow up highlight')
#
#         # Click back to first page
#         list_page = driver.find_elements_by_css_selector('.ant-pagination>li')
#         if len(list_page):
#             driver.find_element_by_css_selector('.ant-pagination>li.ant-pagination-item-1').click()
#
#         driver.refresh()
#
#         # Sort Decrease
#         sorted_decrease = Helper.Helper_common.sorting_decrease_column(driver, column)
#
#         # when blank data -> convert date time to 01/01/0001
#         str_2_date = []
#         for i in sorted_decrease:
#             if i == '':
#                 str_2_date.append(datetime.strptime('01/01/0001', '%d/%m/%Y'))
#             else:
#                 str_2_date.append(datetime.strptime(i, '%d/%m/%Y'))
#         str_2_date = sorted(str_2_date, reverse=True)
#
#         # convert back to string, if date time = 01/01/0001 -> append blank
#         date_2_str = []
#         for i in str_2_date:
#             if i.strftime('%d/%m/%Y') == '01/01/0001':
#                 date_2_str.append('')
#             else:
#                 date_2_str.append(i.strftime('%d/%m/%Y'))
#         try:
#             with allure.step('Check result sorted Decrease by ' + column):
#                 self.assertListEqual(date_2_str, sorted_decrease, 'Check sort decrease by ' + column)
#         except AssertionError:
#             list_step_fail.append('2.1 Check sort decrease by ' + column)
#
#         actual_color = driver.find_element_by_xpath(
#             '//div[@class="ant-table-scroll"]//tr/th//span[text()="' + column
#             + '"]/..//div/i[@class="anticon anticon-caret-down ant-table-column-sorter-down on"]')
#         actual_color = actual_color.value_of_css_property('color')
#         actual_color = Helper.Helper_common.rbga_to_hex(actual_color)
#         expected_color = '#1890ff'
#         try:
#             with allure.step('Check icon arrow down highlight'):
#                 self.assertEqual(actual_color, expected_color, 'Check decrease sort is highlight')
#         except AssertionError:
#             list_step_fail.append('2.2 Check icon arrow down highlight')
#
#         self.assertListEqual(list_step_fail, [])
#
#     def test_PS_Sort_end_date(self):
#         list_step_fail = []
#         driver = self.driver
#         driver.refresh()
#         time.sleep(5)
#         column = 'End date'
#         # Sort increase
#         sorted_increase = Helper.Helper_common.sorting_increase_column(driver, column)
#
#         # when blank data -> convert date time to 01/01/0001
#         str_2_date = []
#         for i in sorted_increase:
#             if i == '':
#                 str_2_date.append(datetime.strptime('01/01/0001', '%d/%m/%Y'))
#             else:
#                 str_2_date.append(datetime.strptime(i, '%d/%m/%Y'))
#         str_2_date = sorted(str_2_date)
#
#         # convert back to string, if date time = 01/01/0001 -> append blank
#         date_2_str = []
#         for i in str_2_date:
#             if i.strftime('%d/%m/%Y') == '01/01/0001':
#                 date_2_str.append('')
#             else:
#                 date_2_str.append(i.strftime('%d/%m/%Y'))
#
#         try:
#             with allure.step('Check result sorted Increase by ' + column):
#                 self.assertListEqual(sorted_increase, date_2_str, 'Check sort increase by ' + column)
#         except AssertionError:
#             list_step_fail.append('1.1 Check sort increase by ' + column)
#
#         actual_color = driver.find_element_by_xpath(
#             '//div[@class="ant-table-scroll"]//tr/th//span[text()="' + column
#             + '"]/..//div/i[@class="anticon anticon-caret-up ant-table-column-sorter-up on"]')
#         actual_color = actual_color.value_of_css_property('color')
#         actual_color = Helper.Helper_common.rbga_to_hex(actual_color)
#         expected_color = '#1890ff'
#         try:
#             with allure.step('Check icon arrow up highlight'):
#                 self.assertEqual(actual_color, expected_color, 'Check increase sort is highlight')
#         except AssertionError:
#             list_step_fail.append('1.2 Check icon arrow up highlight')
#
#         # Click back to first page
#         list_page = driver.find_elements_by_css_selector('.ant-pagination>li')
#         if len(list_page):
#             driver.find_element_by_css_selector('.ant-pagination>li.ant-pagination-item-1').click()
#
#         driver.refresh()
#
#         # Sort Decrease
#         sorted_decrease = Helper.Helper_common.sorting_decrease_column(driver, column)
#
#         # when blank data -> convert date time to 01/01/0001
#         str_2_date = []
#         for i in sorted_decrease:
#             if i == '':
#                 str_2_date.append(datetime.strptime('01/01/0001', '%d/%m/%Y'))
#             else:
#                 str_2_date.append(datetime.strptime(i, '%d/%m/%Y'))
#         str_2_date = sorted(str_2_date, reverse=True)
#
#         # convert back to string, if date time = 01/01/0001 -> append blank
#         date_2_str = []
#         for i in str_2_date:
#             if i.strftime('%d/%m/%Y') == '01/01/0001':
#                 date_2_str.append('')
#             else:
#                 date_2_str.append(i.strftime('%d/%m/%Y'))
#         try:
#             with allure.step('Check result sorted Decrease by ' + column):
#                 self.assertListEqual(date_2_str, sorted_decrease, 'Check sort decrease by ' + column)
#         except AssertionError:
#             list_step_fail.append('2.1 Check sort decrease by ' + column)
#
#         actual_color = driver.find_element_by_xpath(
#             '//div[@class="ant-table-scroll"]//tr/th//span[text()="' + column
#             + '"]/..//div/i[@class="anticon anticon-caret-down ant-table-column-sorter-down on"]')
#         actual_color = actual_color.value_of_css_property('color')
#         actual_color = Helper.Helper_common.rbga_to_hex(actual_color)
#         expected_color = '#1890ff'
#         try:
#             with allure.step('Check icon arrow down highlight'):
#                 self.assertEqual(actual_color, expected_color, 'Check decrease sort is highlight')
#         except AssertionError:
#             list_step_fail.append('2.2 Check icon arrow down highlight')
#
#         self.assertListEqual(list_step_fail, [])


class RecentlySearch(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.driver = webdriver.Chrome('chromedriver.exe')
        driver = self.driver
        driver.maximize_window()
        # Log in
        login(driver)
        time.sleep(5)
        driver.get(url_root + '/project-visibility/project-status')
        time.sleep(7)

    @classmethod
    def tearDownClass(self):
        self.driver.quit()

    @allure.story('Check Recently Search')
    def test_PS_recently_search(self):
        list_step_fail = []
        driver = self.driver
        search_key = 'Rose 5'
        search_box = driver.find_element_by_css_selector('input.ant-input')
        ActionChains(driver).move_to_element(search_box).click().send_keys(search_key).perform()
        # Click search
        driver.find_element_by_css_selector('button.search-btn').click()

        list_recently_search = driver.find_elements_by_css_selector('.suggestion')
        actual = []
        for i in list_recently_search:
            a = i.text.replace(',', '')
            actual.append(a.strip())

        try:
            with allure.step('Value just searched is display highlight at Recently search'):
                self.assertIn(search_key, actual, 'Check Recently Search')
        except AssertionError:
            list_step_fail.append('2.2 Check key search in Recently Search area')

        for i in list_recently_search:
            if i.text.strip(',').strip() == search_key:
                ActionChains(driver).move_to_element(i).click().perform()
        search_box_value = driver.find_element_by_css_selector('input.ant-input').get_attribute('value')

        try:
            with allure.step('Choosen project is display automatically on text box Search'):
                self.assertEqual(search_box_value, search_key, 'Check choosen recently search to display in search box')
        except AssertionError:
            list_step_fail.append('3.1 Check choosen recently search to display in search box')

        result_search = Helper.Helper_common.get_all_table_info(driver)
        for row in result_search:
            try:
                with allure.step('The result display matched with condition Search'):
                    self.assertIn(search_key, row[1], 'Check key search in result')
            except AssertionError:
                list_step_fail.append('3.2 Check The result display matched with condition Search')

        self.assertListEqual(list_step_fail, [])

    @allure.story('Check Cancel')
    def test_PS_RecentlySearch_Cancel(self):
        driver = self.driver
        list_fail = []

        try:
            with allure.step('Verify the result list is different compare to original list'):
                projects_original = driver.find_elements_by_css_selector(
                    'div.ant-table-fixed-left> * tbody.ant-table-tbody>tr>td:first-child>div')
                lst_original = []
                for i in projects_original:
                    lst_original.append(i.text)

                driver.find_element_by_css_selector('div.smart-search> * input.ant-input').send_keys('Rose 5')
                time.sleep(0.5)

                ActionChains(driver).move_to_element(
                    driver.find_element_by_css_selector('div.smart-search>button.search-btn')).click().perform()
                time.sleep(1)

                projects_result = driver.find_elements_by_css_selector(
                    'div.ant-table-fixed-left> * tbody.ant-table-tbody>tr>td:first-child>div')
                lst_result = []
                for i in projects_result:
                    lst_result.append(i.text)

                self.assertNotEqual(lst_original, lst_result)
                time.sleep(0.5)
        except:
            list_fail.append('The Search feature does not work correctly')

        try:
            with allure.step('Click on X icon to remove text'):
                ActionChains(driver).move_to_element(
                    driver.find_element_by_css_selector('div.smart-search> * i.anticon-close-circle')).click().perform()
                time.sleep(1)
        except:
            list_fail.append('The X icon is unclickable')

        try:
            with allure.step('Verify the textbox content'):
                self.assertEqual(driver.find_element_by_css_selector('div.smart-search> * input.ant-input').text, '')
                time.sleep(0.5)
        except:
            list_fail.append('The textbox is not blank')

        driver.refresh()
        time.sleep(5)


class DownloadExcel(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.driver = webdriver.Chrome('chromedriver.exe')
        driver = self.driver
        driver.maximize_window()
        # Log in
        login(driver)
        time.sleep(5)
        driver.get(url_root + '/project-visibility/project-status')
        time.sleep(7)

    @classmethod
    def tearDownClass(self):
        self.driver.quit()

    @allure.story('Check Download Excel feature')
    def test_PS_DownloadExcel_download_excel(self):
        list_step_fail = []
        driver = self.driver
        # Click Download setting
        driver.find_element_by_css_selector('.download-excel .icon-settings').click()

        pop_up = driver.find_elements_by_css_selector('.download-settings')

        try:
            with allure.step('Check Download settings pop-up '):
                self.assertEqual(len(pop_up), 1, 'Check Pop up Download Setting appear')
        except AssertionError:
            list_step_fail.append('1.1 Check Download settings pop-up ')

        # Check Dialog title
        dialog_title = driver.find_elements_by_css_selector('.dialog-title')
        try:
            with allure.step('Check Download settings Title'):
                self.assertEqual(len(dialog_title), 1, 'Check Dialog Title of Pop up Download Setting appear')
        except AssertionError:
            list_step_fail.append('1.2 Check Download settings Title')

        # Check All check box is checked
        total_label = driver.find_elements_by_css_selector('.dialog-body label')
        check_box = driver.find_elements_by_css_selector('.dialog-body label input')
        num_checked = 0
        for i in check_box:
            if i.get_property('checked') is True:
                num_checked += 1

        try:
            with allure.step('Check all fields are checked'):
                self.assertEqual(len(total_label), num_checked, 'Check Total Label were checked')
        except AssertionError:
            list_step_fail.append('1.3 Check all fields are checked')

        expected = ['Project ID', 'Project Type', 'Project Name', 'Project Reference', 'Customer Company',
                    'Customer Department', 'Customer Position', 'Customer Name', 'Member role', 'Member Name',
                    'Member MM', 'Member Budget', 'Resource feature', 'Resource work', 'Resource Name',
                    'Resource Team', 'Resource Part', 'Resource Position', 'Resource Job grade', 'Resource MM',
                    'ResourceBudget', 'Status', 'Warning Expense', 'Warning icon', 'Progress Rate',
                    'Warranty Description', 'Warranty Period ', 'Start date', 'End date', 'Statement Description',
                    'Statement Requirement', 'Statement Deliverable', 'Statement Acceptance Criteria']
        actual = []
        labels = driver.find_elements_by_css_selector('.download-settings label')
        for i in labels:
            actual.append(i.text)
        try:
            with allure.step('Check all text of fields are same as Requirement'):
                self.assertListEqual(actual, expected, 'Check Total Label were checked')
        except AssertionError:
            list_step_fail.append('1.4 Check all text of fields are same as Requirement')

        num_checkbox = driver.find_elements_by_css_selector('.download-settings label>.ant-checkbox')
        try:
            with allure.step('Check quantity of check box is same as Requirement'):
                self.assertEqual(len(num_checkbox), len(expected), 'Check Total Label were checked')
        except AssertionError:
            list_step_fail.append('1.5 Check quantity of check box is same as Requirement: Actual ' + str(actual))

        # Uncheck Value
        uncheck1 = driver.find_element_by_xpath(
            '//div[@class="dialog-body column"]//label//span[text()="Customer Company"]//..//input')
        uncheck1.click()
        uncheck1_property = uncheck1.get_property('checked')
        uncheck2 = driver.find_element_by_xpath(
            '//div[@class="dialog-body column"]//label//span[text()="Resource Name"]//..//input')
        uncheck2.click()
        uncheck2_property = uncheck2.get_property('checked')
        actual = [uncheck1_property, uncheck2_property]
        expected = [False, False]

        try:
            with allure.step('Check Uncheck of Customer Company and Resource Name'):
                self.assertListEqual(actual, expected, 'Checked Uncheck label')
        except AssertionError:
            list_step_fail.append('2. Check Uncheck of Customer Company and Resource Name')

        # Close pop-up
        driver.find_element_by_css_selector('.icon-ico_cancel').click()
        pop_up = driver.find_elements_by_css_selector('.download-settings')

        try:
            with allure.step('Check Pop up Download Setting disappear when click exit'):
                self.assertEqual(len(pop_up), 0, 'Check Pop up Download Setting disappear')
        except AssertionError:
            list_step_fail.append('3. Check Pop up Download Setting disappear when click exit')


        with OpenKey(HKEY_CURRENT_USER, 'SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders') as key:
            Downloads = QueryValueEx(key, '{374DE290-123F-4565-9164-39C4925E467B}')[0]
        # Check download success
        before_download = len(os.listdir(Downloads))
        # Download
        driver.find_element_by_css_selector('.download-excel>.first').click()
        time.sleep(5)
        after_download = len(os.listdir(Downloads))
        try:
            with allure.step('Check download successfully'):
                self.assertEqual(before_download + 1, after_download)
        except AssertionError:
            list_step_fail.append('4. Download excel file Fail')

        list_of_files = glob.glob(Downloads + '\\*.xlsx')
        latest_file = max(list_of_files, key=os.path.getctime)
        time.sleep(3)
        column_value = []
        wb = openpyxl.load_workbook(latest_file)
        ws = wb.active
        for i in range(1, ws.max_column+1):
            column_value.append(ws.cell(1, i).value)
        time.sleep(3)
        try:
            with allure.step('Information of all fields choose in Download settings will be displayed in the Excel. '
                             'Customer Company and Resource Name field is not display'):
                self.assertNotIn('Customer Company', column_value)
                self.assertNotIn('Resource Name', column_value)
        except AssertionError:
            list_step_fail.append(
                '5. Information of all fields choose in Download settings will be displayed in the Excel. '
                                  'Customer Company and Resource Name field is not display')

        self.assertListEqual(list_step_fail, [], list_step_fail)

    # @allure.story('Check Download Excel default')
    # def test_PS_download_excel_default(self):
    #     driver = self.driver
    #     driver.refresh()
    #     time.sleep(7)
    #     # Download
    #     driver.find_element_by_css_selector('.download-excel>.first').click()
    #     time.sleep(3)
    #     with OpenKey(HKEY_CURRENT_USER, 'SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders') as key:
    #         Downloads = QueryValueEx(key, '{374DE290-123F-4565-9164-39C4925E467B}')[0]
    #     list_of_files = glob.glob(Downloads + '\\*.xlsx')
    #     latest_file = max(list_of_files, key=os.path.getctime)
    #     time.sleep(3)
    #     values_excel = Helper.Helper_common.get_download_excel_info(latest_file)
    #
    #     api_project = Helper.Helper_common.api_project_info()
    #     api_full_employees = Helper.Helper_common.api_full_employees()
    #     api_field_config = Helper.Helper_common.api_field_config()
    #     for i in range(len(api_project)):
    #         api_detail = Helper.Helper_common.api_project_detail(api_project[i]['id'])['project']
    #
    #         expected = [i, api_detail['code'], api_detail['projectTypeID'], api_detail['name'], api_project[i]['hqName'],
    #                     api_detail['customer'][0]['customerName'], api_detail['customer'][0]['departmentName'],
    #                     api_detail['customer'][0]['companyName'], api_detail['customer'][0]['positionName']
    #                     ]
    #
    #         for role in api_detail['managers']:
    #             for empl in api_full_employees:
    #                 if empl['userID'] == role['employeeID']:
    #                     employee_name = empl['name']
    #             for filed in api_field_config:
    #                 if filed['fieldName'] == 'ProjectRoleID':
    #                     for value in filed['filedDataValues']:
    #                         if value['id'] == role['roleID']:
    #                             role_name = value['value']
    #             member_info = [role_name, employee_name, role['mm'], role['budget']]


# **********************************************************************************************************************

class Search(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.driver = webdriver.Chrome('chromedriver.exe')
        driver = self.driver
        driver.maximize_window()
        # Log in
        login(driver)
        time.sleep(5)
        driver.get(url_root + '/project-visibility/project-status')
        time.sleep(7)

    @classmethod
    def tearDownClass(self):
        self.driver.quit()

    def work_around(a):

        # workaround for sort function by shortening the result list
        a.find_element_by_css_selector('.icon-filter_list').click()
        time.sleep(1.5)
        a.find_element_by_css_selector('div.right>div.list>div.list-item:last-child>label>span.ant-checkbox') \
            .click()
        time.sleep(1.5)
        a.find_element_by_css_selector('a.active').click()
        time.sleep(1.5)

    @allure.story('Check Search filter reset')
    def test_PS_Search_filter_reset(self):
        list_step_fail = []
        driver = self.driver
        driver.refresh()
        time.sleep(3)
        # Filter
        driver.find_element_by_css_selector('.icon-filter_list').click()
        time.sleep(1)
        #Status
        driver.find_element_by_xpath('//div[@class="left"]//div[text()="Status"]').click()
        time.sleep(1)
        # Pending
        driver.find_element_by_xpath('//div[@class="right"]//span[text()="Pending"]/../label').click()
        time.sleep(1)
        # Input
        search_key = 'VN_00072019'
        search_box = driver.find_element_by_css_selector('input.ant-input')
        ActionChains(driver).move_to_element(search_box).click().send_keys(search_key).perform()
        # Click search
        driver.find_element_by_css_selector('button.search-btn').click()

        # Verify condition
        result_return = Helper.Helper_common.get_all_table_info(driver)
        total_result = int(driver.find_element_by_xpath('//span[@class="result-text "]').text.split(': ')[1])
        if total_result:
            for row in result_return:
                try:
                    with allure.step(
                            'The result display list project matched with condition Search project and filter Pending'):
                        self.assertIn(search_key, row[0], 'Check value of Filter and Search')
                        self.assertEqual(row[8], 'PENDING', 'Check value of Filter and Search')
                except AssertionError:
                    list_step_fail.append('7.2 The result display list project matched with '
                                          'condition Search project and filter Pending')

        else:
            try:
                with allure.step(' if no result display No data'):
                    self.assertEqual(result_return, 'No data', 'Check value of Filter and Search')
            except AssertionError:
                list_step_fail.append('7.3  if no result display No data')

        # Filter
        driver.find_element_by_css_selector('.icon-filter_list').click()
        time.sleep(2)
        # Click reset
        driver.find_element_by_xpath('//span[text()="Reset"]/..').click()
        check_box = driver.find_element_by_xpath('//span[text()="Pending"]/..//input').get_property('checked')
        try:
            with allure.step('Pending check box of Status filter is uncheck'):
                self.assertFalse(check_box, 'Check Pending is unchecked')
        except AssertionError:
            list_step_fail.append('7.1 Pending check box of Status filter is uncheck')

        self.assertListEqual(list_step_fail, [])

    @allure.story('Check Search Have result Smart Search Name')
    def test_PS_Search_Haveresult_SmartSearch_Name(self):
        self.driver.refresh()
        time.sleep(5)
        list_fail = []

        # Search.work_around(self.driver)

        # Input search textbox
        search_textbox = self.driver.find_element_by_css_selector("input.ant-input")
        search_textbox.send_keys('#name:sanji')
        time.sleep(1)

        try:
            with allure.step('Step 1: Input string with the same value inputted'):
                self.assertEqual(search_textbox.get_property('value'), '#name:sanji')
        except AssertionError:
            list_fail.append("Step 1: Can input string with the same value inputted")

        # Click search btn
        btn_search = self.driver.find_element_by_css_selector(
            "button.ant-btn.search-btn.ant-btn-primary.ant-btn-lg").click()
        time.sleep(3)

        try:
            with allure.step('Step 2: Check result web'):
                data_web = Helper.Helper_common.read_column(self.driver, 'Name')
                for i in range(0, len(data_web)):
                    self.assertTrue('SANJI' in data_web[i].upper())
        except AssertionError:
            list_fail.append("Step 2: Result web wrong")

        # Get table data
        table_data = Helper.Helper_common.get_all_table_info(self.driver)

        # Call API
        api_get_all = Helper.Helper_common.api_get_all()
        api_get_field_config = Helper.Helper_common.api_get_field_config()

        lst_name = []
        for i in range(0, len(api_get_all)):
            lst_name.append(api_get_all[i]['name'])
        name = [u for u in lst_name if 'SANJI' in u.upper()]
        lst_index = []
        for i in range(0, len(name)):
            lst_index.append(lst_name.index(name[i]))

        # Check project ID
        project_id_web = []
        project_id_api = []
        for i in lst_index:
            project_id_api.append(api_get_all[i]['code'])
        for i in range(0, len(table_data)):
            project_id_web.append(table_data[i][0])
        try:
            with allure.step('3.1 Check result api - ProjectID'):
                self.assertListEqual(project_id_web, project_id_api)
        except AssertionError:
            list_fail.append('3.1 Check result api - ProjectID'
                             + 'Expected: ' + str(project_id_api) + 'Actual: ' + str(project_id_web))

        # Check name
        name_api = []
        name_web = []
        for i in lst_index:
            name_api.append(api_get_all[i]['name'])
        for i in range(0, len(table_data)):
            name_web.append(table_data[i][1].split("\n")[0])
        try:
            with allure.step('3.2 Check result api - Name'):
                self.assertListEqual(name_api, name_web)
        except AssertionError:
            list_fail.append('3.2 Check result api - Name'
                             + 'Expected: ' + str(name_api) + 'Actual: ' + str(name_web))

        # Check customer
        customer_api = []
        customer_web = []
        for i in lst_index:
            customer_api.append(api_get_all[i]['customer']['companyName'])
        for i in range(0, len(table_data)):
            customer_web.append(table_data[i][2])
        try:
            with allure.step('3.3 Check result api - Customer'):
                self.assertListEqual(customer_api, customer_web)
        except AssertionError:
            list_fail.append('3.3 Check result api - Customer'
                             + 'Expected: ' + str(customer_api) + 'Actual: ' + str(customer_web))

        # Check Department
        department_api = []
        department_web = []
        for i in lst_index:
            department_api.append(api_get_all[i]['customer']['departmentName'])
        department_api = ['' if i == None else i for i in department_api]
        for i in range(0, len(table_data)):
            department_web.append(table_data[i][3])
        try:
            with allure.step('3.4 Check result api - Department'):
                self.assertListEqual(department_api, department_web)
        except AssertionError:
            list_fail.append('3.4 Check result api - Department'
                             + 'Expected: ' + str(department_api) + 'Actual: ' + str(department_web))

        # Check PO
        po_api = []
        po_web = []
        for i in lst_index:
            if len(api_get_all[i]['roles']) == 0:
                po_api.append("")
            elif api_get_all[i]['roles'][0]['roleID'] == '622ad862-13db-9090-9ad3-1991e2227777':
                j = Helper.Helper_common.querydb(
                    "Select EnglishName from EM_Employee where EmployeeID = '" + api_get_all[i]['roles'][0][
                        'employeeID'] + "'")
                po_api.append(str(j).split("'")[1])
            else:
                j = Helper.Helper_common.querydb(
                    "Select EnglishName from EM_Employee where EmployeeID = '" + api_get_all[i]['roles'][1][
                        'employeeID'] + "'")
                po_api.append(str(j).split("'")[1])

        for i in range(0, len(table_data)):
            po_web.append(table_data[i][4])
        try:
            with allure.step('3.5 Check result api - PO'):
                self.assertListEqual(po_api, po_web)
        except AssertionError:
            list_fail.append('3.5 Check result api - PO'
                             + 'Expected: ' + str(po_api) + 'Actual: ' + str(po_web))

        # Check TotalMM
        totalmm_api = []
        totalmm_web = []
        for i in lst_index:
            totalmm_api.append(str(int(api_get_all[i]['totalMonth'])))
        totalmm_api = ['' if i == '0' else i for i in totalmm_api]
        for i in range(0, len(table_data)):
            totalmm_web.append(table_data[i][5])
        try:
            with allure.step('3.6 Check result api - TotalMM'):
                self.assertListEqual(totalmm_api, totalmm_web)
        except AssertionError:
            list_fail.append('3.6 Check result api - TotalMM'
                             + 'Expected: ' + str(totalmm_api) + 'Actual: ' + str(totalmm_web))

        # Check RealMM
        realmm_api = []
        realmm_web = []
        for i in lst_index:
            realmm_api.append(str(int(api_get_all[i]['realMonth'])))
        realmm_api = ['' if i == '0' else i for i in realmm_api]
        for i in range(0, len(table_data)):
            realmm_web.append(table_data[i][6])
        try:
            with allure.step('3.7 Check result api - RealMM'):
                self.assertListEqual(realmm_api, realmm_web)
        except AssertionError:
            list_fail.append('3.7 Check result api - RealMM'
                             + 'Expected: ' + str(realmm_api) + 'Actual: ' + str(realmm_web))

        # Check Total Budget
        total_budget_api = []
        total_budget_web = []
        for i in lst_index:
            total_budget_api.append(str(int(api_get_all[i]['totalBudget'])))
        total_budget_api = ['' if i == '0' else i for i in total_budget_api]
        for i in range(0, len(table_data)):
            total_budget_web.append(table_data[i][7])
        try:
            with allure.step('3.8 Check result api - Total Budget'):
                self.assertListEqual(total_budget_api, total_budget_api)
        except AssertionError:
            list_fail.append('3.8 Check result api - Total Budget'
                             + 'Expected: ' + str(total_budget_api) + 'Actual: ' + str(total_budget_web))

        # Check status
        status_api = []
        status_web = []
        for j in lst_index:
            for i in range(0, len(api_get_field_config[89]['fieldDataValues'])):
                if api_get_field_config[89]['fieldDataValues'][i]['valueCode'] == str(api_get_all[j]['status']):
                    status = (api_get_field_config[89]['fieldDataValues'][i]['value'])
                    status_api.append(status.upper())

        for i in range(1, len(table_data) + 1):
            j = self.driver.find_element_by_css_selector(
                "table>tbody>tr:nth-child(" + str(i) + ")>td:nth-child(9)>div").text
            status_web.append(str(j))
        try:
            with allure.step('3.9 Check result api - Status'):
                self.assertListEqual(status_api, status_web)
        except AssertionError:
            list_fail.append('3.9 Check result api - Status'
                             + 'Expected: ' + str(status_api) + 'Actual: ' + str(status_web))

        # Check Resolved Issues
        resolved_api = []
        resolved_web = []
        for i in lst_index:
            resolved_api.append(str(int(api_get_all[i]['resolveIssues'])))
        resolved_api = ['' if i == '0' else i for i in resolved_api]
        for i in range(0, len(table_data)):
            resolved_web.append(table_data[i][9])
        try:
            with allure.step('3.10 Check result api - Resolved Issues'):
                self.assertListEqual(resolved_api, resolved_web)
        except AssertionError:
            list_fail.append('3.10 Check result api - Resolved Issues'
                             + 'Expected: ' + str(resolved_api) + 'Actual: ' + str(resolved_web))

        # Check Expense
        expense_api = []
        expense_web = []
        for i in lst_index:
            expense_api.append(str(int(api_get_all[i]['expense'])))
        expense_api = ['' if i == '0' else i for i in expense_api]
        for i in range(0, len(table_data)):
            expense_web.append(table_data[i][10])
        try:
            with allure.step('3.11 Check result api - Expense'):
                self.assertListEqual(expense_api, expense_web)
        except AssertionError:
            list_fail.append('3.11 Check result api - Expense'
                             + 'Expected: ' + str(expense_api) + 'Actual: ' + str(expense_web))

        # Check warranty
        warranty_api = []
        warranty_web = []
        for i in lst_index:
            warranty_api.append(str(int(api_get_all[i]['warranty'])))
        # expense_api = ['' if i == '0' else i for i in expense_api]
        for i in range(0, len(table_data)):
            warranty_web.append(table_data[i][11].split(' ')[0])
        try:
            with allure.step('3.12 Check result api - Warranty'):
                self.assertListEqual(warranty_api, warranty_web)
        except AssertionError:
            list_fail.append('3.12 Check result api - Warranty'
                             + 'Expected: ' + str(warranty_api) + 'Actual: ' + str(warranty_web))

        # Check start date
        start_date_api = []
        start_date_web = []
        for i in lst_index:
            j = datetime.strptime(api_get_all[i]['startDate'].split('T')[0], '%Y-%m-%d')
            j = datetime.strftime(j, '%d/%m/%Y')
            start_date_api.append(j)
        # # expense_api = ['' if i == '0' else i for i in expense_api]
        for i in range(0, len(table_data)):
            start_date_web.append(table_data[i][12])
        try:
            with allure.step('3.13 Check result api - Start date'):
                self.assertListEqual(start_date_web, start_date_api)
        except AssertionError:
            list_fail.append('3.13 Check result api - Start date'
                             + 'Expected: ' + str(start_date_api) + 'Actual: ' + str(start_date_web))

        # Check end date
        end_date_api = []
        end_date_web = []
        for i in lst_index:
            j = datetime.strptime(api_get_all[i]['endDate'].split('T')[0], '%Y-%m-%d')
            j = datetime.strftime(j, '%d/%m/%Y')
            end_date_api.append(j)
        # # expense_api = ['' if i == '0' else i for i in expense_api]
        for i in range(0, len(table_data)):
            end_date_web.append(table_data[i][13])
        try:
            with allure.step('3.14 Check result api - End date'):
                self.assertListEqual(end_date_api, end_date_web)
        except AssertionError:
            list_fail.append("3.14 Check result api - End date"
                             + 'Expected: ' + str(end_date_api) + 'Actual: ' + str(end_date_web))

        self.driver.refresh()
        time.sleep(5)

        self.assertEqual(len(list_fail), 0, list_fail)

    @allure.story('Check Search Have result Advance Search Team')
    def test_PS_Search_Haveresult_AdvanceSearch_Team(self):
        self.driver.refresh()
        time.sleep(5)
        list_fail = []

        Search.work_around(self.driver)

        # Input search textbox
        search_textbox = self.driver.find_element_by_css_selector("input.ant-input")
        # search_textbox.send_keys('NOT #status:Save Draft')
        search_textbox.send_keys('#team:Solution AND #customer:Customer')
        time.sleep(1)

        try:
            with allure.step('Step 1: Input string with the same value inputted'):
                self.assertEqual(search_textbox.get_property('value'), '#team:Solution NOT #team:Network')
                # self.assertEqual(search_textbox.get_property('value'), '#team:Network NOT #team:Solution')
        except AssertionError:
            list_fail.append("Step 1: Can input string with the same value inputted")

        # Click search btn
        btn_search = self.driver.find_element_by_css_selector(
            "button.ant-btn.search-btn.ant-btn-primary.ant-btn-lg").click()
        time.sleep(3)

        # Get table data
        table_data = Helper.Helper_common.get_all_table_info(self.driver)

        # Call API
        api_get_all = Helper.Helper_common.api_get_all()
        api_get_field_config = Helper.Helper_common.api_get_field_config()

        # lst_name = []
        # for i in range(0, len(api_get_all)):
        #     lst_name.append(api_get_all[i]['name'])
        # name = [u for u in lst_name if 'SANJI' in u.upper()]
        lst_project_id = []
        lst_index = []
        lst_code_api = []
        for i in range(0, len(table_data)):
            lst_project_id.append(table_data[i][0])
        for i in range(0, len(api_get_all)):
            lst_code_api.append(api_get_all[i]['code'])
        for i in range(0, len(lst_project_id)):
            lst_index.append(lst_code_api.index(lst_project_id[i]))

        time.sleep(0.2)

        # Check project ID
        project_id_web = []
        project_id_api = []
        for i in lst_index:
            project_id_api.append(api_get_all[i]['code'])
        for i in range(0, len(table_data)):
            project_id_web.append(table_data[i][0])
        try:
            with allure.step('3.1 Check result api - ProjectID'):
                self.assertListEqual(project_id_web, project_id_api)
        except AssertionError:
            list_fail.append('3.1 Check result api - ProjectID'
                             + '\nExpected: ' + str(project_id_api) + '\nActual: ' + str(project_id_web))

        # Check name
        name_api = []
        name_web = []
        for i in lst_index:
            name_api.append(api_get_all[i]['name'])
        for i in range(0, len(table_data)):
            name_web.append(table_data[i][1].split("\n")[0])
        try:
            with allure.step('3.2 Check result api - Name'):
                self.assertListEqual(name_api, name_web)
        except AssertionError:
            list_fail.append('3.2 Check result api - Name'
                             + '\nExpected: ' + str(name_api) + '\nActual: ' + str(name_web))

        # Check customer
        customer_api = []
        customer_web = []
        for i in lst_index:
            if str(api_get_all[i]['customer']) == 'None':
                customer_api.append('')
            else:
                customer_api.append(api_get_all[i]['customer']['companyName'])
        for i in range(0, len(table_data)):
            customer_web.append(table_data[i][2])
        try:
            with allure.step('3.3 Check result api - Customer'):
                self.assertListEqual(customer_api, customer_web)
        except AssertionError:
            list_fail.append('3.3 Check result api - Customer'
                             + '\nExpected: ' + str(customer_api) + '\nActual: ' + str(customer_web))

        # Check Department
        department_api = []
        department_web = []
        for i in lst_index:
            department_api.append(api_get_all[i]['customer']['departmentName'])
        department_api = ['' if i == None else i for i in department_api]
        for i in range(0, len(table_data)):
            department_web.append(table_data[i][3])
        try:
            with allure.step('3.4 Check result api - Department'):
                self.assertListEqual(department_api, department_web)
        except AssertionError:
            list_fail.append('3.4 Check result api - Department'
                             + '\nExpected: ' + str(department_api) + '\nActual: ' + str(department_web))

        # Check PO
        po_api = []
        po_web = []
        for i in lst_index:
            if len(api_get_all[i]['roles']) == 0:
                po_api.append("")
            elif api_get_all[i]['roles'][0]['roleID'] == '622ad862-13db-9090-9ad3-1991e2227777':
                j = Helper.Helper_common.querydb(
                    "Select EnglishName from EM_Employee where EmployeeID = '" + api_get_all[i]['roles'][0][
                        'employeeID'] + "'")
                po_api.append(str(j).split("'")[1])
            else:
                j = Helper.Helper_common.querydb(
                    "Select EnglishName from EM_Employee where EmployeeID = '" + api_get_all[i]['roles'][1][
                        'employeeID'] + "'")
                po_api.append(str(j).split("'")[1])

        for i in range(0, len(table_data)):
            po_web.append(table_data[i][4])
        try:
            with allure.step('3.5 Check result api - PO'):
                self.assertListEqual(po_api, po_web)
        except AssertionError:
            list_fail.append('3.5 Check result api - PO'
                             + '\nExpected: ' + str(po_api) + '\nActual: ' + str(po_web))

        # Check TotalMM
        totalmm_api = []
        totalmm_web = []
        for i in lst_index:
            totalmm_api.append(str(int(api_get_all[i]['totalMonth'])))
        totalmm_api = ['' if i == '0' else i for i in totalmm_api]
        for i in range(0, len(table_data)):
            totalmm_web.append(table_data[i][5])
        try:
            with allure.step('3.6 Check result api - TotalMM'):
                self.assertListEqual(totalmm_api, totalmm_web)
        except AssertionError:
            list_fail.append('3.6 Check result api - TotalMM'
                             + '\nExpected: ' + str(totalmm_api) + '\nActual: ' + str(totalmm_web))

        # Check RealMM
        realmm_api = []
        realmm_web = []
        for i in lst_index:
            realmm_api.append(str(int(api_get_all[i]['realMonth'])))
        realmm_api = ['' if i == '0' else i for i in realmm_api]
        for i in range(0, len(table_data)):
            realmm_web.append(table_data[i][6])
        try:
            with allure.step('3.7 Check result api - RealMM'):
                self.assertListEqual(realmm_api, realmm_web)
        except AssertionError:
            list_fail.append('3.7 Check result api - RealMM'
                             + '\nExpected: ' + str(realmm_api) + '\nActual: ' + str(realmm_web))

        # Check Total Budget
        total_budget_api = []
        total_budget_web = []
        for i in lst_index:
            total_budget_api.append(str(int(api_get_all[i]['totalBudget'])))
        total_budget_api = ['' if i == '0' else i for i in total_budget_api]
        for i in range(0, len(table_data)):
            total_budget_web.append(table_data[i][7])
        try:
            with allure.step('3.8 Check result api - Total Budget'):
                self.assertListEqual(total_budget_api, total_budget_api)
        except AssertionError:
            list_fail.append('3.8 Check result api - Total Budget'
                             + '\nExpected: ' + str(total_budget_api) + '\nActual: ' + str(total_budget_web))

        self.driver.refresh()
        time.sleep(10)

        # Check status
        status_api = []
        status_web = []
        for j in lst_index:
            for i in range(0, len(api_get_field_config[89]['fieldDataValues'])):
                if api_get_field_config[89]['fieldDataValues'][i]['valueCode'] == str(api_get_all[j]['status']):
                    status = (api_get_field_config[89]['fieldDataValues'][i]['value'])
                    status_api.append(status.upper())
        for i in range(1, len(table_data) + 1):
            j = self.driver.find_element_by_css_selector(
                "table>tbody>tr:nth-child(" + str(i) + ")>td:nth-child(9)>div").text

            status_web.append(str(j))
        try:
            with allure.step('3.9 Check result api - Status'):
                self.assertListEqual(status_api, status_web)
        except AssertionError:
            list_fail.append('3.9 Check result api - Status'
                             + 'Expected: ' + str(status_api) + 'Actual: ' + str(status_web))

        # Check Resolved Issues
        resolved_api = []
        resolved_web = []
        for i in lst_index:
            resolved_api.append(str(int(api_get_all[i]['resolveIssues'])))
        resolved_api = ['' if i == '0' else i for i in resolved_api]
        for i in range(0, len(table_data)):
            resolved_web.append(table_data[i][9])
        try:
            with allure.step('3.10 Check result api - Resolved Issues'):
                self.assertListEqual(resolved_api, resolved_web)
        except AssertionError:
            list_fail.append('3.10 Check result api - Resolved Issues'
                             + '\nExpected: ' + str(resolved_api) + '\nActual: ' + str(resolved_web))

        # Check Expense
        expense_api = []
        expense_web = []
        for i in lst_index:
            expense_api.append(str(int(api_get_all[i]['expense'])))
        expense_api = ['' if i == '0' else i for i in expense_api]
        for i in range(0, len(table_data)):
            expense_web.append(table_data[i][10])
        try:
            with allure.step('3.11 Check result api - Expense'):
                self.assertListEqual(expense_api, expense_web)
        except AssertionError:
            list_fail.append('3.11 Check result api - Expense'
                             + '\nExpected: ' + str(expense_api) + '\nActual: ' + str(expense_web))

        # Check warranty
        warranty_api = []
        warranty_web = []
        for i in lst_index:
            warranty_api.append(str(int(api_get_all[i]['warranty'])))
        # expense_api = ['' if i == '0' else i for i in expense_api]
        for i in range(0, len(table_data)):
            warranty_web.append(table_data[i][11].split(' ')[0])
        try:
            with allure.step('3.12 Check result api - Warranty'):
                self.assertListEqual(warranty_api, warranty_web)
        except AssertionError:
            list_fail.append('3.12 Check result api - Warranty'
                             + '\nExpected: ' + str(warranty_api) + '\nActual: ' + str(warranty_web))

        # Check start date
        start_date_api = []
        start_date_web = []
        for i in lst_index:
            j = datetime.strptime(api_get_all[i]['startDate'].split('T')[0], '%Y-%m-%d')
            j = datetime.strftime(j, '%d/%m/%Y')
            start_date_api.append(j)
        # # expense_api = ['' if i == '0' else i for i in expense_api]
        for i in range(0, len(table_data)):
            start_date_web.append(table_data[i][12])
        try:
            with allure.step('3.13 Check result api - Start date'):
                self.assertListEqual(start_date_web, start_date_api)
        except AssertionError:
            list_fail.append('3.13 Check result api - Start date'
                             + '\nExpected: ' + str(start_date_api) + '\nActual: ' + str(start_date_web))

        # Check end date
        end_date_api = []
        end_date_web = []
        for i in lst_index:
            j = datetime.strptime(api_get_all[i]['endDate'].split('T')[0], '%Y-%m-%d')
            j = datetime.strftime(j, '%d/%m/%Y')
            end_date_api.append(j)
        # # expense_api = ['' if i == '0' else i for i in expense_api]
        for i in range(0, len(table_data)):
            end_date_web.append(table_data[i][13])
        try:
            with allure.step('3.14 Check result api - End date'):
                self.assertListEqual(end_date_api, end_date_web)
        except AssertionError:
            list_fail.append("3.14 Check result api - End date"
                             + '\nExpected: ' + str(end_date_api) + '\nActual: ' + str(end_date_web))

        self.assertEqual(len(list_fail), 0, list_fail)


class Value(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        self.driver = webdriver.Chrome('chromedriver.exe')
        driver = self.driver
        driver.maximize_window()

        # Log in
        Helper.Helper_common.login(driver)
        time.sleep(5)

        # Moving to test page
        driver.get(url_root + '/project-visibility/project-status')
        time.sleep(10)

    @classmethod
    def tearDownClass(self):
        self.driver.quit()

    @allure.story('Check default value')
    def test_PS_Value_Default(self):
        list_fail = []
        driver = self.driver

        # Click and open first project
        # first_project = driver.find_element_by_css_selector(".ant-table-fixed-left tr:first-child>td:nth-child(2)").click()

        # Switch window popup then close
        # driver.switch_to_window(driver.window_handles[1])
        # time.sleep(1)
        api_get_all = Helper.Helper_common.api_get_all()
        first_project_id = api_get_all[0]['id']
        first_project_detail = Helper.Helper_common.api_project_detail(first_project_id)
        with allure.step("1. Check data of first project"):
            try:
                with allure.step("1.1 Check Project ID"):
                    # Check Project ID
                    project_id_web = driver.find_element_by_css_selector(
                        "div.ant-table-fixed-left tr:first-child>td:first-child").text
                    project_id_api = api_get_all[0]['code']
                    self.assertEqual(project_id_web, project_id_api)
            except AssertionError:
                list_fail.append("1.1 Project ID wrong")

            try:
                with allure.step("1.2 Check name"):
                    # Check name
                    name_api = api_get_all[0]['name']
                    name_web = driver.find_element_by_css_selector(
                        "div.ant-table-fixed-left tr:first-child>td:nth-child(2)").text
                    self.assertEqual(name_web.split('\n')[0], name_api)
            except AssertionError:
                list_fail.append("1.2 Name wrong")

            try:
                with allure.step("1.3 Check customer"):
                    # Check Customer
                    customer = driver.find_element_by_css_selector(
                        "div.ant-table-scroll table.ant-table-fixed tbody.ant-table-tbody>tr:first-child>td:nth-child(3)>div")
                    ActionChains(driver).move_to_element(customer).perform()
                    customer_web = customer.text
                    if str(api_get_all[0]['customer']) == 'None':
                        customer_api = ''
                    else:
                        customer_api = api_get_all[0]['customer']['companyName']
                    self.assertEqual(customer_api, customer_web)
            except AssertionError:
                list_fail.append("1.3 Customer wrong")

            try:
                with allure.step("1.4 Check department"):
                    # Check Department
                    department_api = api_get_all[0]['customer']['departmentName']
                    department = driver.find_element_by_css_selector(
                        "div.ant-table-scroll table.ant-table-fixed tbody.ant-table-tbody>tr:first-child>td:nth-child(4)>div")
                    ActionChains(driver).move_to_element(department).perform()
                    department_web = department.text
                    self.assertEqual(department_web, department_api)
            except AssertionError:
                list_fail.append("1.4 Department wrong")

            try:
                with allure.step("1.5 Check PO"):
                    # Check PO
                    if len(api_get_all[0]['roles']) == 0:
                        po_api = ""
                    elif api_get_all[0]['roles'][0]['roleID'] == '622ad862-13db-9090-9ad3-1991e2227777':
                        j = Helper.Helper_common.querydb(
                            "Select EnglishName from EM_Employee where EmployeeID = '" + api_get_all[0]['roles'][0][
                                'employeeID'] + "'")
                        po_api = str(j).split("'")[1]
                    else:
                        j = Helper.Helper_common.querydb(
                            "Select EnglishName from EM_Employee where EmployeeID = '" + api_get_all[0]['roles'][1][
                                'employeeID'] + "'")
                        po_api = str(j).split("'")[1]

                    po = driver.find_element_by_css_selector(
                        "div.ant-table-scroll table.ant-table-fixed tbody.ant-table-tbody>tr:first-child>td:nth-child(5)>div")
                    ActionChains(driver).move_to_element(po).perform()
                    po_web = po.text
                    self.assertEqual(po_web, po_api)
            except AssertionError:
                list_fail.append("1.5 PO wrong")

            try:
                with allure.step("1.6 Check TotalMM"):
                    # Check TotalMM
                    totalmm_api = str(int(api_get_all[0]['totalMonth']))
                    totalmm = driver.find_element_by_css_selector(
                        "div.ant-table-scroll table.ant-table-fixed tbody.ant-table-tbody>tr:first-child>td:nth-child(6)>div")
                    ActionChains(driver).move_to_element(totalmm).perform()
                    totalmm_web = totalmm.text
                    self.assertEqual(totalmm_web, totalmm_api)
            except AssertionError:
                list_fail.append("1.6 TotalMM wrong")

            try:
                with allure.step("1.7 Check RealMM"):
                    # Check RealMM
                    realmm_api = str(int(api_get_all[0]['realMonth']))
                    realmm = driver.find_element_by_css_selector(
                        "div.ant-table-scroll table.ant-table-fixed tbody.ant-table-tbody>tr:first-child>td:nth-child(7)>div")
                    ActionChains(driver).move_to_element(realmm).perform()
                    realmm_web = realmm.text
                    if realmm_web == '':
                        realmm_web = '0'
                    self.assertEqual(realmm_web, realmm_api)
            except AssertionError:
                list_fail.append("1.7 RealMM wrong")
            try:
                with allure.step("1.8 Check Total Budget"):
                    # Check Total Budget
                    total_budget_api = str(int(api_get_all[0]['totalBudget']))
                    total_budget_ = driver.find_element_by_css_selector(
                        "div.ant-table-scroll table.ant-table-fixed tbody.ant-table-tbody>tr:first-child>td:nth-child(8)>div")
                    ActionChains(driver).move_to_element(realmm).perform()
                    realmm_web = realmm.text
                    if realmm_web == '':
                        realmm_web = '0'
                    self.assertEqual(realmm_web, realmm_api)
            except AssertionError:
                list_fail.append("1.8 Total Budget wrong")

            try:
                with allure.step("1.9 Check Status"):
                    # Check Status
                    status_api = str(int(api_get_all[0]['status']))
                    status = driver.find_element_by_css_selector(
                        "div.ant-table-scroll table.ant-table-fixed tbody.ant-table-tbody>tr:first-child>td:nth-child(9)>div")
                    ActionChains(driver).move_to_element(status).perform()
                    status = status.text

                    j = Helper.Helper_common.querydb(
                        "Select ValueCode from Common_FieldDataValue where FieldConfigID = '84BE3D19-7538-403E-B2BA-77668DF6B7CE' AND Value = '" +
                        status + "'")

                    status_web = str(j).split("'")[1]
                    self.assertEqual(status_web, status_api)
            except AssertionError:
                list_fail.append("1.9 Status wrong")
            try:
                with allure.step("1.10 Check Resolved Issuses"):
                    # Check Resolved Issuses
                    resolved_api = str(int(api_get_all[0]['resolveIssues']))
                    resolved = driver.find_element_by_css_selector(
                        "div.ant-table-scroll table.ant-table-fixed tbody.ant-table-tbody>tr:first-child>td:nth-child(10)>div")
                    ActionChains(driver).move_to_element(resolved).perform()
                    resolved_web = resolved.text
                    if resolved_web == '':
                        resolved_web = '0'
                    self.assertEqual(resolved_web, resolved_api)
            except AssertionError:
                list_fail.append("1.10 Resolved Issuses wrong")
            try:
                with allure.step("1.11 Check Expense"):
                    # Check Expense
                    expense_api = str(int(api_get_all[0]['expense']))
                    expense = driver.find_element_by_css_selector(
                        "div.ant-table-scroll table.ant-table-fixed tbody.ant-table-tbody>tr:first-child>td:nth-child(11)>div")
                    ActionChains(driver).move_to_element(expense).perform()
                    expense_web = expense.text
                    if expense_web == '':
                        expense_web = '0'
                    self.assertEqual(expense_web, expense_api)
            except AssertionError:
                list_fail.append("1.11 Expense wrong")

            try:
                with allure.step("1.12 Check warranty"):
                    # Check Warranty
                    warranty_api = str(int(api_get_all[0]['warranty']))
                    warranty = driver.find_element_by_css_selector(
                        "div.ant-table-scroll table.ant-table-fixed tbody.ant-table-tbody>tr:first-child>td:nth-child(12)")
                    ActionChains(driver).move_to_element(warranty).perform()
                    warranty_web = warranty.text
                    if warranty_web == '':
                        warranty_web = '0'
                    self.assertEqual(warranty_web.split(" ")[0], warranty_api)
            except AssertionError:
                list_fail.append("1.12 Warranty wrong")

            try:
                with allure.step("1.13 Check start date"):
                    # Check Start date
                    j = datetime.strptime(api_get_all[0]['startDate'].split('T')[0], '%Y-%m-%d')
                    start_date_api = datetime.strftime(j, '%d/%m/%Y')

                    start_date = driver.find_element_by_css_selector(
                        "div.ant-table-scroll table.ant-table-fixed tbody.ant-table-tbody>tr:first-child>td:nth-child(13)")
                    ActionChains(driver).move_to_element(start_date).perform()
                    start_date_web = start_date.text
                    self.assertEqual(start_date_web, start_date_api)
            except AssertionError:
                list_fail.append("1.13 Start date wrong")

            try:
                with allure.step("1.14 Check End date"):
                    # Check And date
                    j = datetime.strptime(api_get_all[0]['endDate'].split('T')[0], '%Y-%m-%d')
                    end_date_api = datetime.strftime(j, '%d/%m/%Y')

                    end_date = driver.find_element_by_css_selector(
                        "div.ant-table-scroll table.ant-table-fixed tbody.ant-table-tbody>tr:first-child>td:nth-child(14)")
                    ActionChains(driver).move_to_element(end_date).perform()
                    end_date_web = end_date.text
                    self.assertEqual(end_date_web, end_date_api)
            except AssertionError:
                list_fail.append("1.14 End date wrong")

        try:
            with allure.step("1.1 Check color of status value"):
                # Get status
                status = driver.find_element_by_css_selector(
                    "div.ant-table-scroll table.ant-table-fixed tbody.ant-table-tbody>tr:first-child>td:nth-child(9)>div")
                ActionChains(driver).move_to_element(status).perform()
                status_text_color = status.value_of_css_property('color')
                status_background_color = status.value_of_css_property('background-color')

                if status.text == 'WAITING FOR APRROVAL':
                    self.assertEqual(Helper.Helper_common.rbga_to_hex(status_text_color).upper(), '#724396')
                    self.assertEqual(Helper.Helper_common.rbga_to_hex(status_background_color).upper(), '#E4C5FF')

                elif status.text == 'PREPARING':
                    self.assertEqual(Helper.Helper_common.rbga_to_hex(status_text_color).upper(), '#725E10')
                    self.assertEqual(Helper.Helper_common.rbga_to_hex(status_background_color).upper(), '#37f0d6')

                elif status.text == 'IN-PROGRESS':
                    self.assertEqual(Helper.Helper_common.rbga_to_hex(status_text_color).upper(), '#2F6514')
                    self.assertEqual(Helper.Helper_common.rbga_to_hex(status_background_color).upper(), '#FFD4D3')

                elif status.text == 'PENDING':
                    self.assertEqual(Helper.Helper_common.rbga_to_hex(status_text_color).upper(), '#AD1625')
                    self.assertEqual(Helper.Helper_common.rbga_to_hex(status_background_color).upper(), '#FFD4D3')

                elif status.text == 'CLOSED':
                    self.assertEqual(Helper.Helper_common.rbga_to_hex(status_text_color).upper(), '#555555')
                    self.assertEqual(Helper.Helper_common.rbga_to_hex(status_background_color).upper(), '#DFE3E6')

                elif status.text == 'APPROVED':
                    self.assertEqual(Helper.Helper_common.rbga_to_hex(status_text_color).upper(), '#006CB2')
                    self.assertEqual(Helper.Helper_common.rbga_to_hex(status_background_color).upper(), '#C1E6FE')

                elif status.text == 'REJECTED':
                    self.assertEqual(Helper.Helper_common.rbga_to_hex(status_text_color).upper(), '#AD169F')
                    self.assertEqual(Helper.Helper_common.rbga_to_hex(status_background_color).upper(), '#FFD3E8')

                elif status.text == 'WARRANTY':
                    self.assertEqual(Helper.Helper_common.rbga_to_hex(status_text_color).upper(), '#142865')
                    self.assertEqual(Helper.Helper_common.rbga_to_hex(status_background_color).upper(), '#BDCDFF')

                # elif status.text == 'SAVE DRAFT':
                #     self.assertEqual(Helper.Helper_common.rbga_to_hex(status_text_color).upper(), '#142865')
                #     self.assertEqual(Helper.Helper_common.rbga_to_hex(status_background_color).upper(), '#BDCDFF')

                else:
                    self.assertEqual(1, 2)

        except AssertionError:
            list_fail.append("1.1 Color of status value wrong")

        try:
            with allure.step("1.2 Check total budget display value"):
                # Check total budget
                total_budget = driver.find_element_by_css_selector(
                    "div.ant-table-scroll table.ant-table-fixed tbody.ant-table-tbody>tr:first-child>td:nth-child(8)>div")
                ActionChains(driver).move_to_element(total_budget).perform()
                total_budget = total_budget.text
                self.assertNotEqual(total_budget, '')
        except AssertionError:
            list_fail.append('1.2 Check total budget value')

        try:
            with allure.step("1.2 Check expense display value"):
                # Check expense
                expense = driver.find_element_by_css_selector(
                    "div.ant-table-scroll table.ant-table-fixed tbody.ant-table-tbody>tr:first-child>td:nth-child(11)>div")
                ActionChains(driver).move_to_element(expense).perform()
                expense = expense.text
                self.assertNotEqual(expense, '')
        except AssertionError:
            list_fail.append('1.2 Check expense value')

        try:
            with allure.step("1.3 Check color of customer value"):
                # Get customer
                customer = driver.find_element_by_css_selector(
                    "div.ant-table-scroll table.ant-table-fixed tbody.ant-table-tbody>tr:first-child>td:nth-child(3)>div")
                ActionChains(driver).move_to_element(customer).perform()
                customer = customer.value_of_css_property('color')
                customer_color = Helper.Helper_common.rbga_to_hex(customer)
                self.assertEqual(customer_color.upper(), '#427FEC')
        except AssertionError:
            list_fail.append("1.3 Color of customer value wrong")

        try:
            with allure.step("1.3 Check color of department value"):
                # Get department
                department = driver.find_element_by_css_selector(
                    "div.ant-table-scroll table.ant-table-fixed tbody.ant-table-tbody>tr:first-child>td:nth-child(4)>div")
                ActionChains(driver).move_to_element(department).perform()
                department = department.value_of_css_property('color')
                department_color = Helper.Helper_common.rbga_to_hex(department)
                self.assertEqual(department_color.upper(), '#427FEC')
        except AssertionError:
            list_fail.append("1.3 Color of department value wrong")

        try:
            with allure.step("1.3 Check color of PO value"):
                # Get department
                po = driver.find_element_by_css_selector(
                    "div.ant-table-scroll table.ant-table-fixed tbody.ant-table-tbody>tr:first-child>td:nth-child(5)>div")
                ActionChains(driver).move_to_element(po).perform()
                po = po.value_of_css_property('color')
                po_color = Helper.Helper_common.rbga_to_hex(po)
                self.assertEqual(po_color.upper(), '#427FEC')
        except AssertionError:
            list_fail.append("1.3 Color of PO value wrong")

        try:
            with allure.step("1.4 Check resolved issue"):
                # Check resolved issue
                resolved_issue = driver.find_element_by_css_selector(
                    "div.ant-table-scroll table.ant-table-fixed tbody.ant-table-tbody>tr:first-child>td:nth-child(10)")
                ActionChains(driver).move_to_element(resolved_issue).perform()
                resolved_issue = resolved_issue.text
                self.assertEqual('%', resolved_issue[-1:])
        except AssertionError:
            list_fail.append("1.4 Check resolved issue")

        try:
            with allure.step("1.5 Check format Warranty"):
                # Check format Warranty
                warranty = driver.find_element_by_css_selector(
                    "div.ant-table-scroll table.ant-table-fixed tbody.ant-table-tbody>tr:first-child>td:nth-child(12)")
                ActionChains(driver).move_to_element(warranty).perform()
                warranty = warranty.text
                self.assertEqual(warranty.split(' ')[1], 'months')
        except AssertionError:
            list_fail.append("1.5 Check format Warranty")

        try:
            with allure.step("1.6 Format text of Start date"):
                # Check start date
                start_date = driver.find_element_by_css_selector(
                    "div.ant-table-scroll table.ant-table-fixed tbody.ant-table-tbody>tr:first-child>td:nth-child(13)")
                ActionChains(driver).move_to_element(start_date).perform()
                start_date = start_date.text
                self.assertEqual(start_date[2], "/")
                self.assertEqual(start_date[5], "/")
        except AssertionError:
            list_fail.append("1.6 Format text of Start date")

        try:
            with allure.step("1.6 Format text of End date"):
                # Check end date
                end_date = driver.find_element_by_css_selector(
                    "div.ant-table-scroll table.ant-table-fixed tbody.ant-table-tbody>tr:first-child>td:nth-child(14)")
                ActionChains(driver).move_to_element(end_date).perform()
                end_date = end_date.text
                self.assertEqual(end_date[2], "/")
                self.assertEqual(end_date[5], "/")
        except AssertionError:
            list_fail.append("1.7 Format text of End date")

        try:
            with allure.step("2. Move to the middle page"):
                list_pages = driver.find_elements_by_xpath('//ul//li//a')
                middle_page = int(len(list_pages) / 2)
                list_pages[middle_page].click()
                time.sleep(2)
                check_selected_page = driver.find_element_by_css_selector(
                    "ul.ant-pagination.ant-table-pagination li:nth-child(" + str(middle_page + 1) + ")")
                ActionChains(driver).move_to_element(check_selected_page).perform()
                check_selected_page = check_selected_page.value_of_css_property('background-color')
                self.assertNotEqual(check_selected_page, 'rgba(255, 255, 255, 1)')

        except AssertionError:
            list_fail.append("2. Move to the middle page")

        try:
            with allure.step("3. Move to the last page"):
                list_pages = driver.find_elements_by_xpath('//ul//li//a')
                last_page = len(list_pages) - 1
                list_pages[last_page - 1].click()
                time.sleep(2)
                check_selected_page = driver.find_element_by_css_selector(
                    "ul.ant-pagination.ant-table-pagination li:nth-child(" + str(
                        last_page - 1) + ")").value_of_css_property('background-color')
                self.assertNotEqual(check_selected_page, 'rgba(255, 255, 255, 1)')

        except AssertionError:
            list_fail.append("3. Move to the last page")

        self.assertEqual(len(list_fail), 0, list_fail)


class Filter(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.driver = webdriver.Chrome('chromedriver.exe')
        driver = self.driver
        driver.maximize_window()
        # Log in
        login(driver)
        time.sleep(5)
        driver.get(url_root + '/project-visibility/project-status')
        time.sleep(7)

    @classmethod
    def tearDownClass(self):
        self.driver.quit()

    @allure.story('Check Filter mix')
    def test_PS_Filter_mix_all(self):
        list_step_fail = []
        driver = self.driver
        driver.refresh()
        time.sleep(5)
        # Filter
        driver.find_element_by_css_selector('.icon-filter_list').click()
        time.sleep(1)

        # Common
        driver.find_element_by_xpath('//div[@class="right"]//span[text()="Common"]/../label').click()
        time.sleep(1)

        # Status
        driver.find_element_by_xpath('//div[@class="left"]//div[text()="Status"]').click()
        time.sleep(1)
        # Wwarranty
        driver.find_element_by_xpath('//div[@class="right"]//span[text()="Warranty"]/../label').click()
        time.sleep(1)

        # Custormer's company
        driver.find_element_by_xpath('//div[@class="left"]//div[text()="Customer\'s company"]').click()
        time.sleep(1)
        # First option
        driver.find_element_by_xpath('//div[@class="right"]//div[1]/label[1]').click()
        company_option = driver.find_element_by_xpath('//div[@class="right"]//div[1]/div[1]').text
        time.sleep(1)

        # Custormer's Department
        driver.find_element_by_xpath('//div[@class="left"]//div[text()="Customer\'s department"]').click()
        time.sleep(1)
        # First option
        driver.find_element_by_xpath('//div[@class="right"]//div[1]/label[1]').click()
        department_option = driver.find_element_by_xpath('//div[@class="right"]//div[1]/div[1]').text
        time.sleep(1)

        # Verify condition
        result_return = Helper.Helper_common.get_all_table_info(driver)
        total_result = int(driver.find_element_by_xpath('//span[@class="result-text "]').text.split(': ')[1])
        expected = ['Common', 'Warranty'.upper(), company_option, department_option]

        if total_result:
            for row in result_return:
                actual = [row[1].splitlines()[1], row[8], row[2], row[3]]
                try:
                    with allure.step(
                            'The result display list project matched with condition filter Status and '
                            'Custormer\'s company, Customer\'s department'):
                        self.assertListEqual(expected, actual, 'Check value of Mix Filter')
                except AssertionError:
                    list_step_fail.append(
                        '9.1 The result display list project matched with condition filter Status and '
                        'Custormer\'s company, Customer\'s department')
        else:
            try:
                with allure.step('if no result display No data'):
                    self.assertEqual(result_return, 'No data', 'Check value of Mix Filter')
            except AssertionError:
                list_step_fail.append('9.2 if no result display No data')

        driver.refresh()
        time.sleep(5)

        self.assertEqual(len(list_step_fail), 0, list_step_fail)

    @allure.story('Check Project type')
    def test_PS_Filter_project_type(self):
        list_step_fail = []
        driver = self.driver
        driver.refresh()

        time.sleep(5)
        # Filter
        driver.find_element_by_css_selector('.icon-filter_list').click()
        time.sleep(1)
        # Verify Label
        list_filter = driver.find_elements_by_css_selector('.left>div')
        list_filter_name = []
        [[list_filter_name.append(i.text)] for i in list_filter]
        expected_name = ['Project Type', "Customer's company", "Customer's department", 'Status']
        try:
            with allure.step('Check labels of filter'):
                self.assertListEqual(list_filter_name, expected_name)
        except AssertionError:
            list_step_fail.append('1. Check labels of filter')
        # Verify button
        list_btn = driver.find_elements_by_css_selector('.right .blue')
        list_btn_name = []
        [[list_btn_name.append(i.text)] for i in list_btn]
        expected = ['Reset']

        try:
            with allure.step('Check buttons of filter'):
                self.assertListEqual(list_btn_name, expected)
        except AssertionError:
            list_step_fail.append('1. Check buttons of filter')
        # Verify background of chosen label
        background = driver.find_element_by_xpath('//div[@class="left"]//div[text()="Project Type"]')\
            .value_of_css_property('background')
        background_color = Helper.Helper_common.rbga_to_hex(background)
        expected = '#e11cff'

        try:
            with allure.step('Check Icon Project type display blue background'):
                self.assertListEqual(background_color, expected)
        except AssertionError:
            list_step_fail.append('2.1 Check Icon Project type display blue background')

        # Verify option of project type
        driver.find_elements_by_css_selector('.right .list-item')
        list_option = []
        [[list_option.append(i.text)] for i in driver.find_elements_by_css_selector('.right .list-item')]
        expected = ['Common', 'Business']
        try:
            with allure.step('Check options of Project type'):
                self.assertListEqual(list_option, expected)
        except AssertionError:
            list_step_fail.append('2.2 Check options of Project type')

        # Click to Common and verify result
        # driver.find_element_by_xpath('//div[@class="right"]/div/div[1]//span[@class="ant-checkbox"]').click()
        driver.find_element_by_css_selector('div.right>div.list>div.list-item:last-child>label>span.ant-checkbox') \
            .click()

        read_name = Helper.Helper_common.read_column(driver, 'Name')
        check = True

        for i in read_name:
            if i.splitlines()[1] != 'Business':
                check = False

        try:
            with allure.step('Check all Project type is True'):
                self.assertTrue(check)
        except AssertionError:
            list_step_fail.append('3. Check all Project type is True')

        self.assertEqual(len(list_step_fail), 0, list_step_fail)

    @allure.story('Check Customer Company')
    def test_PS_Filter_customer_company(self):
        list_step_fail = []
        driver = self.driver
        driver.refresh()
        time.sleep(5)
        # Filter
        driver.find_element_by_css_selector('.icon-filter_list').click()
        time.sleep(1)
        # Status
        driver.find_element_by_xpath('//div[@class="left"]//div[text()="Customer\'s company"]').click()
        time.sleep(1)
        # Verify background of chosen label
        background = driver.find_element_by_xpath(
            '//div[@class="left"]//div[text()="Customer\'s company"]').value_of_css_property('background')
        background_color = Helper.Helper_common.rbga_to_hex(background)
        expected = '#e1ecff'
        try:
            with allure.step('Check Icon Project type display blue background'):
                self.assertEqual(background_color, expected)
        except AssertionError:
            list_step_fail.append('2. Check Icon Project type display blue background')

        # Click to First option and verify result
        driver.find_element_by_xpath('//div[@class="right"]/div//span[contains(text(),"HQ")]/..//span').click()
        read_name = Helper.Helper_common.read_column(driver, 'Customer')
        check = True
        for i in read_name:
            if 'HQ' not in i:
                check = False
        try:
            with allure.step('Check all Customer Company is HQ'):
                self.assertTrue(check)
        except AssertionError:
            list_step_fail.append('3. Check all Customer Company is HQ')

        self.assertListEqual(list_step_fail, [])

    @allure.story('Check Status Customer Company')
    def test_PS_Filter_status_customer_company(self):
        list_step_fail = []
        driver = self.driver
        driver.refresh()
        time.sleep(5)
        # Filter
        driver.find_element_by_css_selector('.icon-filter_list').click()
        time.sleep(1)
        # CC
        driver.find_element_by_xpath('//div[@class="left"]//div[text()="Status"]').click()
        time.sleep(1)
        # Verify background of chosen label
        background = driver.find_element_by_xpath('//div[@class="left"]//div[text()="Status"]').value_of_css_property(
            'background')
        background_color = Helper.Helper_common.rbga_to_hex(background)
        expected = '#e1ecff'
        try:
            with allure.step('Check Icon Status display blue background'):
                self.assertEqual(background_color, expected)
        except AssertionError:
            list_step_fail.append('2. Check Icon Status display blue background')
        driver.find_element_by_xpath('//div[@class="right"]/div//span[text()="Warranty"]/..//span').click()
        time.sleep(1)
        driver.find_element_by_xpath('//div[@class="left"]//div[text()="Customer\'s company"]').click()
        time.sleep(1)
        # Click to First option and verify result
        driver.find_element_by_xpath('//div[@class="right"]/div//span[contains(text(),"HQ")]/..//span').click()

        get_all_table = Helper.Helper_common.get_all_table_info(driver)
        check = True
        for i in get_all_table:
            if 'HQ' not in i[2] or i[8] != 'Warranty'.upper():
                check = False
            if i == 'No data':
                check = True
        try:
            with allure.step('Check Mix Status and Customer Company'):
                self.assertTrue(check)
        except AssertionError:
            list_step_fail.append('3. Check Mix Status and Customer Company')

        self.assertListEqual(list_step_fail, [])

    @allure.story('Check Status Customer Company Department')
    def test_PS_Filter_status_customer_company_department(self):
        list_step_fail = []
        driver = self.driver
        driver.refresh()
        time.sleep(5)
        # Filter
        driver.find_element_by_css_selector('.icon-filter_list').click()
        time.sleep(1)
        # CC
        driver.find_element_by_xpath('//div[@class="left"]//div[text()="Customer\'s department"]').click()
        time.sleep(1)
        # Verify background of chosen label
        background = driver.find_element_by_xpath(
            '//div[@class="left"]//div[text()="Customer\'s department"]').value_of_css_property('background')
        background_color = Helper.Helper_common.rbga_to_hex(background)
        expected = '#e1ecff'
        try:
            with allure.step('Check Icon Customer\'s department display blue background'):
                self.assertEqual(background_color, expected)
        except AssertionError:
            list_step_fail.append('2. Check Icon Customer\'s department display blue background')
        driver.find_element_by_xpath('//div[@class="right"]/div//span[text()="Web Humax"]/..//span').click()
        # Status
        driver.find_element_by_xpath('//div[@class="left"]//div[text()="Status"]').click()
        time.sleep(1)
        driver.find_element_by_xpath('//div[@class="right"]/div//span[text()="Warranty"]/..//span').click()
        time.sleep(1)
        # Customer Company
        driver.find_element_by_xpath('//div[@class="left"]//div[text()="Customer\'s company"]').click()
        time.sleep(1)
        # Click to First option and verify result
        driver.find_element_by_xpath('//div[@class="right"]/div//span[contains(text(),"HQ")]/..//span').click()

        get_all_table = Helper.Helper_common.get_all_table_info(driver)
        check = True
        if get_all_table == 'No data':
            check = True
        else:
            for i in get_all_table:
                if 'HQ' not in i[2] or i[8] != 'Warranty'.upper() or i[3] != 'Web Humax':
                    check = False

        try:
            with allure.step('Check Mix Status and Customer Company and Customer Department'):
                self.assertTrue(check)
        except AssertionError:
            list_step_fail.append('3. Check Mix Status and Customer Company and Customer Department')

        self.assertListEqual(list_step_fail, [])

    @allure.story('Check Filter Pending')
    def test_PS_Filter_Pending(self):
        driver = self.driver
        list_fail = []

        driver.refresh()
        time.sleep(5)

        # click Filter button
        try:
            with allure.step('Click on Filter button'):
                ActionChains(driver).move_to_element(
                    driver.find_element_by_css_selector('span.icon-filter_list')).click().perform()
                time.sleep(0.5)
        except:
            list_fail.append('Unable to click on Filter button')

        # click Status
        try:
            with allure.step('Click on Status'):
                ActionChains(driver).move_to_element(
                    driver.find_element_by_css_selector('div.ant-popover-inner-content> * div.left>div:last-child'))\
                    .click().perform()
                time.sleep(0.5)
        except:
            list_fail.append('Unable to click on Status')

        # click Pending
        try:
            with allure.step('Click on Pending'):
                pending_opt = driver.find_element_by_xpath('//span[text() = "Pending"]/preceding-sibling::label/span')
                ActionChains(driver).move_to_element(pending_opt).click().perform()
                time.sleep(0.5)
        except:
            list_fail.append('Unable to click on Pending')

        results = driver.find_elements_by_css_selector('div.ant-table-scroll> * tbody>tr')
        if len(results) == 0:
            try:
                with allure.step('Verify text [No Data] is displayed after searching with no result'):
                    self.assertTrue(len(driver.find_element_by_xpath('//*[text()="No Data"]')) > 0)
            except:
                list_fail.append('The text [No Data] is not displayed after searching with no result')
        else:
            try:
                with allure.step('Verify status [Pending] after searching returns at least 1 result'):
                    driver.find_element_by_css_selector('div.smart-search> * input.ant-input').send_keys('HVN_00072019')
                    time.sleep(0.5)

                    ActionChains(driver).move_to_element(
                        driver.find_element_by_css_selector('div.smart-search>button.search-btn')).click().perform()
                    time.sleep(1)

                    self.assertEqual(
                        driver.find_element_by_css_selector('div.ant-table-scroll> * tbody>tr>td:nth-child(9)>div').text,
                        'Pending')
            except:
                list_fail.append('The status of result is displayed as [' +
                                 driver.find_element_by_css_selector(
                                     'div.ant-table-scroll> * tbody>tr>td:nth-child(9)>div').text
                                 + '] instead of [Pending]')

        self.assertEqual(len(list_fail), 0, list_fail)

    @allure.story('Check Filter cancel')
    def test_PS_Filter_Cancel(self):
        driver = self.driver
        list_fail = []

        # click Filter button
        try:
            with allure.step('Click on Filter button'):
                ActionChains(driver).move_to_element(
                    driver.find_element_by_css_selector('span.icon-filter_list')).click().perform()
                time.sleep(0.5)
        except:
            list_fail.append('Unable to click on Filter button')

        # click Status
        try:
            with allure.step('Click on Status'):
                ActionChains(driver).move_to_element(
                    driver.find_element_by_css_selector('div.ant-popover-inner-content> * div.left>div:last-child')) \
                    .click().perform()
                time.sleep(0.5)
        except:
            list_fail.append('Unable to click on Status')

        # click Pending
        try:
            with allure.step('Click on Pending'):
                pending_opt = driver.find_element_by_xpath('//span[text() = "Pending"]/preceding-sibling::label/span')
                ActionChains(driver).move_to_element(pending_opt).click().perform()
                time.sleep(0.5)
        except:
            list_fail.append('Unable to click on Pending')

        results = driver.find_elements_by_css_selector('div.ant-table-scroll> * tbody>tr')
        if len(results) == 0:
            try:
                with allure.step('Verify text [No Data] is displayed after searching with no result'):
                    driver.find_element_by_css_selector('div.smart-search> * input.ant-input').send_keys('HVN_00072019')
                    time.sleep(0.5)

                    ActionChains(driver).move_to_element(
                        driver.find_element_by_css_selector('div.smart-search>button.search-btn')).click().perform()
                    time.sleep(1)

                    ActionChains(driver).move_to_element(
                        driver.find_element_by_css_selector(
                            'div.smart-search> * i.anticon-close-circle')).click().perform()
                    time.sleep(1)

                    try:
                        with allure.step('Verify the textbox content'):
                            self.assertEqual(
                                driver.find_element_by_css_selector('div.smart-search> * input.ant-input').text, '')
                            time.sleep(0.5)
                    except:
                        list_fail.append('The textbox is not blank')

                    self.assertTrue(len(driver.find_element_by_xpath('//*[text()="No Data"]')) > 0)
            except:
                list_fail.append('The text [No Data] is not displayed after searching with no result')
        else:
            try:
                with allure.step('Verify status [Pending] after searching returns at least 1 result'):
                    driver.find_element_by_css_selector('div.smart-search> * input.ant-input').send_keys('HVN_00072019')
                    time.sleep(0.5)

                    ActionChains(driver).move_to_element(
                        driver.find_element_by_css_selector('div.smart-search>button.search-btn')).click().perform()
                    time.sleep(1)

                    ActionChains(driver).move_to_element(
                        driver.find_element_by_css_selector(
                            'div.smart-search> * i.anticon-close-circle')).click().perform()
                    time.sleep(1)

                    try:
                        with allure.step('Verify the textbox content'):
                            self.assertEqual(
                                driver.find_element_by_css_selector('div.smart-search> * input.ant-input').text, '')
                            time.sleep(0.5)
                    except:
                        list_fail.append('The textbox is not blank')

                    self.assertEqual(
                        driver.find_element_by_css_selector(
                            'div.ant-table-scroll> * tbody>tr>td:nth-child(9)>div').text,
                        'Pending')
            except:
                list_fail.append('The status of result is displayed as [' +
                                 driver.find_element_by_css_selector(
                                     'div.ant-table-scroll> * tbody>tr>td:nth-child(9)>div').text
                                 + '] instead of [Pending]')

        self.assertEqual(len(list_fail), 0, list_fail)


if __name__ == '__main__':
    # unittest.main()
    HTMLTestRunner.main()