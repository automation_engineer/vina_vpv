import sys
import icu

sys.path.append('../')

import allure

from Helper.Helper_common import *

import unittest
import HTMLTestRunner
from read_configs import *
import Helper.Helper_common
from datetime import datetime


# **********************************************************************************************************************


class Sort(unittest.TestCase):
    collator = icu.Collator.createInstance(icu.Locale('de_DE.UTF-8'))

    def setUp(self):
        self.driver = webdriver.Chrome('chromedriver.exe')
        driver = self.driver
        driver.maximize_window()
        # Log in
        login(driver)
        time.sleep(5)
        driver.get(url_root + '/project-visibility/project-status')
        time.sleep(10)

    def tearDown(self):
        self.driver.close()

        self.driver.quit()

    def work_around(a):

        # workaround for sort function by shortening the result list
        a.find_element_by_css_selector('.icon-filter_list').click()
        time.sleep(1.5)
        a.find_element_by_css_selector('div.right>div.list>div.list-item:last-child>label>span.ant-checkbox') \
            .click()
        time.sleep(1.5)
        a.find_element_by_css_selector('a.active').click()
        time.sleep(1.5)

    @allure.story('Check sort by Name')
    def test_PS_Sort_name(self):
        list_step_fail = []
        driver = self.driver

        driver.refresh()
        time.sleep(5)

        Sort.work_around(driver)

        column = 'Name'
        # Sort increase
        sorted_increase = Helper.Helper_common.sorting_increase_column(driver, column)
        expected = sorted(sorted_increase, key=Sort.collator.getSortKey)
        try:
            with allure.step('Check result sorted Increase by ' + column):
                self.assertListEqual(sorted_increase, expected, 'Check sort increase by ' + column)
        except AssertionError:
            list_step_fail.append('1.1 Check sort increase by ' + column)

        actual_color = driver.find_element_by_xpath(
            '//div[@class="ant-table-fixed-left"]//tr/th//span[text()="' + column
            + '"]/..//div/i[@class="anticon anticon-caret-up ant-table-column-sorter-up on"]')
        actual_color = actual_color.value_of_css_property('color')
        actual_color = Helper.Helper_common.rbga_to_hex(actual_color)
        expected_color = '#1890ff'
        try:
            with allure.step('Check icon arrow up highlight'):
                self.assertEqual(actual_color, expected_color, 'Check increase sort is highlight')
        except AssertionError:
            list_step_fail.append('1.2 Check icon arrow up highlight')

        # Click back to first page
        list_page = driver.find_elements_by_css_selector('.ant-pagination>li')
        if len(list_page):
            driver.find_element_by_css_selector('.ant-pagination>li.ant-pagination-item-1').click()

        driver.refresh()
        time.sleep(5)
        Sort.work_around(driver)

        # Sort Decrease
        sorted_decrease = Helper.Helper_common.sorting_decrease_column(driver, column)
        expected = sorted(sorted_decrease, key=Sort.collator.getSortKey, reverse=True)
        try:
            with allure.step('Check result sorted Decrease by ' + column):
                self.assertListEqual(sorted_decrease, expected, 'Check sort decrease by ' + column)
        except AssertionError:
            list_step_fail.append('2.1 Check sort decrease by ' + column)

        actual_color = driver.find_element_by_xpath(
            '//div[@class="ant-table-fixed-left"]//tr/th//span[text()="' + column
            + '"]/..//div/i[@class="anticon anticon-caret-down ant-table-column-sorter-down on"]')
        actual_color = actual_color.value_of_css_property('color')
        actual_color = Helper.Helper_common.rbga_to_hex(actual_color)
        expected_color = '#1890ff'
        try:
            with allure.step('Check icon arrow down highlight'):
                self.assertEqual(actual_color, expected_color, 'Check decrease sort is highlight')
        except AssertionError:
            list_step_fail.append('2.2 Check icon arrow down highlight')

        self.assertListEqual(list_step_fail, [])

    def test_PS_Sort_customer(self):
        list_step_fail = []
        driver = self.driver

        driver.refresh()
        time.sleep(5)

        Sort.work_around(driver)

        column = 'Customer'
        # Sort increase
        sorted_increase = Helper.Helper_common.sorting_increase_column(driver, column)
        expected = sorted(sorted_increase, key=Sort.collator.getSortKey)
        try:
            with allure.step('Check result sorted Increase by ' + column):
                self.assertListEqual(sorted_increase, expected, 'Check sort increase by ' + column)
        except AssertionError:
            list_step_fail.append('1.1 Check sort increase by ' + column)

        actual_color = driver.find_element_by_xpath(
            '//div[@class="ant-table-scroll"]//tr/th//span[text()="' + column
            + '"]/..//div/i[@class="anticon anticon-caret-up ant-table-column-sorter-up on"]')
        actual_color = actual_color.value_of_css_property('color')
        actual_color = Helper.Helper_common.rbga_to_hex(actual_color)
        expected_color = '#1890ff'
        try:
            with allure.step('Check icon arrow up highlight'):
                self.assertEqual(actual_color, expected_color, 'Check increase sort is highlight')
        except AssertionError:
            list_step_fail.append('1.2 Check icon arrow up highlight')

        # Click back to first page
        list_page = driver.find_elements_by_css_selector('.ant-pagination>li')
        if len(list_page):
            driver.find_element_by_css_selector('.ant-pagination>li.ant-pagination-item-1').click()

        driver.refresh()
        time.sleep(5)
        Sort.work_around(driver)

        # Sort Decrease
        sorted_decrease = Helper.Helper_common.sorting_decrease_column(driver, column)
        expected = sorted(sorted_decrease, key=Sort.collator.getSortKey, reverse=True)
        try:
            with allure.step('Check result sorted Decrease by ' + column):
                self.assertListEqual(sorted_decrease, expected, 'Check sort decrease by ' + column)
        except AssertionError:
            list_step_fail.append('2.1 Check sort decrease by ' + column)

        actual_color = driver.find_element_by_xpath(
            '//div[@class="ant-table-scroll"]//tr/th//span[text()="' + column
            + '"]/..//div/i[@class="anticon anticon-caret-down ant-table-column-sorter-down on"]')
        actual_color = actual_color.value_of_css_property('color')
        actual_color = Helper.Helper_common.rbga_to_hex(actual_color)
        expected_color = '#1890ff'
        try:
            with allure.step('Check icon arrow down highlight'):
                self.assertEqual(actual_color, expected_color, 'Check decrease sort is highlight')
        except AssertionError:
            list_step_fail.append('2.2 Check icon arrow down highlight')

        self.assertListEqual(list_step_fail, [])

    def test_PS_Sort_department(self):
        list_step_fail = []
        driver = self.driver

        driver.refresh()
        time.sleep(5)

        Sort.work_around(driver)

        column = 'Department'
        # Sort increase
        sorted_increase = Helper.Helper_common.sorting_increase_column(driver, column)
        expected = sorted(sorted_increase, key=Sort.collator.getSortKey)
        try:
            with allure.step('Check result sorted Increase by ' + column):
                self.assertListEqual(sorted_increase, expected, 'Check sort increase by ' + column)
        except AssertionError:
            list_step_fail.append('1.1 Check sort increase by ' + column)

        actual_color = driver.find_element_by_xpath(
            '//div[@class="ant-table-scroll"]//tr/th//span[text()="' + column
            + '"]/..//div/i[@class="anticon anticon-caret-up ant-table-column-sorter-up on"]')
        actual_color = actual_color.value_of_css_property('color')
        actual_color = Helper.Helper_common.rbga_to_hex(actual_color)
        expected_color = '#1890ff'
        try:
            with allure.step('Check icon arrow up highlight'):
                self.assertEqual(actual_color, expected_color, 'Check increase sort is highlight')
        except AssertionError:
            list_step_fail.append('1.2 Check icon arrow up highlight')

        # Click back to first page
        list_page = driver.find_elements_by_css_selector('.ant-pagination>li')
        if len(list_page):
            driver.find_element_by_css_selector('.ant-pagination>li.ant-pagination-item-1').click()

        driver.refresh()
        time.sleep(5)
        Sort.work_around(driver)

        # Sort Decrease
        sorted_decrease = Helper.Helper_common.sorting_decrease_column(driver, column)
        expected = sorted(sorted_decrease, key=Sort.collator.getSortKey, reverse=True)
        try:
            with allure.step('Check result sorted Decrease by ' + column):
                self.assertListEqual(sorted_decrease, expected, 'Check sort decrease by ' + column)
        except AssertionError:
            list_step_fail.append('2.1 Check sort decrease by ' + column)

        actual_color = driver.find_element_by_xpath(
            '//div[@class="ant-table-scroll"]//tr/th//span[text()="' + column
            + '"]/..//div/i[@class="anticon anticon-caret-down ant-table-column-sorter-down on"]')
        actual_color = actual_color.value_of_css_property('color')
        actual_color = Helper.Helper_common.rbga_to_hex(actual_color)
        expected_color = '#1890ff'
        try:
            with allure.step('Check icon arrow down highlight'):
                self.assertEqual(actual_color, expected_color, 'Check decrease sort is highlight')
        except AssertionError:
            list_step_fail.append('2.2 Check icon arrow down highlight')

        self.assertListEqual(list_step_fail, [])

    def test_PS_Sort_total_budget(self):
        list_step_fail = []
        driver = self.driver

        driver.refresh()
        time.sleep(5)

        Sort.work_around(driver)

        column = 'Total budget'
        # Sort increase
        sorted_increase = Helper.Helper_common.sorting_increase_column(driver, column)
        expected = sorted(sorted_increase, reverse=True)
        try:
            with allure.step('Check result sorted Increase by ' + column):
                self.assertListEqual(sorted_increase, expected, 'Check sort increase by ' + column)
        except AssertionError:
            list_step_fail.append('1.1 Check sort increase by ' + column)

        actual_color = driver.find_element_by_xpath(
            '//div[@class="ant-table-scroll"]//tr/th//span[text()="' + column
            + '"]/..//div/i[@class="anticon anticon-caret-up ant-table-column-sorter-up on"]')
        actual_color = actual_color.value_of_css_property('color')
        actual_color = Helper.Helper_common.rbga_to_hex(actual_color)
        expected_color = '#1890ff'
        try:
            with allure.step('Check icon arrow up highlight'):
                self.assertEqual(actual_color, expected_color, 'Check increase sort is highlight')
        except AssertionError:
            list_step_fail.append('1.2 Check icon arrow up highlight')

        # Click back to first page
        list_page = driver.find_elements_by_css_selector('.ant-pagination>li')
        if len(list_page):
            driver.find_element_by_css_selector('.ant-pagination>li.ant-pagination-item-1').click()

        driver.refresh()
        time.sleep(5)
        Sort.work_around(driver)

        # Sort Decrease
        sorted_decrease = Helper.Helper_common.sorting_decrease_column(driver, column)
        expected = sorted(sorted_decrease)
        try:
            with allure.step('Check result sorted Decrease by ' + column):
                self.assertListEqual(sorted_decrease, expected, 'Check sort decrease by ' + column)
        except AssertionError:
            list_step_fail.append('2.1 Check sort decrease by ' + column)

        actual_color = driver.find_element_by_xpath(
            '//div[@class="ant-table-scroll"]//tr/th//span[text()="' + column
            + '"]/..//div/i[@class="anticon anticon-caret-down ant-table-column-sorter-down on"]')
        actual_color = actual_color.value_of_css_property('color')
        actual_color = Helper.Helper_common.rbga_to_hex(actual_color)
        expected_color = '#1890ff'
        try:
            with allure.step('Check icon arrow down highlight'):
                self.assertEqual(actual_color, expected_color, 'Check decrease sort is highlight')
        except AssertionError:
            list_step_fail.append('2.2 Check icon arrow down highlight')

        self.assertListEqual(list_step_fail, [])

    def test_PS_Sort_status(self):
        list_step_fail = []
        driver = self.driver

        driver.refresh()
        time.sleep(5)

        Sort.work_around(driver)

        column = 'Status'
        # Sort increase
        sorted_increase = Helper.Helper_common.sorting_increase_column(driver, column)
        expected = sorted(sorted_increase, key=Sort.collator.getSortKey)
        try:
            with allure.step('Check result sorted Increase by ' + column):
                self.assertListEqual(sorted_increase, expected, 'Check sort increase by ' + column)
        except AssertionError:
            list_step_fail.append('1.1 Check sort increase by ' + column)

        actual_color = driver.find_element_by_xpath(
            '//div[@class="ant-table-scroll"]//tr/th//span[text()="' + column
            + '"]/..//div/i[@class="anticon anticon-caret-up ant-table-column-sorter-up on"]')
        actual_color = actual_color.value_of_css_property('color')
        actual_color = Helper.Helper_common.rbga_to_hex(actual_color)
        expected_color = '#1890ff'
        try:
            with allure.step('Check icon arrow up highlight'):
                self.assertEqual(actual_color, expected_color, 'Check increase sort is highlight')
        except AssertionError:
            list_step_fail.append('1.2 Check icon arrow up highlight')

        # Click back to first page
        list_page = driver.find_elements_by_css_selector('.ant-pagination>li')
        if len(list_page):
            driver.find_element_by_css_selector('.ant-pagination>li.ant-pagination-item-1').click()

        driver.refresh()
        time.sleep(5)
        Sort.work_around(driver)

        # Sort Decrease
        sorted_decrease = Helper.Helper_common.sorting_decrease_column(driver, column)
        expected = sorted(sorted_decrease, key=Sort.collator.getSortKey, reverse=True)
        try:
            with allure.step('Check result sorted Decrease by ' + column):
                self.assertListEqual(sorted_decrease, expected, 'Check sort decrease by ' + column)
        except AssertionError:
            list_step_fail.append('2.1 Check sort decrease by ' + column)

        actual_color = driver.find_element_by_xpath(
            '//div[@class="ant-table-scroll"]//tr/th//span[text()="' + column
            + '"]/..//div/i[@class="anticon anticon-caret-down ant-table-column-sorter-down on"]')
        actual_color = actual_color.value_of_css_property('color')
        actual_color = Helper.Helper_common.rbga_to_hex(actual_color)
        expected_color = '#1890ff'
        try:
            with allure.step('Check icon arrow down highlight'):
                self.assertEqual(actual_color, expected_color, 'Check decrease sort is highlight')
        except AssertionError:
            list_step_fail.append('2.2 Check icon arrow down highlight')

        self.assertListEqual(list_step_fail, [])

    def test_PS_Sort_resolved_issues(self):
        list_step_fail = []
        driver = self.driver

        driver.refresh()
        time.sleep(5)

        Sort.work_around(driver)

        column = 'Resolved Issues'
        # Sort increase
        sorted_increase = Helper.Helper_common.sorting_increase_column(driver, column)
        expected = sorted(sorted_increase, key=Sort.collator.getSortKey)
        try:
            with allure.step('Check result sorted Increase by ' + column):
                self.assertListEqual(sorted_increase, expected, 'Check sort increase by ' + column)
        except AssertionError:
            list_step_fail.append('1.1 Check sort increase by ' + column)

        actual_color = driver.find_element_by_xpath(
            '//div[@class="ant-table-scroll"]//tr/th//span[text()="' + column
            + '"]/..//div/i[@class="anticon anticon-caret-up ant-table-column-sorter-up on"]')
        actual_color = actual_color.value_of_css_property('color')
        actual_color = Helper.Helper_common.rbga_to_hex(actual_color)
        expected_color = '#1890ff'
        try:
            with allure.step('Check icon arrow up highlight'):
                self.assertEqual(actual_color, expected_color, 'Check increase sort is highlight')
        except AssertionError:
            list_step_fail.append('1.2 Check icon arrow up highlight')

        # Click back to first page
        list_page = driver.find_elements_by_css_selector('.ant-pagination>li')
        if len(list_page):
            driver.find_element_by_css_selector('.ant-pagination>li.ant-pagination-item-1').click()

        driver.refresh()
        time.sleep(5)
        Sort.work_around(driver)

        # Sort Decrease
        sorted_decrease = Helper.Helper_common.sorting_decrease_column(driver, column)
        expected = sorted(sorted_decrease, key=Sort.collator.getSortKey, reverse=True)
        try:
            with allure.step('Check result sorted Decrease by ' + column):
                self.assertListEqual(sorted_decrease, expected, 'Check sort decrease by ' + column)
        except AssertionError:
            list_step_fail.append('2.1 Check sort decrease by ' + column)

        actual_color = driver.find_element_by_xpath(
            '//div[@class="ant-table-scroll"]//tr/th//span[text()="' + column
            + '"]/..//div/i[@class="anticon anticon-caret-down ant-table-column-sorter-down on"]')
        actual_color = actual_color.value_of_css_property('color')
        actual_color = Helper.Helper_common.rbga_to_hex(actual_color)
        expected_color = '#1890ff'
        try:
            with allure.step('Check icon arrow down highlight'):
                self.assertEqual(actual_color, expected_color, 'Check decrease sort is highlight')
        except AssertionError:
            list_step_fail.append('2.2 Check icon arrow down highlight')

        self.assertListEqual(list_step_fail, [])

    def test_PS_Sort_expense(self):
        list_step_fail = []
        driver = self.driver

        driver.refresh()
        time.sleep(5)

        Sort.work_around(driver)

        column = 'Expense'
        # Sort increase
        sorted_increase = Helper.Helper_common.sorting_increase_column(driver, column)
        expected = sorted(sorted_increase, key=Sort.collator.getSortKey)
        try:
            with allure.step('Check result sorted Increase by ' + column):
                self.assertListEqual(sorted_increase, expected, 'Check sort increase by ' + column)
        except AssertionError:
            list_step_fail.append('1.1 Check sort increase by ' + column)

        actual_color = driver.find_element_by_xpath(
            '//div[@class="ant-table-scroll"]//tr/th//span[text()="' + column
            + '"]/..//div/i[@class="anticon anticon-caret-up ant-table-column-sorter-up on"]')
        actual_color = actual_color.value_of_css_property('color')
        actual_color = Helper.Helper_common.rbga_to_hex(actual_color)
        expected_color = '#1890ff'
        try:
            with allure.step('Check icon arrow up highlight'):
                self.assertEqual(actual_color, expected_color, 'Check increase sort is highlight')
        except AssertionError:
            list_step_fail.append('1.2 Check icon arrow up highlight')

        # Click back to first page
        list_page = driver.find_elements_by_css_selector('.ant-pagination>li')
        if len(list_page):
            driver.find_element_by_css_selector('.ant-pagination>li.ant-pagination-item-1').click()

        driver.refresh()
        time.sleep(5)
        Sort.work_around(driver)

        # Sort Decrease
        sorted_decrease = Helper.Helper_common.sorting_decrease_column(driver, column)
        expected = sorted(sorted_decrease, key=Sort.collator.getSortKey, reverse=True)
        try:
            with allure.step('Check result sorted Decrease by ' + column):
                self.assertListEqual(sorted_decrease, expected, 'Check sort decrease by ' + column)
        except AssertionError:
            list_step_fail.append('2.1 Check sort decrease by ' + column)

        actual_color = driver.find_element_by_xpath(
            '//div[@class="ant-table-scroll"]//tr/th//span[text()="' + column
            + '"]/..//div/i[@class="anticon anticon-caret-down ant-table-column-sorter-down on"]')
        actual_color = actual_color.value_of_css_property('color')
        actual_color = Helper.Helper_common.rbga_to_hex(actual_color)
        expected_color = '#1890ff'
        try:
            with allure.step('Check icon arrow down highlight'):
                self.assertEqual(actual_color, expected_color, 'Check decrease sort is highlight')
        except AssertionError:
            list_step_fail.append('2.2 Check icon arrow down highlight')

        self.assertListEqual(list_step_fail, [])

    def test_PS_Sort_warranty(self):
        list_step_fail = []
        driver = self.driver

        driver.refresh()
        time.sleep(5)

        Sort.work_around(driver)

        column = 'Warranty'
        # Sort increase
        sorted_increase = Helper.Helper_common.sorting_increase_column(driver, column)
        actual = []
        for i in sorted_increase:
            actual.append(float(i.split('Month(s)')[0]))
        expected = sorted(actual)

        try:
            with allure.step('Check result sorted Increase by ' + column):
                self.assertListEqual(actual, expected, 'Check sort increase by ' + column)
        except AssertionError:
            list_step_fail.append('1.1 Check sort increase by ' + column)

        actual_color = driver.find_element_by_xpath(
            '//div[@class="ant-table-scroll"]//tr/th//span[text()="' + column
            + '"]/..//div/i[@class="anticon anticon-caret-up ant-table-column-sorter-up on"]')
        actual_color = actual_color.value_of_css_property('color')
        actual_color = Helper.Helper_common.rbga_to_hex(actual_color)
        expected_color = '#1890ff'
        try:
            with allure.step('Check icon arrow up highlight'):
                self.assertEqual(actual_color, expected_color, 'Check increase sort is highlight')
        except AssertionError:
            list_step_fail.append('1.2 Check icon arrow up highlight')

        # Click back to first page
        list_page = driver.find_elements_by_css_selector('.ant-pagination>li')
        if len(list_page):
            driver.find_element_by_css_selector('.ant-pagination>li.ant-pagination-item-1').click()

        driver.refresh()
        time.sleep(5)
        Sort.work_around(driver)

        # Sort Decrease
        sorted_decrease = Helper.Helper_common.sorting_decrease_column(driver, column)
        actual = []
        for i in sorted_decrease:
            actual.append(float(i.split('Month(s)')[0]))
        expected = sorted(actual, reverse=True)
        try:
            with allure.step('Check result sorted Decrease by ' + column):
                self.assertListEqual(actual, expected, 'Check sort decrease by ' + column)
        except AssertionError:
            list_step_fail.append('2.1 Check sort decrease by ' + column)

        actual_color = driver.find_element_by_xpath(
            '//div[@class="ant-table-scroll"]//tr/th//span[text()="' + column
            + '"]/..//div/i[@class="anticon anticon-caret-down ant-table-column-sorter-down on"]')
        actual_color = actual_color.value_of_css_property('color')
        actual_color = Helper.Helper_common.rbga_to_hex(actual_color)
        expected_color = '#1890ff'
        try:
            with allure.step('Check icon arrow down highlight'):
                self.assertEqual(actual_color, expected_color, 'Check decrease sort is highlight')
        except AssertionError:
            list_step_fail.append('2.2 Check icon arrow down highlight')

        self.assertListEqual(list_step_fail, [])

    def test_PS_Sort_start_date(self):
        list_step_fail = []
        driver = self.driver

        driver.refresh()
        time.sleep(5)

        Sort.work_around(driver)

        column = 'Start date'
        # Sort increase
        sorted_increase = Helper.Helper_common.sorting_increase_column(driver, column)

        # when blank data -> convert date time to 01/01/0001
        str_2_date = []
        for i in sorted_increase:
            if i == '':
                str_2_date.append(datetime.strptime('01/01/0001', '%d/%m/%Y'))
            else:
                str_2_date.append(datetime.strptime(i, '%d/%m/%Y'))
        str_2_date = sorted(str_2_date)

        # convert back to string, if date time = 01/01/0001 -> append blank
        date_2_str = []
        for i in str_2_date:
            if i.strftime('%d/%m/%Y') == '01/01/0001':
                date_2_str.append('')
            else:
                date_2_str.append(i.strftime('%d/%m/%Y'))

        try:
            with allure.step('Check result sorted Increase by ' + column):
                self.assertListEqual(sorted_increase, date_2_str, 'Check sort increase by ' + column)
        except AssertionError:
            list_step_fail.append('1.1 Check sort increase by ' + column)

        actual_color = driver.find_element_by_xpath(
            '//div[@class="ant-table-scroll"]//tr/th//span[text()="' + column
            + '"]/..//div/i[@class="anticon anticon-caret-up ant-table-column-sorter-up on"]')
        actual_color = actual_color.value_of_css_property('color')
        actual_color = Helper.Helper_common.rbga_to_hex(actual_color)
        expected_color = '#1890ff'
        try:
            with allure.step('Check icon arrow up highlight'):
                self.assertEqual(actual_color, expected_color, 'Check increase sort is highlight')
        except AssertionError:
            list_step_fail.append('1.2 Check icon arrow up highlight')

        # Click back to first page
        list_page = driver.find_elements_by_css_selector('.ant-pagination>li')
        if len(list_page):
            driver.find_element_by_css_selector('.ant-pagination>li.ant-pagination-item-1').click()

        driver.refresh()
        time.sleep(5)
        Sort.work_around(driver)

        # Sort Decrease
        sorted_decrease = Helper.Helper_common.sorting_decrease_column(driver, column)

        # when blank data -> convert date time to 01/01/0001
        str_2_date = []
        for i in sorted_decrease:
            if i == '':
                str_2_date.append(datetime.strptime('01/01/0001', '%d/%m/%Y'))
            else:
                str_2_date.append(datetime.strptime(i, '%d/%m/%Y'))
        str_2_date = sorted(str_2_date, reverse=True)

        # convert back to string, if date time = 01/01/0001 -> append blank
        date_2_str = []
        for i in str_2_date:
            if i.strftime('%d/%m/%Y') == '01/01/0001':
                date_2_str.append('')
            else:
                date_2_str.append(i.strftime('%d/%m/%Y'))
        try:
            with allure.step('Check result sorted Decrease by ' + column):
                self.assertListEqual(date_2_str, sorted_decrease, 'Check sort decrease by ' + column)
        except AssertionError:
            list_step_fail.append('2.1 Check sort decrease by ' + column)

        actual_color = driver.find_element_by_xpath(
            '//div[@class="ant-table-scroll"]//tr/th//span[text()="' + column
            + '"]/..//div/i[@class="anticon anticon-caret-down ant-table-column-sorter-down on"]')
        actual_color = actual_color.value_of_css_property('color')
        actual_color = Helper.Helper_common.rbga_to_hex(actual_color)
        expected_color = '#1890ff'
        try:
            with allure.step('Check icon arrow down highlight'):
                self.assertEqual(actual_color, expected_color, 'Check decrease sort is highlight')
        except AssertionError:
            list_step_fail.append('2.2 Check icon arrow down highlight')

        self.assertListEqual(list_step_fail, [])

    def test_PS_Sort_end_date(self):
        list_step_fail = []
        driver = self.driver

        driver.refresh()
        time.sleep(5)

        Sort.work_around(driver)

        column = 'End date'
        # Sort increase
        sorted_increase = Helper.Helper_common.sorting_increase_column(driver, column)

        # when blank data -> convert date time to 01/01/0001
        str_2_date = []
        for i in sorted_increase:
            if i == '':
                str_2_date.append(datetime.strptime('01/01/0001', '%d/%m/%Y'))
            else:
                str_2_date.append(datetime.strptime(i, '%d/%m/%Y'))
        str_2_date = sorted(str_2_date)

        # convert back to string, if date time = 01/01/0001 -> append blank
        date_2_str = []
        for i in str_2_date:
            if i.strftime('%d/%m/%Y') == '01/01/0001':
                date_2_str.append('')
            else:
                date_2_str.append(i.strftime('%d/%m/%Y'))

        try:
            with allure.step('Check result sorted Increase by ' + column):
                self.assertListEqual(sorted_increase, date_2_str, 'Check sort increase by ' + column)
        except AssertionError:
            list_step_fail.append('1.1 Check sort increase by ' + column)

        actual_color = driver.find_element_by_xpath(
            '//div[@class="ant-table-scroll"]//tr/th//span[text()="' + column
            + '"]/..//div/i[@class="anticon anticon-caret-up ant-table-column-sorter-up on"]')
        actual_color = actual_color.value_of_css_property('color')
        actual_color = Helper.Helper_common.rbga_to_hex(actual_color)
        expected_color = '#1890ff'
        try:
            with allure.step('Check icon arrow up highlight'):
                self.assertEqual(actual_color, expected_color, 'Check increase sort is highlight')
        except AssertionError:
            list_step_fail.append('1.2 Check icon arrow up highlight')

        # Click back to first page
        list_page = driver.find_elements_by_css_selector('.ant-pagination>li')
        if len(list_page):
            driver.find_element_by_css_selector('.ant-pagination>li.ant-pagination-item-1').click()

        driver.refresh()
        time.sleep(5)
        Sort.work_around(driver)

        # Sort Decrease
        sorted_decrease = Helper.Helper_common.sorting_decrease_column(driver, column)

        # when blank data -> convert date time to 01/01/0001
        str_2_date = []
        for i in sorted_decrease:
            if i == '':
                str_2_date.append(datetime.strptime('01/01/0001', '%d/%m/%Y'))
            else:
                str_2_date.append(datetime.strptime(i, '%d/%m/%Y'))
        str_2_date = sorted(str_2_date, reverse=True)

        # convert back to string, if date time = 01/01/0001 -> append blank
        date_2_str = []
        for i in str_2_date:
            if i.strftime('%d/%m/%Y') == '01/01/0001':
                date_2_str.append('')
            else:
                date_2_str.append(i.strftime('%d/%m/%Y'))
        try:
            with allure.step('Check result sorted Decrease by ' + column):
                self.assertListEqual(date_2_str, sorted_decrease, 'Check sort decrease by ' + column)
        except AssertionError:
            list_step_fail.append('2.1 Check sort decrease by ' + column)

        actual_color = driver.find_element_by_xpath(
            '//div[@class="ant-table-scroll"]//tr/th//span[text()="' + column
            + '"]/..//div/i[@class="anticon anticon-caret-down ant-table-column-sorter-down on"]')
        actual_color = actual_color.value_of_css_property('color')
        actual_color = Helper.Helper_common.rbga_to_hex(actual_color)
        expected_color = '#1890ff'
        try:
            with allure.step('Check icon arrow down highlight'):
                self.assertEqual(actual_color, expected_color, 'Check decrease sort is highlight')
        except AssertionError:
            list_step_fail.append('2.2 Check icon arrow down highlight')

        self.assertListEqual(list_step_fail, [])


if __name__ == '__main__':
    # unittest.main()
    HTMLTestRunner.main()
