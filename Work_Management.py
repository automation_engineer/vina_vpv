import calendar
import datetime
import re
import sys

import pyautogui
from selenium.webdriver.common.keys import Keys

sys.path.append('../')

import pytest
import random

import time
import unittest
import allure
from selenium import webdriver
from selenium.webdriver import ActionChains

from Helper.Helper_common import *

import glob
import os
import unittest
import configparser
import HTMLTestRunner
import openpyxl
from read_configs import *
from winreg import *
import Helper.Helper_common
from datetime import datetime
from calendar import monthrange


class GUI(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.driver = webdriver.Chrome('chromedriver.exe')
        driver = self.driver
        driver.maximize_window()

        # Log in
        login_payroll(driver)
        time.sleep(5)

        # Moving to test page
        driver.get(url_payroll + '/project-visibility/work-management')
        time.sleep(10)

    @classmethod
    def tearDownClass(self):
        self.driver.quit()

    @allure.story('Check Humbager icon')
    def test_WM_Humbager_icon(self):
        driver = self.driver
        list_fail = []

        driver.refresh()
        time.sleep(5)

        try:
            with allure.step('Verify Hambager icon'):
                ico_ham = driver.find_element_by_css_selector('span.icon-menu')
        except:
            list_fail.append('Unable to locate Hambager')

        try:
            with allure.step('Click on Hambager'):
                ActionChains(driver).move_to_element(ico_ham).click().perform()
                time.sleep(1)
        except:
            list_fail.append('Unable to click on Hambager')

        try:
            with allure.step('Verify menu appearance'):
                self.assertTrue(driver.find_element_by_css_selector(
                    'div.ant-drawer-wrapper-body').is_displayed() == True)
                time.sleep(1)
        except:
            list_fail.append('Menu is not displayed after clicking')

        try:
            with allure.step('Click on Hambager again'):
                ActionChains(driver).move_to_element(ico_ham).click().perform()
                time.sleep(1)
        except:
            list_fail.append('Issue when click on Hambager again')

        try:
            with allure.step('Verify menu disappear'):
                self.assertTrue(len(driver.find_elements_by_css_selector(
                    'div.ant-drawer-open')) == 0)
                time.sleep(1)
        except:
            list_fail.append('Menu is not closed')

        self.assertEqual(len(list_fail), 0, list_fail)

    @allure.story('Check GUI default')
    def test_WM_GUI_default(self):
        driver = self.driver
        list_fail = []

        driver.refresh()
        time.sleep(5)

        # Check Hambager icon
        try:
            with allure.step('Verify Hambager icon'):
                ico_ham = driver.find_element_by_css_selector('span.icon-menu')
        except:
            list_fail.append('Unable to locate Hambager')
            pass

        # Check label HUMAX Project Visibility
        try:
            with allure.step('Check Humax logo'):
                self.assertEqual(len(driver.find_elements_by_css_selector(
                    'img[src="/images/humax.png"]')), 1)
                time.sleep(0.1)
        except:
            list_fail.append('[Version 2]No Humax logo')
            pass

        try:
            with allure.step('Check Project Visibility text'):
                self.assertEqual(driver.find_element_by_css_selector('div.pvs').text, "Project Visibility")
                time.sleep(0.1)
        except:
            list_fail.append('No Project Visibility text')
            pass

        # Check menu bar
        try:
            with allure.step('Check Header bar'):
                self.assertEqual(len(driver.find_elements_by_css_selector(
                    'div.employee-info-header-nav>div.nav-main')), 1)
                time.sleep(0.1)
        except:
            list_fail.append('No Header bar')
            pass

        try:
            with allure.step('Check Header bar content'):
                actual = []
                for i in driver.find_elements_by_css_selector(
                        'div.employee-info-header-nav>div.nav-main>a.menu-item:not(.hidden)'):
                    actual.append(i.text)

                self.assertEqual(actual, ['Dashboard', 'Project Status', 'Work Management', 'Invoices', 'SOW',
                                          'Settings'])
        except:
            list_fail.append('The Menu bar is displayed as ' + str(actual))
            pass

        # Check label Work Management on menu
        try:
            with allure.step('Check label Work Management on menu'):
                self.assertTrue(len(driver.find_elements_by_css_selector(
                    'div.employee-info-header-nav>div.nav-main>a.menu-item.active')) > 0)
        except:
            list_fail.append('The Work Management label is not displayed')
            pass

        try:
            with allure.step('Check label Work Management colour'):
                self.assertEqual(driver.find_element_by_css_selector(
                    'div.employee-info-header-nav>div.nav-main>a.menu-item.active')
                                 .value_of_css_property('color'), 'rgba(255, 255, 255, 1)')
        except:
            list_fail.append('The Work Management label is displayed with incorrect colour')
            pass

        # Check label Work Timeline
        try:
            with allure.step('Check label Work Timeline'):
                self.assertEqual(driver.find_element_by_css_selector(
                    'div.page-content> * a.menu-item:first-child').text, 'Work Timeline')
        except:
            list_fail.append('The ' + driver.find_element_by_css_selector(
                    'div.page-content> * a.menu-item:first-child').text + ' label is displayed instead of [Work '
                                                                          'Timeline]')
            pass

        try:
            with allure.step('Check label Work Timeline colour'):
                self.assertEqual(driver.find_element_by_css_selector(
                    'div.page-content> * a.menu-item:first-child')
                                 .value_of_css_property('color'), 'rgba(66, 127, 236, 1)')
        except:
            list_fail.append('The [Work Timeline] is displayed with incorrect colour')
            pass

        # Check label Daily Work Report
        try:
            with allure.step('Check label Daily Work Report'):
                self.assertEqual(driver.find_element_by_css_selector(
                    'div.page-content> * a.menu-item:first-child+a').text, 'Daily Work Report')
        except:
            list_fail.append('The label ' + driver.find_element_by_css_selector(
                    'div.page-content> * a.menu-item:first-child+a').text + ' instead of [Daily Work Report]')
            pass

        # Check label Weekly Work Report
        try:
            with allure.step('Check label Weekly Work Report'):
                self.assertEqual(driver.find_element_by_css_selector(
                    'div.page-content> * a.menu-item:last-child').text, 'Weekly Work Report')
        except:
            list_fail.append('The label ' + driver.find_element_by_css_selector(
                    'div.page-content> * a.menu-item:last-child').text + ' instead of [Weekly Work Report]')
            pass

        # Check label Project Name
        try:
            with allure.step('Check label Project Name'):
                self.assertEqual(driver.find_element_by_css_selector(
                    'div.control-wrap:first-child>div>label').text, 'Project Name')
        except:
            list_fail.append('The label ' + driver.find_element_by_css_selector(
                    'div.control-wrap:first-child>div>label').text + ' instead of [Project Name]')
            pass

        # Check label Project Name default value
        try:
            with allure.step('Check default value for Project Name'):
                self.assertEqual(driver.find_element_by_css_selector(
                    'div.control-wrap:first-child> * div.react-select__placeholder').text, 'Choose a project')
        except:
            list_fail.append('The default value for Project Name is ' + driver.find_element_by_css_selector(
                    'div.control-wrap:first-child> * div.react-select__placeholder').text
                             + ' instead of [Choose a project]')
            pass

        # Check label Project Name chevron down
        try:
            with allure.step('Check chevron down icon for Project Name'):
                self.assertTrue(len(driver.find_elements_by_css_selector(
                    'div.control-wrap:first-child> * div.react-select__indicators> * span.icon-chevron-down')) > 0)
        except:
            list_fail.append('The chevron down icon for Project Name is not displayed')
            pass

        # Check label Feature Name
        try:
            with allure.step('Check label Feature Name'):
                self.assertEqual(driver.find_element_by_css_selector(
                    'div.control-wrap:first-child+div>div>label').text, 'Feature Name')
        except:
            list_fail.append('The label ' + driver.find_element_by_css_selector(
                    'div.control-wrap:first-child+div>div>label').text + ' instead of [Feature Name]')
            pass

        # Check label Feature Name default value
        try:
            with allure.step('Check default value for Project Name'):
                self.assertEqual(driver.find_element_by_css_selector(
                    'div.control-wrap:first-child+div> * div.react-select__placeholder').text, 'Choose a project')
        except:
            list_fail.append('The default value for Project Name is ' + driver.find_element_by_css_selector(
                    'div.control-wrap:first-child+div> * div.react-select__placeholder').text
                             + ' instead of [Choose a feature]')
            pass

        # Check label Feature Name chevron down
        try:
            with allure.step('Check chevron down icon for Feature Name'):
                self.assertTrue(len(driver.find_elements_by_css_selector(
                    'div.control-wrap:first-child+div> * div.react-select__indicators> * span.icon-chevron-down')) > 0)
        except:
            list_fail.append('The chevron down icon for Feature Name is not displayed')
            pass

        # Check label Work Type
        try:
            with allure.step('Check label Work Type'):
                self.assertEqual(driver.find_element_by_css_selector(
                    'div.control-wrap:last-child>div>label').text, 'Work Type')
        except:
            list_fail.append('The label ' + driver.find_element_by_css_selector(
                    'div.control-wrap:last-child>div>label').text + ' instead of [Work Type]')
            pass

        # Check label Work Type default value
        try:
            with allure.step('Check default value for Work Type'):
                self.assertEqual(driver.find_element_by_css_selector(
                    'div.control-wrap:last-child> * div.react-select__placeholder').text, 'Choose a project')
        except:
            list_fail.append('The default value for Work Type is ' + driver.find_element_by_css_selector(
                    'div.control-wrap:last-child> * div.react-select__placeholder').text
                             + ' instead of [Choose a work type]')
            pass

        # Check label Work Type chevron down
        try:
            with allure.step('Check chevron down icon for Work Type'):
                self.assertTrue(len(driver.find_elements_by_css_selector(
                    'div.control-wrap:last-child> * div.react-select__indicators> * span.icon-chevron-down')) > 0)
        except:
            list_fail.append('The chevron down icon for Work Type is not displayed')
            pass

        # Check "WORKS" label
        try:
            with allure.step('Check "WORKS" label'):
                self.assertEqual(driver.find_element_by_css_selector('div.header-left>h3').text, 'WORKS')
        except:
            list_fail.append('The label ' + driver.find_element_by_css_selector('div.header-left>h3').text +
                             ' is displayed instead of [WORKS]')
            pass

        # Check "#" label
        try:
            with allure.step('Check "#" label'):
                self.assertEqual(driver.find_element_by_css_selector(
                    'div.header-left> * span.text-secondary:not(.week-range-text)').text, '#')
        except:
            list_fail.append('The label ' + driver.find_element_by_css_selector(
                    'div.header-left> * span.text-secondary:not(.week-range-text)').text + ' is displayed instead of '
                                                                                           '[#]')
            pass

        # Check format "dd/mm/yy"
        try:
            with allure.step('Check format "dd/mm/yy"'):
                for i in driver.find_element_by_css_selector(
                        'div.header-left> * span.week-range-text').text.strip('(').strip(')').replace(' ', '').split('-'):
                    self.assertTrue(datetime.strptime(i, '%d/%b/%Y'))
        except:
            list_fail.append('The date format is not dd/mm/yy')
            pass

        # Check "DAY"/"WEEK"/"MONTH" button
        try:
            with allure.step('Check "DAY" button'):
                self.assertEqual(driver.find_element_by_css_selector(
                    'div.header-right>div>label:first-child>span:last-child>span').text, 'DAY')
        except:
            list_fail.append('The first button is ' + driver.find_element_by_css_selector(
                    'div.header-right>div>label:first-child>span:last-child>span').text + ' instead of [DAY]')
            pass

        try:
            with allure.step('Check "WEEK" button'):
                self.assertEqual(driver.find_element_by_css_selector(
                    'div.header-right>div>label:first-child+label>span:last-child>span').text, 'WEEK')
        except:
            list_fail.append('The second button is ' + driver.find_element_by_css_selector(
                    'div.header-right>div>label:first-child+label>span:last-child>span').text + ' instead of [WEEK]')
            pass

        try:
            with allure.step('Check "MONTH" button'):
                self.assertEqual(driver.find_element_by_css_selector(
                    'div.header-right>div>label:last-child>span:last-child>span').text, 'MONTH')
        except:
            list_fail.append('The last button is ' + driver.find_element_by_css_selector(
                    'div.header-right>div>label:last-child>span:last-child>span').text + ' instead of [MONTH]')
            pass

        try:
            with allure.step('Check Week highlight'):
                self.assertEqual(driver.find_element_by_css_selector(
                    'div.header-right>div>label.ant-radio-button-wrapper-checked').value_of_css_property(
                    'background-color'), 'rgba(66, 127, 236, 1)')
        except:
            list_fail.append('The Week is highlighted with incorrect colour')
            pass

        # Check "<",">" icon
        try:
            with allure.step('Check "<" icon'):
                self.assertTrue(len(driver.find_elements_by_css_selector('span.icon-cheveron-left')) > 0)
        except:
            list_fail.append('The "<" icon is not displayed')
            pass

        try:
            with allure.step('Check ">" icon'):
                self.assertTrue(len(driver.find_elements_by_css_selector('span.icon-cheveron-right')) > 0)
        except:
            list_fail.append('The ">" icon is not displayed')
            pass

        # Check Today label
        try:
            with allure.step('Check Today label'):
                self.assertEqual(driver.find_element_by_css_selector('span.text-today').text, '(TODAY)')
        except:
            list_fail.append('The label ' + driver.find_element_by_css_selector('span.text-today').text +
                             ' is displayed instead of [(TODAY)]')
            pass

        # Check format table
        try:
            with allure.step('Check Filter Options label'):
                self.assertEqual(driver.find_element_by_css_selector('div.filter-div').text, 'Filter Options')
        except:
            list_fail.append('The label ' + driver.find_element_by_css_selector('div.filter-div').text +
                             ' is displayed instead of [Filter Options]')
            pass

        try:
            with allure.step('Check Filter icon'):
                self.assertTrue(len(driver.find_elements_by_css_selector('span.icon-filter_list')) > 0)
        except:
            list_fail.append('The Filter icon is not displayed')
            pass

        try:
            with allure.step('Check date name header'):
                # check table header
                for i in driver.find_elements_by_css_selector('table.scheduler-bg-table> * th'):
                    self.assertTrue(i.find_element_by_css_selector('* span.day-name').text in
                                    ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN'])
        except:
            list_fail.append("The header is not displayed as ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN']")
            pass

        try:
            with allure.step('Check date text header'):
                # check table header
                for i in driver.find_elements_by_css_selector('table.scheduler-bg-table> * th'):
                    self.assertTrue(datetime.strptime(i.find_element_by_css_selector('* span.day-num').text, '%d/%m'))
        except:
            list_fail.append('The date format is not DD/MM')
            pass

        self.assertEqual(len(list_fail), 0, list_fail)


class WorkTimeline(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.driver = webdriver.Chrome('chromedriver.exe')
        driver = self.driver
        driver.maximize_window()

        # Log in
        login_payroll(driver)
        time.sleep(5)

        # Moving to test page
        driver.get(url_payroll + '/project-visibility/work-management')
        time.sleep(10)

    @classmethod
    def tearDownClass(self):
        self.driver.quit()

    @allure.step('Check change Week')
    def test_WM_WorkTimeline_change_Week(self):
        driver = self.driver
        list_fail = []

        driver.refresh()
        time.sleep(5)

        try:
            with allure.step('Check Week dropdown'):
                # check week Dropdown
                self.assertTrue(
                    len(driver.find_elements_by_css_selector('div.select-week>div.react-select__control')) > 0)
        except:
            list_fail.append('No Week dropdown')

        try:
            with allure.step('Select new week'):
                # click on dropdown and select random week
                ActionChains(driver).move_to_element(
                    driver.find_element_by_css_selector('div.select-week>div.react-select__control')).click().perform()
                time.sleep(0.5)

                selections = driver.find_elements_by_css_selector(
                    'div.react-select__menu-list>div:not(.react-select__option--is-selected)')

                ActionChains(driver).move_to_element(
                    selections[random.randint(0, len(selections) - 1)]).click().perform()
                time.sleep(5)
        except:
            list_fail.append('Unable to select another week')

        try:
            with allure.step('Check Filter Options label'):
                self.assertEqual(driver.find_element_by_css_selector('div.filter-div').text, 'Filter Options')
        except:
            list_fail.append('The label ' + driver.find_element_by_css_selector('div.filter-div').text +
                             ' is displayed instead of [Filter Options]')
            pass

        try:
            with allure.step('Check date name header'):
                # check table header
                for i in driver.find_elements_by_css_selector('table.scheduler-bg-table> * th'):
                    self.assertTrue(i.find_element_by_css_selector('* span.day-name').text in
                                    ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN'])
        except:
            list_fail.append("The header is not displayed as ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN']")
            pass

        try:
            with allure.step('Check date text header'):
                # check table header
                for i in driver.find_elements_by_css_selector('table.scheduler-bg-table> * th'):
                    self.assertTrue(datetime.strptime(i.find_element_by_css_selector('* span.day-num').text, '%d/%m'))
        except:
            list_fail.append('The date format is not DD/MM')
            pass

        try:
            with allure.step('Check Monday highlight'):
                self.assertEqual(rbga_to_hex(driver.find_element_by_css_selector(
                    'table.scheduler-bg-table> * th:first-child').value_of_css_property(
                    'border-left-color')).upper(), '#7A8EBB')
        except:
            list_fail.append('The Monday is highlight with incorrect colour')
            pass

        try:
            with allure.step('Check Sat/Sun background colour'):
                # check Sat bg colour
                sat_color = driver.find_element_by_css_selector('table.scheduler-bg-table> * th:nth-last-child(2)'). \
                    value_of_css_property('background-color')
                sun_color = driver.find_element_by_css_selector('table.scheduler-bg-table> * th:last-child'). \
                    value_of_css_property('background-color')

                self.assertEquals(sat_color, sun_color, 'rgba(225, 236, 255, 0.5)')
        except:
            list_fail.append('The background colour of Sat/Sun is not [rgba(225,236,255,0.5)]')

        try:
            with allure.step('Check user Alexiel'):
                # check current user name
                scroll_to(driver, driver.find_element_by_xpath("//div[contains(text(), 'Alexiel')]"))
                time.sleep(0.2)
        except:
            list_fail.append('Username Alexiel is not available')

        try:
            with allure.step('Check department'):
                # check user department
                self.assertTrue(
                    driver.find_element_by_xpath("//div[contains(text(), 'Alexiel')]/following-sibling::div"))
        except:
            list_fail.append("User's department is not displayed")

        try:
            with allure.step('Check avatar'):
                # check user avatar
                self.assertTrue(
                    driver.find_element_by_xpath("//div[contains(text(), 'Alexiel')]/../preceding-sibling::span"))
        except:
            list_fail.append("User's avatar is not displayed")

        try:
            with allure.step('Check week icons'):
                # check week icon
                week_icon = []
                for i in driver.find_elements_by_xpath(
                        "//div[contains(text(), 'Alexiel')]/../../following::div[1]/div/div[@class='day']"):
                    week_icon.append(i.text)
                self.assertEqual(week_icon, ['M', 'T', 'W', 'T', 'F', 'S', 'S'])
        except:
            list_fail.append("The week order is not ['M', 'T', 'W', 'T', 'F', 'S', 'S']")

        try:
            with allure.step('Check text Total'):
                # check text [Total: x/40]
                total_text = ''
                for i in driver.find_elements_by_xpath(
                        "//div[contains(text(), 'Alexiel')]/../../following-sibling::div[2]/span"):
                    total_text = total_text + i.text
                self.assertTrue('Total:' in total_text and '/40' in total_text)
        except:
            list_fail.append('The Total text is displayed in wrong format')

        self.assertEqual(len(list_fail), 0, list_fail)

    def test_WM_WorkTimeline_Day(self):
        driver = self.driver
        list_fail = []

        driver.refresh()
        time.sleep(5)

        try:
            with allure.step('Click 1st button'):
                ActionChains(driver).move_to_element(
                    driver.find_element_by_css_selector('div.header-right> * label:first-child')).click().perform()
                time.sleep(3)
        except:
            list_fail.append('The first button is unclickable')

        try:
            with allure.step('Check DAY highlight'):
                self.assertEqual(rbg_to_hex(driver.find_element_by_css_selector(
                    'div.header-right> * label:first-child').value_of_css_property('border-color')), '#427fec')
        except:
            list_fail.append('The DAY button is highlighted with incorrect colour')

        try:
            with allure.step('Check "<" icon'):
                self.assertTrue(len(driver.find_elements_by_css_selector(
                    'div.header-mid>div>span.icon-cheveron-left')) > 0)
        except:
            list_fail.append('The "<" icon is not displayed')

        try:
            with allure.step('Check ">" icon'):
                self.assertTrue(len(driver.find_elements_by_css_selector(
                    'div.header-mid>div>span.icon-cheveron-right')) > 0)
        except:
            list_fail.append('The ">" icon is not displayed')

        try:
            with allure.step('Check [Today] label'):
                self.assertEqual(driver.find_element_by_css_selector('div.header-mid>div>span:first-child+span').text,
                                 'Today')
        except:
            list_fail.append('The [Today] label is not displayed')

        try:
            with allure.step('Check Filter Options label'):
                # Filter Options label
                self.assertEqual(driver.find_element_by_css_selector('div.filter-div').text, 'Filter Options')
        except:
            list_fail.append('No Filter Options label')

        try:
            with allure.step('Check Filter icon'):
                # check Filter icon
                self.assertTrue(len(driver.find_elements_by_css_selector('div.filter-div>span.icon-filter_list')) > 0)
        except:
            list_fail.append('No Filter icon')

        try:
            with allure.step('Check date name header'):
                # check table header
                for i in driver.find_elements_by_css_selector('div.week-days'):
                    self.assertTrue(i.find_element_by_css_selector('* span.day-name').text in
                                    ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN'])
        except:
            list_fail.append("The header is not displayed as ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN']")

        today = datetime.today().strftime('%a')
        try:
            with allure.step('Check [TODAY] text for current day'):
                self.assertTrue(len(driver.find_elements_by_xpath('//span[@class="day-name"][contains(text(),"' + today
                                                                  + '")]/following::span[contains(@class,"text-today")]')) > 0)
                try:
                    self.assertEqual(driver.find_element_by_xpath('//span[@class="day-name"][contains(text(),"' + today
                                                                  + '")]/following::span[contains(@class,"text-today")]').text,
                                     '(TODAY)')
                except:
                    list_fail.append('The text [(TODAY)] is not displayed')
        except:
            list_fail.append('The [TODAY] label is not displayed in ' + today)

        try:
            with allure.step('Check highlight of today'):
                self.assertNotEqual(driver.find_element_by_css_selector('div.is-today')
                                    .value_of_css_property('border-top-color'), 'rgba(0, 0, 0, 0)')
        except:
            list_fail.append('Today block is not highlighted')

        try:
            with allure.step('Check copy yesterday button'):
                self.assertTrue(len(driver.find_elements_by_css_selector('div.is-today>label>svg.copy-file')) > 0)
        except:
            list_fail.append('No copy yesterday button')

        try:
            with allure.step('Check time stamp'):
                # check timestamp
                timestamp = []
                for i in driver.find_elements_by_css_selector('table.scheduler-bg-table> * th>div>span'):
                    scroll_to(driver, i)
                    timestamp.append(i.text)

                expected = ['00h30', '01h', '01h30', '02h', '02h30', '03h', '03h30', '04h', '04h30', '05h',
                            '05h30', '06h', '06h30', '07h', '07h30', '08h', '08h30', '09h', '09h30', '10h',
                            '10h30', '11h', '11h30', '12h', '12h30', '13h', '13h30', '14h', '14h30', '15h',
                            '15h30', '16h', '16h30', '17h', '17h30', '18h', '18h30', '19h', '19h30', '20h',
                            '20h30', '21h', '21h30', '22h', '22h30', '23h', '23h30', '00h']

                self.assertEqual(timestamp, expected)
        except:
            list_fail.append('The different value is ' + str(list(set(expected) - set(timestamp))))

        try:
            with allure.step('Check Sat/Sun background colour'):
                # check Sat bg colour
                sat_color = driver.find_element_by_css_selector('table.scheduler-bg-table> * th:nth-last-child(2)'). \
                    value_of_css_property('background-color')
                sun_color = driver.find_element_by_css_selector('table.scheduler-bg-table> * th:last-child'). \
                    value_of_css_property('background-color')

                self.assertEquals(sat_color, sun_color, 'rgba(225, 236, 255, 0.5)')
        except:
            list_fail.append('The background colour of Sat/Sun is not [rgba(225,236,255,0.5)]')

        try:
            with allure.step('Check real time line'):
                # check current time tail
                self.assertEqual(len(driver.find_elements_by_css_selector('div.current-time-tail')), 1)
        except:
            list_fail.append('The real time line is not displayed')

        try:
            with allure.step('Check user Alexiel'):
                # check current user name
                scroll_to(driver, driver.find_element_by_xpath("//div[contains(text(), 'Alexiel')]"))
                time.sleep(0.2)
        except:
            list_fail.append('Username Alexiel is not available')

        try:
            with allure.step('Check department'):
                # check user department
                self.assertTrue(
                    driver.find_element_by_xpath("//div[contains(text(), 'Alexiel')]/following-sibling::div"))
        except:
            list_fail.append("User's department is not displayed")

        try:
            with allure.step('Check avatar'):
                # check user avatar
                self.assertTrue(
                    driver.find_element_by_xpath("//div[contains(text(), 'Alexiel')]/../preceding-sibling::span"))
        except:
            list_fail.append("User's avatar is not displayed")

        try:
            with allure.step('Check week icons'):
                # check week icon
                week_icon = []
                for i in driver.find_elements_by_xpath(
                        "//div[contains(text(), 'Alexiel')]/../../following::div[1]/div/div[@class='day']"):
                    week_icon.append(i.text)
                self.assertEqual(week_icon, ['M', 'T', 'W', 'T', 'F', 'S', 'S'])
        except:
            list_fail.append("The week order is not ['M', 'T', 'W', 'T', 'F', 'S', 'S']")

        try:
            with allure.step('Check text Total'):
                # check text [Total: x/40]
                total_text = ''
                for i in driver.find_elements_by_xpath(
                        "//div[contains(text(), 'Alexiel')]/../../following-sibling::div[2]/span"):
                    total_text = total_text + i.text
                self.assertTrue('Total:' in total_text and '/40' in total_text)
        except:
            list_fail.append('The Total text is displayed in wrong format')

        try:
            with allure.step('Verify Vertical scrollbar'):
                offsetHeight = int(
                    driver.find_element_by_css_selector('div.resource-view').get_property('offsetHeight'))
                scrollHeight = int(
                    driver.find_element_by_css_selector('table.resource-table>tbody').get_property('scrollHeight'))

                self.assertTrue(scrollHeight > offsetHeight)
                time.sleep(0.01)
        except:
            list_fail.append('No Vertical scrollbar')
            pass

        self.assertEqual(len(list_fail), 0, list_fail)

    def test_WM_WorkTimeline_Month(self):
        driver = self.driver
        list_fail = []

        driver.refresh()
        time.sleep(5)

        try:
            with allure.step('Click last button'):
                ActionChains(driver).move_to_element(
                    driver.find_element_by_css_selector('div.header-right> * label:last-child')).click().perform()
                time.sleep(3)
        except:
            list_fail.append('The last button is unclickable')

        try:
            with allure.step('Check MONTH highlight'):
                self.assertEqual(rbg_to_hex(driver.find_element_by_css_selector(
                    'div.header-right> * label:last-child').value_of_css_property('border-color')), '#427fec')
        except:
            list_fail.append('The MONTH button is highlighted with incorrect colour')

        try:
            with allure.step('Check "<" icon'):
                self.assertTrue(len(driver.find_elements_by_css_selector(
                    'div.header-mid>div>span.icon-cheveron-left')) > 0)
        except:
            list_fail.append('The "<" icon is not displayed')

        try:
            with allure.step('Check ">" icon'):
                self.assertTrue(len(driver.find_elements_by_css_selector(
                    'div.header-mid>div>span.icon-cheveron-right')) > 0)
        except:
            list_fail.append('The ">" icon is not displayed')

        try:
            with allure.step('Check [This Month] label'):
                self.assertEqual(driver.find_element_by_css_selector('div.header-mid>div>span:first-child+span').text,
                                 'This Month')
        except:
            list_fail.append('The [This Month] label is not displayed')

        try:
            with allure.step('Check Filter Options label'):
                # Filter Options label
                self.assertEqual(driver.find_element_by_css_selector('div.filter-div').text, 'Filter Options')
        except:
            list_fail.append('No Filter Options label')

        try:
            with allure.step('Check Filter icon'):
                # check Filter icon
                self.assertTrue(len(driver.find_elements_by_css_selector('div.filter-div>span.icon-filter_list')) > 0)
        except:
            list_fail.append('No Filter icon')

        try:
            with allure.step('Check date with current month'):
                # list no of date in this month
                a = datetime.today().strftime('%Y')
                b = datetime.today().strftime('%m')
                c = monthrange(int(a), int(b))

                expected = []
                for i in range(1, c[1] + 1):
                    expected.append(str(f'{i:02}') + '/' + str(b))

                # fetch current date
                actual = []
                for i in driver.find_elements_by_css_selector(
                        'div.scheduler-view-scroll-header>table.scheduler-bg-table> * th> * span.day-num'):
                    actual.append(i.text)

                self.assertEqual(expected, actual)
        except:
            list_fail.append('The number of dates in table does not match current month')

        try:
            with allure.step('Check Today highlight'):
                self.assertNotEqual(driver.find_element_by_css_selector('th.is-today').value_of_css_property(
                    'border-top-color'), 'rgba(0, 0, 0, 0.65)')
        except:
            list_fail.append('Today is not highlighted')

        try:
            with allure.step('Check Sat/Sun background colour'):
                # check Sat bg colour
                sat_color = driver.find_element_by_css_selector('table.scheduler-bg-table> * th:nth-last-child(2)'). \
                    value_of_css_property('background-color')
                sun_color = driver.find_element_by_css_selector('table.scheduler-bg-table> * th:last-child'). \
                    value_of_css_property('background-color')

                self.assertEquals(sat_color, sun_color, 'rgba(225, 236, 255, 0.5)')
        except:
            list_fail.append('The background colour of Sat/Sun is not [rgba(225,236,255,0.5)]')

        try:
            with allure.step('Check user Alexiel'):
                # check current user name
                scroll_to(driver, driver.find_element_by_xpath("//div[contains(text(), 'Alexiel')]"))
                time.sleep(0.2)
        except:
            list_fail.append('Username Alexiel is not available')

        try:
            with allure.step('Check department'):
                # check user department
                self.assertTrue(
                    driver.find_element_by_xpath("//div[contains(text(), 'Alexiel')]/following-sibling::div"))
        except:
            list_fail.append("User's department is not displayed")

        try:
            with allure.step('Check avatar'):
                # check user avatar
                self.assertTrue(
                    driver.find_element_by_xpath("//div[contains(text(), 'Alexiel')]/../preceding-sibling::span"))
        except:
            list_fail.append("User's avatar is not displayed")

        try:
            with allure.step('Check week icons'):
                # check week icon
                week_icon = []
                for i in driver.find_elements_by_xpath(
                        "//div[contains(text(), 'Alexiel')]/../../following::div[1]/div/div[@class='day']"):
                    week_icon.append(i.text)
                self.assertEqual(week_icon, ['M', 'T', 'W', 'T', 'F', 'S', 'S'])
        except:
            list_fail.append("The week order is not ['M', 'T', 'W', 'T', 'F', 'S', 'S']")

        try:
            with allure.step('Check text Total'):
                # check text [Total: x/40]
                total_text = ''
                for i in driver.find_elements_by_xpath(
                        "//div[contains(text(), 'Alexiel')]/../../following-sibling::div[2]/span"):
                    total_text = total_text + i.text
                self.assertTrue('Total:' in total_text and '/40' in total_text)
        except:
            list_fail.append('The Total text is displayed in wrong format')

        try:
            with allure.step('Verify Vertical scrollbar'):
                offsetHeight = int(
                    driver.find_element_by_css_selector('div.resource-view').get_property('offsetHeight'))
                scrollHeight = int(
                    driver.find_element_by_css_selector('table.resource-table>tbody').get_property('scrollHeight'))

                self.assertTrue(scrollHeight > offsetHeight)
                time.sleep(0.01)
        except:
            list_fail.append('No Vertical scrollbar')
            pass

        self.assertEqual(len(list_fail), 0, list_fail)

    def test_WM_WorkTimeline_Week_Filter_Options_Clear_All(self):
        driver = self.driver
        list_fail = []

        driver.refresh()
        time.sleep(5)

        self.assertEqual(len(list_fail), 0, list_fail)


if __name__ == '__main__':
    # unittest.main()
    HTMLTestRunner.main()
