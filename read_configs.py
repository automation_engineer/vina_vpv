import sys
import configparser

sys.path.append('../')


config = configparser.ConfigParser()
config.read_file(open(r'Config/config_common.txt'))
url_root = config.get('URL', 'url')
url_payroll = config.get('URL_PAYROLL', 'url')
username = config.get('User_info', 'username')
password = config.get('User_info', 'password')