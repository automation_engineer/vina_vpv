import calendar
import datetime
import re
import sys

import pyautogui
from selenium.webdriver.common.keys import Keys

sys.path.append('../')

import pytest
import random

import time
import unittest
import allure
from selenium import webdriver
from selenium.webdriver import ActionChains

from Helper.Helper_common import *

import glob
import os
import unittest
import configparser
import HTMLTestRunner
import openpyxl
from read_configs import *
from winreg import *
import Helper.Helper_common
from datetime import datetime


class Search(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.driver = webdriver.Chrome('chromedriver.exe')
        driver = self.driver
        driver.maximize_window()

        # Log in
        login_payroll(driver)
        time.sleep(5)

        # Moving to test page
        driver.get(url_payroll + '/payroll/compensation')
        time.sleep(10)

    @classmethod
    def tearDownClass(self):
        self.driver.quit()

    @allure.story('Check Humbager icon')
    def test_C_Humbager_icon(self):
        driver = self.driver
        list_fail = []

        driver.refresh()
        time.sleep(5)

        try:
            with allure.step('Verify Hambager icon'):
                ico_ham = driver.find_element_by_css_selector('span.icon-menu')
        except:
            list_fail.append('Unable to locate Hambager')

        try:
            with allure.step('Click on Hambager'):
                ActionChains(driver).move_to_element(ico_ham).click().perform()
                time.sleep(1)
        except:
            list_fail.append('Unable to click on Hambager')

        try:
            with allure.step('Verify menu appearance'):
                self.assertTrue(driver.find_element_by_css_selector(
                    'div.ant-drawer-wrapper-body').is_displayed() == True)
                time.sleep(1)
        except:
            list_fail.append('Menu is not displayed after clicking')

        try:
            with allure.step('Click on Hambager again'):
                ActionChains(driver).move_to_element(ico_ham).click().perform()
                time.sleep(1)
        except:
            list_fail.append('Issue when click on Hambager again')

        try:
            with allure.step('Verify menu disappear'):
                self.assertTrue(len(driver.find_elements_by_css_selector(
                    'div.ant-drawer-open')) == 0)
                time.sleep(1)
        except:
            list_fail.append('Menu is not closed')

        self.assertEqual(len(list_fail), 0, list_fail)

    @allure.story('Check sort employee')
    def test_C_Search_English_name(self):
        driver = self.driver
        list_fail = []

        driver.refresh()
        time.sleep(5)

        try:
            with allure.step('Check default text of search box'):
                # default text
                default_text = driver.find_element_by_css_selector('div.ant-select-selection__placeholder').text
                self.assertEqual(default_text, 'Search')
        except:
            list_fail.append('The default text is ' + default_text + ' instead of [Search]')

        # fetch list name from api
        available_names = []
        for i in api_full_employees():
            available_names.append(i['englishName'])

        driver.find_element_by_css_selector('div.smart-search> * input').clear()
        time.sleep(0.2)

        random_name = available_names[random.randint(0, len(available_names)) - 1]

        # enter random name
        driver.find_element_by_css_selector('div.smart-search> * input') \
            .send_keys(random_name)
        time.sleep(0.2)

        try:
            with allure.step('Check value of current text'):
                # compare entered text and current text box
                self.assertEqual(driver.find_element_by_css_selector(
                    'div.smart-search> * input').get_attribute('value'), random_name)
        except:
            list_fail.append('The current text is not the same as entered text')

        # click Search button
        ActionChains(driver).move_to_element(
            driver.find_element_by_css_selector('div.smart-search>button.search-btn')).click().perform()
        time.sleep(2)

        try:
            with allure.step('Check search result'):
                for i in driver.find_elements_by_css_selector(
                        'div.payroll-table__table-frozen-left>div.payroll-table__body> * '
                        'div[role="gridcell"]:nth-child(2)'):
                    self.assertTrue(i.text in random_name)
        except:
            list_fail.append('The search result is incorrect')

        self.assertEqual(len(list_fail), 0, list_fail)

    @allure.story('Check recently Search')
    def test_C_Recently_Search(self):
        driver = self.driver
        list_fail = []

        driver.refresh()
        time.sleep(5)

        # fetch list name from api
        available_names = []
        for i in api_full_employees():
            available_names.append(i['englishName'])

        driver.find_element_by_css_selector('div.smart-search> * input').clear()
        time.sleep(0.2)

        random_name = available_names[random.randint(0, len(available_names)) - 1]

        # enter random name
        driver.find_element_by_css_selector('div.smart-search> * input') \
            .send_keys(random_name)
        time.sleep(0.5)

        try:
            with allure.step('Click Search button'):
                # click Search button
                ActionChains(driver).move_to_element(
                    driver.find_element_by_css_selector('div.smart-search>button.search-btn')).click().perform()
                time.sleep(2)
        except:
            list_fail.append('Unable to click Search button')

        try:
            with allure.step('Check recently searched list'):
                # check name in recently searched
                recent_list = []
                for i in driver.find_elements_by_css_selector('span.suggestion'):
                    recent_list.append(i.text)
                self.assertTrue(random_name in recent_list)
                time.sleep(0.5)
        except:
            list_fail.append('The recently searched list does not contain the entered name')

        try:
            with allure.step('Check recently searched highlight'):
                # check highlight
                for i in driver.find_elements_by_css_selector('span.suggestion'):
                    self.assertEqual(i.value_of_css_property('color'), 'rgba(66, 127, 236, 1)')
        except:
            list_fail.append('The highlight of recently searched is incorrect')

        # refresh page
        driver.refresh()
        time.sleep(5)

        # click on recent search
        for i in driver.find_elements_by_css_selector('span.suggestion'):
            if i.text == random_name:
                ActionChains(driver).move_to_element(driver.find_element_by_xpath(
                    '//span[@class="suggestion"][contains(text(), "' + i.text + '")]')).click().perform()
                time.sleep(1)

        try:
            with allure.step('Check text is copied to text box'):
                # verify text is copied to text box
                self.assertEqual(driver.find_element_by_css_selector(
                    'div.smart-search> * input').get_attribute('value'), random_name)
        except:
            list_fail.append('The text is not copied to text box')

        # click Search button
        ActionChains(driver).move_to_element(
            driver.find_element_by_css_selector('div.smart-search>button.search-btn')).click().perform()
        time.sleep(2)

        try:
            with allure.step('Check search result'):
                for i in driver.find_elements_by_css_selector(
                        'div.payroll-table__table-frozen-left>div.payroll-table__body> * '
                        'div[role="gridcell"]:nth-child(2)'):
                    self.assertTrue(i.text in random_name)
        except:
            list_fail.append('The search result from Recently searched is incorrect')

        self.assertEqual(len(list_fail), 0, list_fail)

    @allure.story('Check filter Year')
    def test_C_filter_Year(self):
        driver = self.driver
        list_fail = []

        driver.refresh()
        time.sleep(5)

        try:
            with allure.step('Click on filter Year'):
                # click on filter Year
                filter = driver.find_element_by_css_selector(
                    'div.payroll-option__datetime>div.ant-select-enabled>div[role="combobox"]')
                ActionChains(driver).move_to_element(filter).click().perform()
                time.sleep(0.2)
        except:
            list_fail.append('Unable to click on filter Year')

        try:
            with allure.step('Select a random year'):
                # select random year
                selections = driver.find_elements_by_css_selector('ul[role="listbox"]>li:not([aria-selected="true"])')
                ActionChains(driver).move_to_element(selections[random.randint(0, len(selections) -1 )]).click().perform()
                time.sleep(3)
        except:
            list_fail.append('Unable to select random year')

        # check list result, if 0 = No Data
        results = driver.find_elements_by_css_selector(
                    'div.payroll-table__table-main>div.payroll-table__body>div>div')

        if len(results) == 0:
            try:
                with allure.step('0 records, check text [No Data]'):
                    self.assertEqual(driver.find_element_by_css_selector('p.ant-empty-description').text, 'No Data')
            except:
                list_fail.append('Text ' + driver.find_element_by_css_selector('p.ant-empty-description').text +
                                 ' is displayed instead of [No Data]')
        else:
            try:
                with allure.step('> 0 records, check conditions'):
                    self.assertTrue()
            except:
                list_fail.append('The return result is incorrect')

        self.assertEqual(len(list_fail), 0, list_fail)

    # @allure.story('')
    # def test_C_Sort_English_name(self):
    #     driver = self.driver
    #     list_fail = []
    #
    #     # driver.refresh()
    #     # time.sleep(5)
    #
    #     # fetch list name from api
    #     available_names = []
    #     for i in api_full_employees():
    #         available_names.append(i['englishName'])
    #
    #     blank = []
    #     for i in driver.find_elements_by_css_selector(
    #         'div.payroll-table__table-frozen-left>div.payroll-table__body>div>div'):
    #         i.send_keys(Keys.ARROW_DOWN)
    #         time.sleep(0.2)
    #
    #     self.assertEqual(len(list_fail), 0, list_fail)


class Excel_Interact(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.driver = webdriver.Chrome('chromedriver.exe')
        driver = self.driver
        driver.maximize_window()

        # Log in
        login_payroll(driver)
        time.sleep(5)

        # Moving to test page
        driver.get(url_payroll + '/payroll/compensation')
        time.sleep(10)

    @classmethod
    def tearDownClass(self):
        self.driver.quit()


if __name__ == '__main__':
    # unittest.main()
    HTMLTestRunner.main()
