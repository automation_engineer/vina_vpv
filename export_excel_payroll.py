#!/usr/bin/env python
# -*- coding: utf-8 -*-
from selenium import webdriver
import time
import openpyxl
import sys, os
from openpyxl.styles import PatternFill
# from API_test import report_html, report_xlsx
# file = 'Report\\report_request.html'
def export_excel(*argv):

    # parse test result from HTML file
    for file in argv:
        driver = webdriver.Chrome("chromedriver.exe")  # open google chrome
        report_html = os.path.join(os.getcwd(), file)
        driver.get(report_html)

        list_detail_text = driver.find_elements_by_xpath('//a[text()="Detail"]')
        for text_detail in list_detail_text:
            text_detail.click()

        list_popup = driver.find_elements_by_class_name('popup_link')

        # get list of test case
        list_test_case = driver.find_elements_by_class_name('testcase')
        list_detail = driver.find_elements_by_xpath('//td[@colspan = "5"]')

        # click to display detail information
        for popup in list_popup:
            if popup.text != 'pass':
                popup.click()
                time.sleep(1)

        output = []
        num_failed = 0
        # parse data from HTML
        for i in range(len(list_test_case)):
            actual_result = ''
            output.append([])
            res = str(list_detail[i].text)
            if res == "pass" != -1:
                result = "PASS"
                failed_reason = 'As expected'
            else:#if res.strip() == "fail":# != -1:
                if 'AssertionError' not in list_detail[i].text:
                    failed_reason = '[ERROR] Test scripts were error. Please check it again.'
                    result = "ERROR"
                elif 'AssertionError' in list_detail[i].text:
                    failed_reason = ((list_detail[i].text).split('AssertionError: ')[1]) #.split(" : ")[1]
                    result = "FAIL"
                # else:
                #     find_actual = str(list_detail[i].text).splitlines()
                #     for line in find_actual:
                #         if line.find(': [') != -1:
                #             actual_result = line.split(':')[-1].split(']')[-1].strip()
                #
                # lines = str(list_detail[i].text).splitlines()
                # for line in lines:
                #     if line.find(': [') != -1:
                #         failed_reason = line.split(':')[-1].strip()
            # else:
            #     result = "ERROR"
            #     if 'AssertionError' not in list_detail[i].text:
            #         failed_reason = '[ERROR] Test scripts were error. Please check it again.'

            # parse fail reason

            # actual_result = ''
            # if 'AssertionError' not in list_detail[i].text:
            #     failed_reason = '[ERROR] Test scripts were error. Please check it again.'
            # else:
            #     if (list_detail[i].text).find('Actual') != -1:
            #         actual_result = (list_detail[i].text)[(list_detail[i].text).rfind('Actual'):].split(':')[-1].strip()
            #     else:
            #         find_actual = str(list_detail[i].text).splitlines()
            #         for line in find_actual:
            #             if line.find(': [') != -1:
            #                 actual_result = line.split(':')[-1].split(']')[-1].strip()
            #
            #     lines = str(list_detail[i].text).splitlines()
            #     for line in lines:
            #         if line.find(': [') != -1:
            #             failed_reason = line.split(':')[-1].strip()

            output[i] = [list_test_case[i].text, result, failed_reason, actual_result]
        driver.quit()

        # write result to excel file

        # report_xlsx = os.path.join(os.getcwd(), 'report.xlsx')
        report_xlsx = 'Reports/Report_Payroll.xlsx'
        wb = openpyxl.load_workbook(report_xlsx)

        ws = wb.active
        redFill = PatternFill(start_color='FF0000',
                              end_color='FF0000',
                              fill_type='solid')
        GreenFill = PatternFill(start_color='32CD32',
                              end_color='32CD32',
                              fill_type='solid')
        YellowFill = PatternFill(start_color='FFF000',
                              end_color='FFF000',
                              fill_type='solid')

        for n in range(2, ws.max_row + 1):
            for i in range(len(output)):
                test_case = str(ws.cell(n, 1).value)
                if output[i][0] == test_case:

                    # actual result
                    # ws.cell(n, 18).value = output[i][2]
                    ws.cell(n, 9).value = output[i][2]

                    # pass/fail?
                    # ws.cell(n, 19).value = output[i][1]
                    ws.cell(n, 8).value = output[i][1]

                    # # fill actual result text
                    # ws.cell(n, 19).value = output[i][3]

                    # if ws.cell(n, 19).value == 'NG':
                    #     ws.cell(n, 19).fill = redFill
                    if ws.cell(n, 8).value == 'ERROR':
                        ws.cell(n, 8).fill = YellowFill

                    elif ws.cell(n, 8).value == 'FAIL':
                        ws.cell(n, 8).fill = redFill
                        num_failed = num_failed + 1
                    else:
                        ws.cell(n, 8).fill = GreenFill
                    break

        wb.save(report_xlsx)


if __name__ == '__main__':
    for i in range(1, len(sys.argv)):
        export_excel(sys.argv[i])
