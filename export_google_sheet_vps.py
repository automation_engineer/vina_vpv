import gspread
from oauth2client.service_account import ServiceAccountCredentials
import datetime
import pandas as pd
from selenium import webdriver
import time
import openpyxl
import sys, os
from openpyxl.styles import PatternFill
import pandas as pd
from pandas import ExcelWriter
from pandas import ExcelFile


# from datetime import datetime
def export_gg_sheet():
    a = str(datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S'))

    scope = ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive']
    creds = ServiceAccountCredentials.from_json_keyfile_name('dn8c_cred.json', scope)
    client = gspread.authorize(creds)

    # Duplicate sheet
    sheet = client.open("VPV Report")
    sheet.duplicate_sheet(source_sheet_id=0, insert_sheet_index=1, new_sheet_name=a)
    sheet = client.open("VPV Report").get_worksheet(1)
    # sheet.resize(50, 12)

    # Read file excel
    df = pd.read_excel('Reports\\Report_VPS.xlsx', sheet_name='Detail')
    df.head()
    result = df['Result'].tolist()
    bug = df['Bug'].tolist()

    # Write to google sheet
    for i in range(2, len(result)+2):
        time.sleep(5)
        sheet.update_cell(i, 8, result[i-2])
        sheet.update_cell(i, 9, bug[i-2])


    sheet = client.open("VPV Report").get_worksheet(0)
    sheet.update_cell(11, 3, "=COUNTIF('" + a + "'!K2:'" + a + "'!K50; " + '"*PASS*")')
    sheet.update_cell(12, 3, "=COUNTIF('" + a + "'!K2:'" + a + "'!K50; " + '"*FAIL*")')
    sheet.update_cell(13, 3, "=COUNTIF('" + a + "'!K2:'" + a + "'!K50; " + '"*ERROR*")')
    sheet.update_cell(14, 3, "=COUNTIF('" + a + "'!K2:'" + a + "'!K50; " + '"*N/A*")')
    sheet.update_cell(15, 3, "=COUNTIF('" + a + "'!K2:'" + a + "'!K50; " + '"*CNT*")')


if __name__ == '__main__':
    export_gg_sheet()
